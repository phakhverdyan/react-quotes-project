const mix = require('laravel-mix');
const ReactLoadablePlugin = require('react-loadable/webpack')
    .ReactLoadablePlugin;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.react('resources/js/app.js', 'public/js').sass(
    'resources/sass/app.scss',
    'public/css'
);

mix.webpackConfig(webpack => {
    return {
        output: {
            chunkFilename: 'js/[name].js'
        },
        resolve: {
            alias: {
                'react-hot-loader': path.resolve(
                    path.join(__dirname, './node_modules/react-hot-loader')
                ),
                react: path.resolve(
                    path.join(__dirname, './node_modules/react')
                ),
                'react-dom': path.resolve(
                    path.join(__dirname, './node_modules/@hot-loader/react-dom' )
                )
            }
        },
        devtool: 'eval-source-map',
        plugins: [
            new ReactLoadablePlugin({
                filename: './dist/react-loadable.json'
            })
        ],
        devServer: {
            hot: true, // this enables hot reload
            inline: true, // use inline method for hmr.
            disableHostCheck: true,
            watchOptions: {
                exclude: [/bower_components/, /node_modules/]
            }
        },
        node: {
            fs: 'empty',
            module: 'empty'
        }
    };
});

Mix.listen('configReady', webpackConfig => {
    if (Mix.isUsing('hmr')) {
        // Remove leading '/' from entry keys
        webpackConfig.entry = Object.keys(webpackConfig.entry).reduce(
            (entries, entry) => {
                entries[entry.replace(/^\//, '')] = webpackConfig.entry[entry];
                // }
                console.log(entries);
                return entries;
            },
            {}
        );
        // Remove leading '/' from ExtractTextPlugin instances
        webpackConfig.plugins.forEach(plugin => {
            if (plugin.constructor.name === 'ExtractTextPlugin') {
                console.log(plugin.filename);
                plugin.filename = plugin.filename.replace(/^\//, '');
                console.log(plugin.filename);
            }
        });
    }
});

if (mix.inProduction()) {
    mix.version();
}

mix.options({
    hmrOptions: {
        host: 'localhost',
        port: 8080
    }
});
