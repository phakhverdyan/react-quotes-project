<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ( !$this->app->environment( 'production' ) ) {
            if ( !empty( $providers = config( 'app.dev_providers' ) ) ) {
                foreach ($providers as $provider) {
                    $this->app->register( $provider );
                }
            }

            if ( !empty( $aliases = config( 'app.dev_aliases' ) ) ) {
                foreach ($aliases as $alias => $facade) {
                    $this->app->alias( $alias, $facade );
                }
            }
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if ( !$this->app->environment( 'production' ) ) {
            Config::set( [ 'mail.to' => [
                'address' => 'repo@rllinsure.com',
                'name' => 'Reep Ough'
            ]] );
        }
    }
}
