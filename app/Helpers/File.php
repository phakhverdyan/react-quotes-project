<?php

namespace App\Helpers;


class File
{
    public static function getFileName( $fileName )
    {
        if ( $fileName != "" ) {
            return pathinfo( $fileName, PATHINFO_FILENAME );
        }

        return null;
    }

    public static function getFileExtension( $fileName )
    {
        if ( $fileName != "" ) {
            return pathinfo( $fileName, PATHINFO_EXTENSION );
        }

        return null;
    }
}