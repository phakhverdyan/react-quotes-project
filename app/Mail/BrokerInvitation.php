<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class BrokerInvitation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var User
     */
    public $user;

    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $randomStr = str_random(10);
        $user->password = Hash::make( $randomStr );
        $user->save();

        $this->user = $user;
        $this->password = $randomStr;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subject = "RLL Quoting System Invitation";

        if (!\App::environment('production')) {
            $subject = "[QUOTES TEST] " . $subject;
        }

        return $this->subject( $subject )->view('mail.broker.invitation');
    }
}
