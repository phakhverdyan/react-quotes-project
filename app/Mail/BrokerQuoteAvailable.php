<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\Property;

class BrokerQuoteAvailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var User
     */
    public $user;
    
    /**
     * The property instance.
     *
     * @var Property
     */
    public $property;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( User $user, Property $property)
    {
        $this->user = $user;
        $this->property = $property;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "RLL Quoting System - Quote Available";

        if (!\App::environment('production')) {
            $subject = "[QUOTES TEST] " . $subject;
        }
        return $this->subject( $subject )->view('mail.broker.quote-available');
    }
}
