<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->wantsJson()) {
            $debug_mode = config('app.debug');
            $jsonResponse = [
                'error' => 'There was an error processing the request'
            ];
            if ($debug_mode) {
                $jsonResponse['exception'] = $exception->getMessage();
                $jsonResponse['trace'] = $exception->getTraceAsString();
            }
            $statusCode = 500;
            // if unauthorized return 401
            if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
                $statusCode = 401;
            }
            return response()->json($jsonResponse, $statusCode);
        }
        return parent::render($request, $exception);
    }
}
