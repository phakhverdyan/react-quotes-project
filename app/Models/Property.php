<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property integer $rental_type_id
 * @property integer $construction_type_id
 * @property integer $building_construction_type_id
 * @property integer $company_id
 * @property string $property_name
 * @property string $phone_no
 * @property string $phone_ext
 * @property string $address
 * @property string $suite
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 * @property int $total_units
 * @property boolean $affordable_units
 * @property int $no_of_affordable_units
 * @property string $name_insured
 * @property string $loss_runs
 * @property boolean $onsite_management
 * @property string $property_software
 * @property string $website
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Company $company
 * @property ConstructionType $constructionType
 */
class Property extends Model
{
    use SoftDeletes;
    
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'company_id',
        'rental_type_id',
        'construction_type_id',
        'building_construction_type_id',
        'quote_id',
        'property_name',
        'phone_no',
        'phone_ext',
        'address',
        'suite',
        'city',
        'state',
        'zip',
        'country',
        'total_units',
        'affordable_units',
        'no_of_affordable_units',
        'name_insured',
        'loss_runs',
        'onsite_management',
        'roof_construction',
        'property_software',
        'website'];


    public static function properties( $userId = "" )
    {
        $properties = DB::table('properties')
            ->select( 'properties.*' )
            ->join('companies', 'companies.id', '=', 'properties.company_id')
            ->join('users', function ($joinUser, $userId = "") {
                $joinUser->on( 'users.id', 'companies.user_id');
                if ( $userId != "" ) {
                    $join->where('companies.user_id', '=', $userId);
                }
                $joinUser->join( 'partners', function( $joinPartners ) {
                    $joinPartners->on( 'partners.id', 'users.partner_id' );
                });
            })
            ->join('statuses', 'statuses.id', '=', 'properties.status_id' )
            ->get();

        return $properties;
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function buildingConstructionType()
    {
        return $this->belongsTo('App\Models\BuildingConstructionType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function constructionType()
    {
        return $this->belongsTo('App\Models\ConstructionType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rentalType()
    {
        return $this->belongsTo('App\Models\RentalType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propertyQuotes()
    {
        return $this->hasMany('App\Models\PropertyQuote');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $max_id = self::max('quote_id');
            $model->attributes['quote_id'] = $max_id + 1;
        });

    }
}
