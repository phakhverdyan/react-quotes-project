<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $company_name
 * @property string $contact_person
 * @property string $phone_no
 * @property string $phone_ext
 * @property string $address
 * @property string $suite
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property User $user
 * @property Property[] $properties
 */
class Company extends Model
{
    use SoftDeletes;
    
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'company_name', 'contact_person', 'phone_no', 'phone_ext', 'address', 'suite', 'city', 'state', 'zip', 'country'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('App\Models\Property');
    }
}
