<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $name
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property DefaultQuote[] $defaultQuotes
 * @property PropertyQuote[] $propertyQuotes
 */
class PdlwOption extends Model
{
    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function defaultQuotes()
    {
        return $this->hasMany('App\Models\DefaultQuote');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propertyQuotes()
    {
        return $this->hasMany('App\Models\PropertyQuote');
    }
}
