<?php

namespace App\Models;

use LucasQuinnGuru\Cerberus\CerberusRole;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends CerberusRole
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'display_name'];
}