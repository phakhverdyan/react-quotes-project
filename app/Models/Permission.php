<?php

namespace App\Models;

use LucasQuinnGuru\Cerberus\CerberusPermission;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends CerberusPermission
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'description', 'display_name'];
}