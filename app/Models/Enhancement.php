<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $name
 * @property float $price
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property DefaultQuote[] $defaultQuotes
 * @property PropertyQuote[] $propertyQuotes
 */
class Enhancement extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'price', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function defaultQuotes()
    {
        return $this->hasMany('App\Models\DefaultQuote');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propertyQuotes()
    {
        return $this->hasMany('App\Models\PropertyQuote');
    }
}
