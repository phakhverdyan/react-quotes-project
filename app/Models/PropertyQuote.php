<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property integer $id
 * @property integer $property_id
 * @property integer $pdlw_option_id
 * @property integer $base_price_id
 * @property integer $personal_coverage_id
 * @property integer $enhancement_id
 * @property boolean include
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property BasePrice $basePrice
 * @property Enhancement $enhancement
 * @property PdlwOption $pdlwOption
 * @property PersonalCoverage $personalCoverage
 * @property Property $property
 */
class PropertyQuote extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'property_id',
        'pdlw_option_id',
        'base_price',
        'personal_coverage_id',
        'enhancement_id',
        'viewable',
        'selected',
        'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function basePrice()
    {
        return $this->belongsTo('App\Models\BasePrice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enhancement()
    {
        return $this->belongsTo('App\Models\Enhancement');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pdlwOption()
    {
        return $this->belongsTo('App\Models\PdlwOption');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function personalCoverage()
    {
        return $this->belongsTo('App\Models\PersonalCoverage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('App\Models\Property');
    }
}
