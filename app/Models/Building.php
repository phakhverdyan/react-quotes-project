<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $property_id
 * @property string $building_number
 * @property int $year_built
 * @property int $number_of_stories
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Property $property
 */
class Building extends Model
{
    use SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['property_id', 'building_number', 'year_built', 'number_of_stories' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('App\Models\Property');
    }
}
