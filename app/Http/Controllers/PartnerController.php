<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partner;

class PartnerController extends Controller
{
    public function partners()
    {
        return response()->json( Partner::all() );
    }
}
