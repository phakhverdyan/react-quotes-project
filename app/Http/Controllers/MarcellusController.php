<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\BrokerInvitation;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Notifications\SlackNotification;

class MarcellusController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index( Request $request )
    {
        echo "Hello there";
    }

    public function emailInvitation( Request $request )
    {

        $id = 33;

        $user = User::where( ["id"=>$id ])->withRole( 'broker')->with( 'UserInvitation' )->first();

        //dd( $user );

        Mail::to($user)
            ->bcc( "repo@rllinsure.com" )
            ->queue(new BrokerInvitation( $user ));

        $user->notify( new SlackNotification( $user ) );

        echo "broker invitation sent for userid = " . $id . ".";
    }
}
