<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RentalType;

class RentalTypeController extends Controller
{
    public function all() 
    {
        return response()->json(RentalType::all());
    }
}
