<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;
use App\Models\PropertyQuote;
use App\Models\User;
use App\Models\Property;
use App\Mail\BrokerQuoteAvailable;
use Illuminate\Support\Facades\Mail;
use App\Notifications\SlackNotificationQuoteSent;
use Config;
use App;

class AdminPropertyQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $propertyId = $request[ 'propertyId' ];

        $aryPropertyQuotes = $request[ 'quotes'];

        DB::beginTransaction();

        try {
            foreach ($aryPropertyQuotes as $aryPropertyQuote) {

                $id = "";

                if( isset( $aryPropertyQuote[ 'property_quote_id' ] ) ) {
                    $id = $aryPropertyQuote[ 'property_quote_id' ];
                }

                $ids[] = $id;


                $propertyQuote = PropertyQuote::updateOrCreate(
                    [
                        "id" => $id
                    ],
                    [
                        'property_id' => $propertyId,
                        'pdlw_option_id' => $aryPropertyQuote[ 'program' ],
                        'base_price' => $aryPropertyQuote[ 'price' ],
                        'personal_coverage_id' => $aryPropertyQuote[ 'coverage' ],
                        'enhancement_id' => ( $aryPropertyQuote[ 'enhancements' ] == "true" ) ? 2 : 1,
                        'viewable' => $aryPropertyQuote[ 'include' ],
                        'status' => 1
                    ]
                );

                $quotes[] = $propertyQuote;
            }

            // Commit Transaction
            DB::commit();
            $status = "success";
            $message = "";

            //Send notification to broker
            $userId = $propertyQuote->property->company->user_id;
            $user = User::find($userId);
            $property = Property::find($propertyId);
            $property->status_id = 6;
            $property->save();

            Mail::to($user)
                ->bcc( "repo@rllinsure.com" )
                ->queue(new BrokerQuoteAvailable( $user, $property ));

            $user->notify( new SlackNotificationQuoteSent($user) );

        } catch (\Exception $e) {
            // Rollback Transaction
            DB::rollback();
            // rethrow the exception so it handled by the global exp.. handler
            Log::alert('There was an error creating the property quotes.', $aryPropertyQuotes);
            $message = $e->getMessage();
            throw $e;
        }

        $respondWith = [
            "status" => $status,
            "message" => $message,
            'payload' => [ "quotes"=>$quotes ]
        ];


        return Response::json($respondWith, 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
