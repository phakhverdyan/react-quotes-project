<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PersonalCoverage;

class PersonalCoveragesController extends Controller
{
    public function all()
    {
        return response()->json(PersonalCoverage::all());
    }
}
