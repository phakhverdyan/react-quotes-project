<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserInvitation;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\BrokerInvitation;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;
use Carbon\Carbon;
use App\Notifications\SlackNotification;
use App\Notifications\SlackNotificationInvitationAccepted;

class UserController extends Controller
{

    public function email_exists(Request $request) {
        $request->validate([
            'email' => 'required',
        ]);

        return response()->json([
            'exists' =>User::where('email', strtolower($request->input('email')))->exists(),
        ]);
    }

    public function token(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'email' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $api_token = Str::random(60);

            $user = Auth::user();
            $user->api_token = $api_token;

            $user->save();

            // $roles = $user->roles;

            return response()->json([
                'token' => $api_token,
                'profile' => $user,
                'isAdmin' => $user->hasRole( 'super-admin')
            ]);
        }

        return response()->json(['message' => 'Invalid email or password'], 401);

    }

    public function users( Request $request )
    {
        $users = User::where();
        return response()->json( $users );
    }

    public function create( Request $request )
    {
        $user = User::create([
            'firstname' => $request['firstname'],
            'partner_id' => $request['partner_id'],
            'middlename' => $request[ 'middlename' ],
            'lastname' => $request[ 'lastname' ],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        $userInvitation = UserInvitation::create([
            'user_id' => $user->id,
            'token' => Str::Random(40)
        ]);
        
        $broker = Role::where([ 'name'=>'broker' ] )->first();

        $user->attachRole( $broker );

        Mail::to($user)
            ->queue(new BrokerInvitation( $user ));
        $user->notify( new SlackNotification($user) );

        return response()->json($user);
    }

    public function brokers( Request $request )
    {
        $brokers = User::withRole('broker')->get();
        return response()->json( $brokers );
    }

    public function acceptInvitation($token)
    {
        $userInvitation = UserInvitation::where('token', $token)->first();

        if (isset($userInvitation) ){
            $user = $userInvitation->user;
            if (!$user->email_verified_at) {
                $userInvitation->user->email_verified_at = Carbon::now();
                $userInvitation->user->save();

                $user->notify( new SlackNotificationInvitationAccepted($user) );

                $status = "Your email is verified and invitation accepted. You can now login.";

            } else {
                $status = "Your have already accepted your invitation. You can now login.";
            }
        } else {
            return redirect('/broker/activity')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect('/')->with('status', $status);
    }
}