<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PropertyQuote;
use App\Models\DefaultQuote;
use App\Models\PdlwOption;
use App\Models\BasePrice;
use App\Models\Enhancement;
use App\Models\PersonalCoverage;
use App\Models\Property;
use App\Notifications\SlackNotificationQuoteBound;

use Illuminate\Support\Facades\Response;

class PropertyQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Property $property)
    {

        $user = $request->user();
        $quotes = [];
    

        if( !$user->hasRole( 'super-admin' ) ) {
            $myQuotes = $property->propertyQuotes->where( "viewable", "=", 1 );

            foreach ($myQuotes as $quote) {
                $quotes[] = $quote;
            }
        
            //$quotes = $property->propertyQuotes;
        } else {
            $quotes = $property->propertyQuotes;
        }

        $respondWith = [
            "status" => "success",
            "message" => "ok",
            'payload' => [
                "property" =>$property,
                "quotes"=>$quotes,
                "default_quotes" => DefaultQuote::all(),
                "pdwl_options" => PdlwOption::all(),
                "base_prices" => BasePrice::all(),
                "enhancements" => Enhancement::all(),
                "personal_coverages" => PersonalCoverage::all()
            ]
        ];

        return Response::json($respondWith, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function defaults(Request $request)
    {
        return response()->json([
            "default_quotes" => DefaultQuote::all(),
            "pdwl_options" => PdlwOption::all(),
            "base_prices" => BasePrice::all(),
            "enhancements" => Enhancement::all(),
            "personal_coverages" => PersonalCoverage::all(),
        ]);
    }

    /**
     * Returns the quotes for the property.
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function byProperty(Request $request, Property $property)
    {
        $quotes = $property->propertyQuotes;
        $respondWith = [
            "status" => "success",
            "message" => "ok",
            'payload' => [ "property" =>$property, "quotes"=>$quotes]
        ];

        return Response::json($respondWith, 200);
    }

    public function bind(Request $request, Property $property)
    {
        PropertyQuote::where( [ "property_id" => $property->id])->update(array('selected' => false));
        $quote = PropertyQuote::where( [ "property_id" => $property->id, "id"=> $request->quoteId ] )->first();
        $quote->selected = true;
        $quote->save();

        $property->status_id = 8;
        $property->save();

        $respondWith = [
            "status" => "success",
            "message" => "ok",
            'payload' => [ "property" =>$property, "quote"=>$quote]
        ];

        $user = $request->user();

        $user->notify( new SlackNotificationQuoteBound($user) );


        return Response::json($respondWith, 200);
    }
}
