<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\Company;
use App\Models\RentalType;
use App\Models\ConstructionType;
use App\Models\BuildingConstructionType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;
use App\Helpers\File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class PropertyController
 * @package App\Http\Controllers
 */
class PropertyController extends Controller
{
    public function index( Request $request) 
    {

        $user = $request->user();

        if( !$user->hasRole( 'super-admin' ) ) {
            $properties = Property::select( 'properties.*' )->with( [ 'company.user' => function ($query) {
                $query->where('users.id', '=', $user->id);
            }])
                ->with( 'company.user.partner' )
                ->with( 'status' )
                ->join( "companies", "companies.id", "=", "properties.company_id" )
                ->where( 'companies.user_id', "=", $user->id)
                ->get();
        } else {
            $properties = Property::with('company.user.partner')->with( 'status' )->get();  
        }
        
        $respondWith = [
            "status" => "success",
            "message" => "ok",
            'payload' => [ 
                "properties" => $properties
            ]
        ];

        return Response::json($respondWith, 200);
    }
    
    public function show(Request $request, Property $property) 
    {
        //$id = $id;

        //$property = Property::find($id);
        $company = $property->company;
        $messages = $property->messages;

        $respondWith = [
            "status" => "success",
            "message" => "ok",
            'payload' => [ 
                "property" => $property,
                "status" => $property->status,
                "company" => $company,
                "messages" => $messages,
            ]
        ];

        return Response::json($respondWith, 200);
    }

    public function create( Request $request )
    {
        $myRequest = json_decode( $request[ 'values' ], true );

        $message = null;
        Log::info('Creating property.', $myRequest);
        Log::info( $request->file('file') );

        DB::beginTransaction();

        try {
            $company = Company::create([
                'user_id' => $request->user()->id,
                'company_name' => $myRequest[ 'companyName' ],
                'contact_person' => $myRequest[ 'companyContactPerson' ],

                'address' => $myRequest[ 'companyAddress' ],
                'suite' => $myRequest[ 'companySuite' ],
                'city' => $myRequest[ 'companyCity' ],
                'state' => $myRequest[ 'companyState' ],
                'zip' => $myRequest[ 'companyZip' ],
                'phone_no' => $myRequest[ 'companyPhone' ]
            ]);

            $property = Property::create([
                'company_id' => $company->id,
                'property_name' => $myRequest['propertyName'],
                'phone_no' => $myRequest['propertyPhone'],
                //'phone_ext' => $myRequest[ 'phone_ext' ],
                'address' => $myRequest[ 'propertyAddress' ],
                'suite' => $myRequest['propertySuite'],
                'city' => $myRequest['propertyCity'],
                'state' => $myRequest['propertyState'],
                'zip' => $myRequest['propertyZip'],
                //'country' => $myRequest['country'],
                'total_units' => $myRequest['propertyNumUnits'],
                'affordable_units' => ( $myRequest['propertyAffordableHousing'] == "false" ) ? 0 : 1,
                'no_of_affordable_units' => ( $myRequest['numRentRegulatedUnits'] != "" ) ? $myRequest['numRentRegulatedUnits'] : 0,
                'name_insured' => $myRequest['propertyNameInsured'],
                'rental_type_id' => $myRequest[ 'propertyRentalsType' ],
                //'loss_runs' => $myRequest['loss_runs'],
                'onsite_management' => ( $myRequest['propertyIsThereOnSiteManagment'] == "false" ) ? 0 : 1,
                'roof_construction' => $myRequest['propertyRoofConstruction'],
                'website' => $myRequest['propertyWebsite'],
                'building_construction_type_id' => $myRequest['propertyBuildingConstruction'],
                'construction_type_id' => $myRequest['propertyConstructionType'],
                'property_software' => $myRequest['propertySoftware' ],
                'status_id' => 3
            ]);

            if( $request->file( 'file' ) ) {
                
                $originalName = File::getFileName( $request->file( 'file' )->getClientOriginalName() );
                $originalFileExtension = File::getFileExtension( $request->file('file' )->getClientOriginalName() );

                $path = $request->file('file')->storeAs('loss_runs/' . $property->id, $originalName . Carbon::now()->format("YmdHis") . "." . $originalFileExtension );

                $property->loss_runs = $path;
                $property->save();
            }

            // Commit Transaction
            DB::commit();
            $status = "success";
        } catch (\Exception $e) {
            // Rollback Transaction
            DB::rollback();
            // rethrow the exception so it handled by the global exp.. handler
            Log::alert('There was an error creating the property.', $myRequest);
            throw $e;
        }

        $respondWith = [
            "status" => $status,
            "message" => $message,
            'payload' => [ "property"=>$property, "company"=>$company ]
        ];


        return Response::json($respondWith, 200); 
    }

    public function update( Request $request, Property $property )
    {
        $status = "success";
        $message = "";

        $myRequest = json_decode( $request[ 'values' ], true );

        $property->property_name = $myRequest['propertyName'];
        $property->phone_no = $myRequest['propertyPhone'];
        $property->address = $myRequest[ 'propertyAddress' ];
        $property->suite = $myRequest['propertySuite'];
        $property->city = $myRequest['propertyCity'];
        $property->state = $myRequest['propertyState'];  
        $property->zip = $myRequest['propertyZip'];
        $property->total_units = $myRequest['propertyNumUnits'];
        $property->affordable_units = ( $myRequest['propertyAffordableHousing'] == "false" ) ? 0 : 1;
        $property->no_of_affordable_units = ( $myRequest['numRentRegulatedUnits'] != "" ) ? $myRequest['numRentRegulatedUnits'] : 0;
        $property->name_insured = $myRequest['propertyNameInsured'];
        $property->rental_type_id = $myRequest[ 'propertyRentalsType' ];
        $property->onsite_management = ( $myRequest['propertyIsThereOnSiteManagment'] == "false" ) ? 0 : 1;
        $property->roof_construction = $myRequest['propertyRoofConstruction'];
        $property->website = $myRequest['propertyWebsite'];
        $property->building_construction_type_id = $myRequest['propertyBuildingConstruction'];
        $property->construction_type_id = $myRequest['propertyConstructionType'];
        $property->property_software = $myRequest['propertySoftware' ];

        $property->save();

        $company = $property->company;

        $company->company_name = $myRequest[ 'companyName' ];
        $company->contact_person = $myRequest[ 'companyContactPerson' ];
        $company->address = $myRequest[ 'companyAddress' ];
        $company->suite = $myRequest[ 'companySuite' ];
        $company->city = $myRequest[ 'companyCity' ];
        $company->state = $myRequest[ 'companyState' ];
        $company->zip = $myRequest[ 'companyZip' ];
        $company->phone_no = $myRequest[ 'companyPhone' ];

        $company->save();

        $respondWith = [
            "status" => $status,
            "message" => $message,
            'payload' => [ "property"=>$property ]
        ];


        return Response::json($respondWith, 200); 
    }

    public function updateSummary( Request $request, Property $property )
    {
        $status = "success";
        $message = "";

       //dd( $property->id );

        if ( $request->note != "" ) {
            $message = $property->messages()->create([
                'user_id' => $request->user()->id,
                'text' => $request->note
            ]);
        }

        if($request->isAdmin)
        {
            //Need to put status here
            $property->status_id = 9;
            $property->save();
        }
        else{
            //Need to put status here
            $property->status_id = 4;
            $property->save();
        }

        

        $respondWith = [
            "status" => $status,
            "message" => $message,
            'payload' => [ "property"=>$property ]
        ];


        return Response::json($respondWith, 200); 
    }

    public function defaults( Request $request )
    {
        return response()->json( [
            "rental_types" => RentalType::all( 'id', 'type' ),
            "construction_types" =>ConstructionType::all( 'id', 'name' ),
            "building_construction_types" => BuildingConstructionType::all( 'id', 'name' ),
        ]);
    }

    public function lossRuns( Request $request, Property $property )
    {
        $status = "success";
        $message = "";
        $payload = [];
        $url = "";

        $user = $request->user();

        if( $user->hasRole( 'super-admin' ) || $property->company->user->id == $user->id ) {
            $url = Storage::temporaryUrl(
                $property->loss_runs, now()->addMinutes(3)
            );
        } else {
            $status = "error";
            $message = "Access Denied.  User does not have permission to view the file";
        }

        $respondWith = [
            "status" => $status,
            "message" => $message,
            'payload' => [ "property"=>$property, "url"=>$url ]
        ];

        return Response::json($respondWith, 200);
    }
}
