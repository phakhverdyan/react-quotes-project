module.exports = {
    env: {
        browser: true,
        es6: true
    },
    extends: ['eslint:recommended', 'plugin:react/recommended'],
    parser: 'babel-eslint',
    parserOptions: {
        ecmaFeatures: {
            experimentalObjectRestSpread: true,
            jsx: true
        },
        allowImportExportEverywhere: true,
        sourceType: 'module'
    },
    plugins: ['react'],
    settings: {
        react: {
            version: '16.2.0' // React version. "detect" automatically picks the version you have
        }
    },
    rules: {
        quotes: ['error', 'single'],
        semi: ['error', 'always']
    }
};
