<html>
    <head>
        <title>Quote Available</title>
    </head>
    <body>
        Dear {{$user->firstname}},
        <br><br>
        You have a quote that is in need of review and accpetance.  Please click on the link below to view your quotes.
        <a href="{{url('broker/quote', $property->id)}}">Review Quotes</a>

        <br><br>
        <table cellpadding="0" style="border: 0px;">
        <tbody>
            <tr>
                <td style="vertical-align:top">
                    <a href="https://htmlsig.com/t/000001DTCAAH" target="_blank"><img alt="Renters Legal Liability LLC" src="https://htmlsigs.s3.amazonaws.com/logos/files/001/030/365/landscape/Asset_2.png" style="height:80px; width:123px" /></a>
                    <img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="height:10px; width:123px" />
                </td>
                <td style="vertical-align:top; width: 10px\">
                    <img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:10px" />
                </td>
                <td style='width:100%'>
                    <strong>Renters Legal Liability LLC</strong><br />
                    Email: <a href='mailto:info@rllinsure.com'>info@rllinsure.com</a><br />
                    Office:&nbsp;800.770.9660&nbsp;|&nbsp;Fax:&nbsp;801.596.2732&nbsp;<br />
                    <a href="http://www.rllinsure.com/" target="_blank">www.rllinsure.com</a><br />
                    <a href="https://www.youtube.com/watch?v=McAsg9ILHwc" target="_blank">Watch Video HERE</a>
                    <br />
                    <img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="height:10px; width:317px" />
                    <br />
                    <a href="https://htmlsig.com/t/000001DXCBBF" target="_blank\"><img alt="Twitter" src="https://s3.amazonaws.com/htmlsig-assets/square/twitter.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />&nbsp;<a href="https://htmlsig.com/t/000001DQV21S" target="_blank\"><img alt="Facebook" src="https://s3.amazonaws.com/htmlsig-assets/square/facebook.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />&nbsp;<a href="https://htmlsig.com/t/000001DMD7CQ" target="_blank"><img alt="LinkedIn" src="https://s3.amazonaws.com/htmlsig-assets/square/linkedin.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />&nbsp;<a href="https://htmlsig.com/t/000001DW7P1Y" target="_blank"><img alt="Instagram" src="https://s3.amazonaws.com/htmlsig-assets/square/instagram.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />
                </td>
            </tr>
        </tbody>
    </table>
    </body>
</html>