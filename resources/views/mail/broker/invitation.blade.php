<html>
    <head>
        <title>Email Invitation</title>
    </head>
    <body>
        Dear {{$user->firstname}},
        <br><br>
        RLL just made it faster and simpler to help your client protect their assets, and for you, as a producer to get
        paid for helping them. As part of our ongoing dedication to ensure the best customer experience, we are excited
        to announce the release of our online Producers Application and Quote platform!   
        <br><br>
        This new platform allows you to complete the short application, obtain a quote and receive approval within 48 hours.
        You will need some basic information from your clients (including a loss run history for the most recent 3-year
        period). 
        <br><br>
        Simply <a href="{{url('user/accept-invitation', $user->userInvitation->token)}}">log in</a> with the username and password that will be provided to you. Once logged in, you will be able to enter
        the information and receive a preliminary quote. We may request additional information as necessary to obtain
        underwriting approval. Within 48 hours of submission, you will receive confirmation of your status. Once approved,
        your client will receive binding confirmation and a copy of their policy.
        <br><br>
        Upon approval and participation in the RLL® program, they will be billed monthly based on the number of participating
        units. As the broker/producer, you will receive a commission check monthly from RLL.   
        <br><br>
        Please feel free to reach out to us with any questions or for assistance at 800-770-9660 or
        <a href='mailto:producers@rllinsure.com'>producers@rllinsure.com</a>.
        <br><br>
        Have a great day and welcome to RLL®.

        <br><br>
        <a href="{{url('user/accept-invitation', $user->userInvitation->token)}}">Accept Invitation</a>
        <br><br>
        Username: {{$user->email}}<br>
        Password: {{$password}}

        <br><br>
        <table cellpadding="0" style="border: 0px;">
        <tbody>
            <tr>
                <td style="vertical-align:top">
                    <a href="https://htmlsig.com/t/000001DTCAAH" target="_blank"><img alt="Renters Legal Liability LLC" src="https://htmlsigs.s3.amazonaws.com/logos/files/001/030/365/landscape/Asset_2.png" style="height:80px; width:123px" /></a>
                    <img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="height:10px; width:123px" />
                </td>
                <td style="vertical-align:top; width: 10px\">
                    <img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:10px" />
                </td>
                <td style='width:100%'>
                    <strong>Renters Legal Liability LLC</strong><br />
                    Email: <a href='mailto:info@rllinsure.com'>info@rllinsure.com</a><br />
                    Office:&nbsp;800.770.9660&nbsp;|&nbsp;Fax:&nbsp;801.596.2732&nbsp;<br />
                    <a href="http://www.rllinsure.com/" target="_blank">www.rllinsure.com</a><br />
                    <a href="https://www.youtube.com/watch?v=McAsg9ILHwc" target="_blank">Watch Video HERE</a>
                    <br />
                    <img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="height:10px; width:317px" />
                    <br />
                    <a href="https://htmlsig.com/t/000001DXCBBF" target="_blank\"><img alt="Twitter" src="https://s3.amazonaws.com/htmlsig-assets/square/twitter.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />&nbsp;<a href="https://htmlsig.com/t/000001DQV21S" target="_blank\"><img alt="Facebook" src="https://s3.amazonaws.com/htmlsig-assets/square/facebook.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />&nbsp;<a href="https://htmlsig.com/t/000001DMD7CQ" target="_blank"><img alt="LinkedIn" src="https://s3.amazonaws.com/htmlsig-assets/square/linkedin.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />&nbsp;<a href="https://htmlsig.com/t/000001DW7P1Y" target="_blank"><img alt="Instagram" src="https://s3.amazonaws.com/htmlsig-assets/square/instagram.png" style="height:16px; width:16px" /></a>&nbsp;<img alt="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" src="https://s3.amazonaws.com/htmlsig-assets/spacer.gif" style="width:2px" />
                </td>
            </tr>
        </tbody>
    </table>
    </body>
</html>