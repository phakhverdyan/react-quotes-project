<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        html, body{ height: 100%; }

        #col1 {
            background-color: blue;
            color: white;
        }

        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="h-100">
    <div class="row justify-content-center h-100">
        <div class="col-md-5 text-center d-flex align-items-center" id="col1">
            <div class='w-100'>
                <h2>Renters Legal Liability</h2>
                <p class="lead">Quotes System</p>
            </div>
        </div>
        <div class="col-md-7 text-center d-flex align-items-center"" id="col2">
            <div class='w-100'>
                @yield('content')
            </div>
        </div>
    </div>

</div>
</body>
</html>
