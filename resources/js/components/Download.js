/* eslint-disable react/prop-types */
import React from 'react';
import { withRouter } from 'react-router';
import axios from 'axios';
import { ComponentLoading } from './Common/Common';

class Download extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            downloadUrl: '',
            loading: true,
            error: null
        };
        this.id = this.props.match.params.id;
    }

    componentWillMount() {
        axios
            .get('/api/broker/property/' + this.id + '/loss-runs')
            .then(r => {
                if (r.data && r.data.payload && r.data.payload.url) {
                    this.setState({
                        url: r.data.payload.url,
                        loading: false,
                        error: null
                    });
                    window.location = r.data.payload.url;
                } else {
                    this.setState({
                        loading: false,
                        error: 'Invalid file'
                    });
                }
            })
            .catch(() => {
                this.setState({
                    loading: false,
                    error: 'There was an error while getting the file'
                });
            });
    }

    render() {
        return (
            <>
                {this.state.error && this.state.error}
                {!this.state.error && (
                    <>
                        <p>Getting download url...</p>
                        <ComponentLoading />
                    </>
                )}
            </>
        );
    }
}

export default withRouter(Download);
