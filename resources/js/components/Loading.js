// eslint-disable-next-line no-unused-vars
import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const Loading = () => (
    <div className="loading-div">
        <LinearProgress />
    </div>
);


export default Loading;
