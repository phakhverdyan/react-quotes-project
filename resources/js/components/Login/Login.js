// eslint-disable-next-line no-unused-vars
import React from 'react';
import Grid from '@material-ui/core/Grid';

import { Logo } from './../Logo';
import LoginForm from './LoginForm';

const Login = () => {
    // eslint-disable-next-line react/prop-types
    return (
        <div className="login-main">
            <Grid container>
                <Grid item xs={6} className="pic-main">
                    <div id="login-title">Producers Application and<br /> Quote System</div>
                    <Logo className="center-logo" />
                </Grid>
                <Grid item xs={6}>
                    <div className="help-text">
                        If you have any problems with sign in, please call:
                        <span className="phone-text"> (800) 770-9660</span>
                    </div>
                    <LoginForm className="login-form" />
                    <div className="forgot-password-div">Forgot password?</div>
                </Grid>
            </Grid>
        </div>
    );
};

export default Login;
