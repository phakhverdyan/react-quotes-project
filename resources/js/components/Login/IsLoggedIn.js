/* eslint-disable react/prop-types */
import React from 'react';
import { observer, inject } from 'mobx-react';
import { Redirect } from 'react-router-dom';
import Login from './Login';
import { AdminActivity, BrokerActivity } from '../../constants';
import { withRouter } from 'react-router';
import queryString from 'query-string';

const IsLoggedIn = ({ loginstore, location }) => {
    let redirectTo = loginstore.isAdmin ? AdminActivity : BrokerActivity;
    const params = queryString.parse(location.search);

    if (params.redirectTo) {
        redirectTo = params.redirectTo;
    }
    return loginstore.isLoggedIn ? <Redirect to={redirectTo} /> : <Login />;
};

const IsLoginObservable = withRouter(
    inject('loginstore')(observer(IsLoggedIn))
);

export default IsLoginObservable;
