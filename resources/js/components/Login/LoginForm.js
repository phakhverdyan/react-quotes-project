/* eslint-disable react/prop-types */
import React from 'react';
import { UsernameInput, PasswordInput } from './../Form/TextInput';
import { SubmitButton } from './../Form/Buttons';
import { WhiteCheckBox } from './../Form/SelectionControls';
import FormHelperText from '@material-ui/core/FormHelperText';
import { observer, inject } from 'mobx-react';
import { Formik } from 'formik';
import * as Yup from 'yup';

// eslint-disable-next-line react/prop-types

const Form = props => {
    const { loginstore } = props;
    const {
        values: { username, password, remember },
        errors,
        touched,
        handleChange,
        handleSubmit,
        isValid,
        setFieldTouched
    } = props;

    const change = (name, e) => {
        e.persist();
        handleChange(e);
        setFieldTouched(name, true, false);
        loginstore.clearError();
    };
    return (
        <form onSubmit={handleSubmit}>
            <UsernameInput
                type="email"
                value={username}
                onChange={change.bind(null, 'username')}
                autoComplete="username"
                helperText={touched.username ? errors.username : ''}
                error={touched.username && Boolean(errors.username)}
            />

            {loginstore.hasEmailError && (
                <FormHelperText
                    id="username-error-text"
                    error={true}
                    classes={{
                        root: 'error-text'
                    }}
                >
                    {loginstore.getEmailError}
                </FormHelperText>
            )}
            <PasswordInput
                onChange={change.bind(null, 'password')}
                helperText={touched.password ? errors.password : ''}
                autoComplete="current-password"
                value={password}
                error={touched.password && Boolean(errors.password)}
            />
            {loginstore.hasPasswordError && (
                <FormHelperText
                    id="password-error-text"
                    error={true}
                    classes={{
                        root: 'error-text'
                    }}
                >
                    {loginstore.getPasswordError}
                </FormHelperText>
            )}
            {loginstore.hasErrorMessage && (
                <FormHelperText
                    id="username-error-text"
                    error={true}
                    classes={{
                        root: 'error-text'
                    }}
                >
                    {loginstore.getErrorMessage}
                </FormHelperText>
            )}
            <SubmitButton
                type="submit"
                disabled={loginstore.isFetching || !isValid}
                fullWidth
            >
                {loginstore.isFetching ? 'Logging In...' : 'Sign In'}
            </SubmitButton>
            <WhiteCheckBox
                label="Remember Me"
                checked={remember}
                onChange={change.bind(null, 'remember')}
                id="remember"
            />
        </form>
    );
};

const validationSchema = Yup.object({
    username: Yup.string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    password: Yup.string('').required('Enter your password')
});

const FormObserver = inject('loginstore')(observer(Form));

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onSubmit(values) {
        this.props.loginstore.doLogin(
            values.remember,
            values.username,
            values.password
        );
    }
    render() {
        const values = {
            username: '',
            password: '',
            remember: false
        };
        return (
            <div {...this.props}>
                <Formik
                    render={props => <FormObserver {...props} />}
                    onSubmit={this.onSubmit}
                    initialValues={values}
                    validationSchema={validationSchema}
                />
            </div>
        );
    }
}

const LoginFormObserver = inject('loginstore')(observer(LoginForm));

export default LoginFormObserver;
