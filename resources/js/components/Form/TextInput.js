/* eslint-disable react/prop-types */
import React from 'react';
import PermIdentity from '@material-ui/icons/PermIdentity';
import Lock from '@material-ui/icons/Lock';
import Search from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';

import TextField from '@material-ui/core/TextField';
import Select from 'react-select';
import Radio from '@material-ui/core/Radio';

import Switch from '@material-ui/core/Switch';
import Checkbox from '@material-ui/core/Checkbox';

import MSelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {
    CustomRadioIcon,
    CustomRadioIconChecked,
    CustomCheckboxIcon,
    CustomCheckboxIconCheked,
    CustomSwitchOffIcon
} from '../Icons';

class AbstractUpdateOnlyValue extends React.Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps) {
        const hasValueChange = this.props.value !== nextProps.value;
        if (hasValueChange) {
            return true;
        }
        const hasCheckedChanged = this.props.checked !== nextProps.checked;
        if (hasCheckedChanged) {
            return true;
        }
        const hasHelperTextChanged =
            this.props.helperText !== nextProps.helperText;
        if (hasHelperTextChanged) {
            return true;
        }
        const hasErrorChanged = this.props.error !== nextProps.error;
        if (hasErrorChanged) {
            return true;
        }
        return false;
    }
}
export class OutLinedTextInput extends AbstractUpdateOnlyValue {
    constructor(props) {
        super(props);
    }

    render() {
        const adornementKey = this.props.icon
            ? 'startAdornment'
            : 'endAdornment';
        const iconPosition = this.props.icon ? 'start' : 'end';
        let labelClass = 'css-label';
        const shrink = this.props.value !== '';
        if (iconPosition === 'start' && !shrink) {
            labelClass = 'css-label css-padding-left';
        }
        return (
            <TextField
                InputLabelProps={{
                    classes: {
                        root: labelClass,
                        focused: 'css-focused'
                    },
                    shrink: shrink
                }}
                InputProps={{
                    classes: {
                        root: 'css-outlined-input',
                        focused: 'css-focused'
                    },
                    [adornementKey]: (
                        <InputAdornment
                            position={iconPosition}
                            classes={{ root: 'icon-class' }}
                        >
                            {this.props.icon
                                ? this.props.icon
                                : this.props.endicon}
                        </InputAdornment>
                    )
                }}
                variant="outlined"
                className={
                    this.props.className ? this.props.className : 'input'
                }
                {...this.props}
            />
        );
    }
}

export class CustomTextInput extends AbstractUpdateOnlyValue {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TextField
                InputLabelProps={{
                    classes: {
                        root: 'css-label',
                        focused: 'css-focused'
                    }
                }}
                InputProps={{
                    classes: {
                        root: 'css-outlined-input',
                        focused: 'css-focused'
                    }
                }}
                variant="outlined"
                className={
                    this.props.className
                        ? this.props.className
                        : 'margin-bottom-20'
                }
                {...this.props}
                label={this.props.placeholder ? this.props.placeholder : ''}
                autoComplete="off"
            />
        );
    }
}

export const UsernameInput = props => (
    <OutLinedTextInput
        icon={<PermIdentity />}
        id="username"
        {...props}
        label="Email"
    />
);
export const PasswordInput = props => (
    <OutLinedTextInput
        icon={<Lock />}
        type="password"
        id="password"
        {...props}
        label="Password"
    />
);
export const SearchInput = props => (
    <OutLinedTextInput
        endicon={<Search />}
        type="search"
        id="search"
        placeholder="Search..."
        {...props}
        label="Search..."
    />
);

// https://react-select.com/styles
const customStyles = {
    control: provided => ({
        ...provided,
        minHeight: '55px',
        height: '55px',
        textAlign: 'center'
    }),
    input: provided => ({
        ...provided,
    }),
    menu: provided => ({
        ...provided,
        zIndex: '20 !important'
    }),
    singleValue: provided => ({
        ...provided
    })
};

// https://react-select.com/home

export class CustomSelectInput extends AbstractUpdateOnlyValue {
    constructor(props) {
        super(props);
    }
    render() {
        const options = this.props.options;
        const currentValue = options
            ? options.find(option => option.value === this.props.value)
            : '';
            
        return (
            <Select
                {...this.props}
                options={this.props.options}
                value={currentValue}
                className="margin-bottom-20 custom-select-input"
                styles={customStyles}
            />
        );
    }
}

export class CustomMaterialSelectInput extends AbstractUpdateOnlyValue {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <MSelect
                inputProps={{
                    name: this.props.name,
                    id: this.props.id
                }}
                {...this.props}
            >
                {this.props.options.map(item => (
                    <MenuItem value={item.value} key={item.value}>
                        {item.label}
                    </MenuItem>
                ))}
            </MSelect>
        );
    }
}

export class CustomSwitchInput extends AbstractUpdateOnlyValue {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Switch
                classes={{
                    bar: 'input-switch-bar'
                }}
                color="default"
                checkedIcon={<CustomSwitchOffIcon />}
                {...this.props}
            />
        );
    }
}

export class CustomCheckbox extends AbstractUpdateOnlyValue {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Checkbox
                color="primary"
                {...this.props}
                icon={<CustomCheckboxIcon fontSize="large" />}
                checkedIcon={<CustomCheckboxIconCheked fontSize="large" />}
            />
        );
    }
}

export class CustomRadio extends AbstractUpdateOnlyValue {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Radio
                color="primary"
                icon={<CustomRadioIcon />}
                checkedIcon={<CustomRadioIconChecked />}
                {...this.props}
            />
        );
    }
}

export const CustomLabel = props => <label {...props}>{props.children}</label>;

export class CustomUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null
        };
        this.triggerInputFile = this.triggerInputFile.bind(this);
    }

    triggerInputFile() {
        this.fileInput.click();
    }

    shouldComponentUpdate(nextProps, state) {
        if (this.state.file !== state.file) {
            return true;
        }

        return false;
    }

    onChange = e => {
        this.props.onChange(e.currentTarget.files[0]);
        this.setState({ file: e.target.files[0] });
    };
    render() {
        return (
            <>
                <input
                    ref={fileInput => (this.fileInput = fileInput)}
                    type="file"
                    style={{ display: 'none' }}
                    onChange={this.onChange}
                    name={this.props.name}
                    id={this.props.name}
                />
                <span
                    className="main-content-action padding-right-20"
                    onClick={this.triggerInputFile}
                >
                    UPLOAD FILE
                </span>
            </>
        );
    }
}
