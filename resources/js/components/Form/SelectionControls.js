/* eslint-disable react/prop-types */
import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const CustomCheckbox = props => (
    <FormControlLabel
        control={
            <Checkbox
                classes={{
                    root: 'checkbox-white',
                    checked: 'checkbox-white'
                }}
                {...props}
            />
        }
        label={props.label}
    />
);

export const WhiteCheckBox = props => <CustomCheckbox {...props} />;
