/* eslint-disable react/prop-types */
import React from 'react';
import Button from '@material-ui/core/Button';

const CustomButton = React.memo(function CustomButton(props) {
    return (
        <Button variant="contained" {...props}>
            {props.children}
        </Button>
    );
});

export const SubmitButton = props => (
    <CustomButton className="btn-main" {...props} />
);
export const AdminSubmitButton = props => (
    <CustomButton className="btn-main-secundary" {...props} />
);

export const AddNewProperty = props => (
    <CustomButton className="add-new-property" {...props} />
);
