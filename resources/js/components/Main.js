/* eslint-disable react/prop-types */
import { hot } from 'react-hot-loader/root';
import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { Provider } from 'mobx-react';
import { NotFound } from './NotFound';
import Loading from './Loading';
import LoginStore from '../stores/LoginStore';
import RootStore from '../stores/RootStore';
import Loadable from 'react-loadable';

import {
    PathBrokerManagment,
    PathAddNewProperty,
    AdminActivity,
    BrokerActivity,
    AdminPartners,
    PathBrokerViewQuote,
    PathBrokerViewProperty,
    PathAdminCreateQuote,
    PathAdminViewProperty,
    PathBrokerPropertySummary,
    PathBrokerDownloadLossRunsFile,
    PathAdminBindQuote,
    UserProfile
} from '../constants';

import './Layout.scss';
import './Login/Login.scss';
import './Form/Form.scss';

const Login = Loadable({
    loader: () => import('./Login/IsLoggedIn'),
    loading: Loading,
    delay: 0
});

const BrokerManagment = Loadable({
    loader: () => import('./Admin/BrokerManagment'),
    loading: Loading,
    delay: 0
});
const PartnerManagment = Loadable({
    loader: () => import('./Admin/PartnerManagment'),
    loading: Loading,
    delay: 0
});

const InviteBroker = Loadable({
    loader: () => import('./Admin/InviteBroker'),
    loading: Loading,
    delay: 0
});

const AddNewProperty = Loadable({
    loader: () => import('./Broker/NewProperty'),
    loading: Loading,
    delay: 0
});
const SubmissionSummary = Loadable({
    loader: () => import('./Broker/SubmissionSummary'),
    loading: Loading,
    delay: 0
});
const AdminViewProperty = Loadable({
    loader: () => import('./Admin/AdminViewProperty'),
    loading: Loading,
    delay: 0
});

const CreateQuote = Loadable({
    loader: () => import('./Admin/CreateQuote'),
    loading: Loading,
    delay: 0
});

const BindQuote = Loadable({
    loader: () => import('./Admin/QuotesBind'),
    loading: Loading,
    delay: 0
});

const ViewQuotes = Loadable({
    loader: () => import('./Broker/ViewQuotes'),
    loading: Loading,
    delay: 0
});

const BrokerCurrentActivity = Loadable({
    loader: () => import('./Broker/BrokerCurrentActivity'),
    loading: Loading,
    delay: 0
});

const Download = Loadable({
    loader: () => import('./Download'),
    loading: Loading,
    delay: 0
});

const AdminCurrentActivity = Loadable({
    loader: () => import('./Admin/AdminCurrentActivity'),
    loading: Loading,
    delay: 0
});

const UserProfileSettings = Loadable({
    loader: () => import('./Admin/UserProfile.js'),
    loading: Loading,
    delay: 0
});

class ScrollToTop extends React.Component {
    componentDidUpdate(prevProps) {
        // eslint-disable-next-line no-undef
        if (process.env.NODE_ENV === 'development' && module.hot) {
            // eslint-disable-next-line no-undef
            return;
        }
        if (this.props.location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0, 0);
        }
    }

    render() {
        return this.props.children;
    }
}

const App = hot(() => (
    <Provider loginstore={LoginStore} rootstore={RootStore}>
        <Router>
            <ScrollToTop>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route exact path={PathBrokerDownloadLossRunsFile} component={Download} />
                    <Route exact path="/login" component={Login} />
                    <Route
                        exact
                        path={BrokerActivity}
                        component={() => <BrokerCurrentActivity />}
                    />
                    <Route
                        exact
                        path={PathAddNewProperty}
                        component={() => <AddNewProperty />}
                    />
                    <Route
                        exact
                        path={PathBrokerPropertySummary}
                        component={() => <SubmissionSummary />}
                    />
                    <Route
                        exact
                        path={AdminActivity}
                        component={() => <AdminCurrentActivity />}
                    />
                    <Route
                        exact
                        path={PathBrokerManagment}
                        component={() => <BrokerManagment />}
                    />
                    <Route
                        exact
                        path={AdminPartners}
                        component={() => <PartnerManagment />}
                    />
                    <Route
                        exact
                        path="/admin/brokers/invite"
                        component={() => <InviteBroker />}
                    />
                    <Route
                        exact
                        path={PathAdminBindQuote}
                        component={() => <BindQuote />}
                    />
                    <Route
                        exact
                        path={PathAdminCreateQuote}
                        component={() => <CreateQuote />}
                    />
                    <Route
                        exact
                        path={PathBrokerViewQuote}
                        component={() => <ViewQuotes />}
                    />
                    <Route
                        exact
                        path={PathBrokerViewProperty}
                        component={() => <AddNewProperty />}
                    />
                    <Route
                        exact
                        path={PathAdminViewProperty}
                        component={() => <AdminViewProperty />}
                    />
                    <Route
                        exact
                        path={UserProfile}
                        component={() => <UserProfileSettings />}
                    />
                    <Route component={NotFound} />
                </Switch>
            </ScrollToTop>
        </Router>
    </Provider>
));

export default App;
