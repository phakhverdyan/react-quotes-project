/* eslint-disable no-unused-vars */
import 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';
import Logger from '../utils/Logger';
import App from './Main';

// import Login from './Login/Login';
// Only enable logger in production
// eslint-disable-next-line no-undef
if (process.env.NODE_ENV !== 'production') {
    localStorage.setItem('debug', 'rllweb-react-app:*');
}

Logger.info('Starting web app');

if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}

// eslint-disable-next-line no-undef
if (process.env.NODE_ENV === 'development' && module.hot) {
    // eslint-disable-next-line no-undef
    module.hot.accept();
}

// eslint-disable-next-line no-undef
// if (process.env.NODE_ENV !== 'production') {
//     // eslint-disable-next-line no-undef
//     const {whyDidYouUpdate} = require('why-did-you-update');
//     whyDidYouUpdate(React);
// }
