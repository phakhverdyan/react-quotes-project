/* eslint-disable react/prop-types */
// eslint-disable-next-line no-unused-vars
import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { observer, inject } from 'mobx-react';
import Reorder from '@material-ui/icons/Reorder';
import { Redirect } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { Logo } from './Logo';
import { AddNewProperty, AdminSubmitButton } from './Form/Buttons';
import AddCircle from '@material-ui/icons/AddCircle';
import { RllHouse, LogoutIcon, SettingsIcon } from './Icons';
import { SuccesModal } from './Common/Dialog';
import {
    PathAddNewProperty,
    BrokerActivity,
    AdminActivity
} from '../constants';

const LogoutModal = ({ logout, close }) => {
    const content = (
        <>
            <span className="in-the-center success-text">LOG OUT</span>
            <p className="modal-body">
                Are you sure you want to exit the system?
            </p>
            <span
                className="in-the-center in-the-center-with-margin"
                style={{
                    maxWidth: '50%',
                    flexDirection: 'row',
                    justifyContent: 'space-around'
                }}
            >
                <AdminSubmitButton onClick={close} className="secondary">
                    CANCEL
                </AdminSubmitButton>
                <AdminSubmitButton onClick={logout}>YES</AdminSubmitButton>
            </span>
        </>
    );
    return <SuccesModal content={content} open={open} />;
};

const MenuItem = props => (
    <Grid container className="menu-item">
        <Grid item xs={props.expanded ? 2 : 3} className="vertical-center">
            <NavLink to={props.url} activeClassName="menu-item-selected">
                {props.icon}
            </NavLink>
        </Grid>
        {props.expanded && (
            <Grid item xs={4} className="vertical-center">
                <span className="menu-text">
                    <NavLink
                        to={props.url}
                        activeClassName="menu-item-selected"
                    >
                        {props.children}
                    </NavLink>
                </span>
            </Grid>
        )}
    </Grid>
);

const SideBarMenu = ({ className, items, expanded }) => {
    return (
        <div className={className}>
            {items.map(item => (
                <MenuItem key={item.text} {...item} expanded={expanded}>
                    {item.text}
                </MenuItem>
            ))}
        </div>
    );
};

class Layout extends React.Component {
    constructor(props) {
        super(props);
        this.toggleExtended = this.toggleExtended.bind(this);
        this.logout = this.logout.bind(this);
        this.state = {
            logginOut: false,
            showLogout: false
        };
    }

    logout() {
        this.setState({
            logginOut: true
        });
        this.props.loginstore.doLogout();
    }

    showLogout = () => {
        this.setState({
            showLogout: true
        });
    };

    closeLogout = () => {
        this.setState({
            showLogout: false
        });
    };

    toggleExtended() {
        this.props.loginstore.toggleExtended();
    }

    render() {
        const { sidebarItems, mainContent, loginstore, admin } = this.props;
        const isAdmin = this.props.loginstore.isAdmin;
        const isExtended = this.props.loginstore.isLayoutExtended;

        let componentToReturn = (
            <Redirect
                to={{
                    pathname: '/',
                    search: '?redirectTo=' + window.location.pathname
                }}
            />
        );
        if (this.state.logginOut) {
            componentToReturn = <Redirect to="/" />;
        }
        if (loginstore.isLoggedIn) {
            componentToReturn = (
                <div className="bg-dashboard full-screen-height">
                    {this.state.showLogout && (
                        <LogoutModal
                            logout={this.logout}
                            close={this.closeLogout}
                        />
                    )}
                    {admin && !isAdmin ? <Redirect to={BrokerActivity} /> : ''}
                    {!admin && isAdmin ? <Redirect to={AdminActivity} /> : ''}
                    <Grid container className="full-screen-height">
                        <Grid
                            item
                            xs={isExtended ? 3 : 1}
                            lg={isExtended ? 3 : 1}
                        >
                            <div className="menu-header">
                                {isExtended && (
                                    <>
                                        <Logo className="menu-logo vertical-center" />
                                        <div>
                                            Producers Application and Quote
                                            System
                                        </div>
                                    </>
                                )}
                                {!isExtended && (
                                    <RllHouse className="menu-logo vertical-center" />
                                )}
                            </div>
                            <div className="bg-menu full-screen-height">
                                <SideBarMenu
                                    className={
                                        isExtended
                                            ? 'sidebar-menu'
                                            : 'sidebar-menu-small'
                                    }
                                    items={sidebarItems}
                                    expanded={isExtended}
                                />
                                {isExtended && (
                                    <div className="footer">
                                        <span>
                                            If you have any questions, please
                                            <br />
                                            call us:
                                        </span>
                                        <a href="tel:8007709660">
                                            {' '}
                                            (800) 770-9660
                                        </a>
                                    </div>
                                )}
                            </div>
                        </Grid>
                        <Grid
                            item
                            xs={isExtended ? 9 : 11}
                            lg={isExtended ? 9 : 11}
                        >
                            <div className="menu-up">
                                <Grid container className="full-height">
                                    <Grid
                                        item
                                        xs={1}
                                        className="vertical-center full-height"
                                    >
                                        <Reorder
                                            className="pointer"
                                            onClick={this.toggleExtended}
                                        />
                                    </Grid>
                                    <Grid
                                        item
                                        xs={isAdmin ? 10 : 7}
                                        className="vertical-center"
                                    >
                                        Welcome, {loginstore.firstName}
                                    </Grid>

                                    <Grid
                                        item
                                        xs={1}
                                        className="vertical-center full-height"
                                    >
                                        <SettingsIcon />
                                        <span
                                            onClick={this.showLogout}
                                            className="pointer"
                                        >
                                            <LogoutIcon />
                                        </span>
                                    </Grid>
                                    {!isAdmin && (
                                        <Grid item xs={3}>
                                            <NavLink
                                                to={PathAddNewProperty}
                                                activeClassName="new-property-active"
                                            >
                                                <AddNewProperty>
                                                    <AddCircle />
                                                    <span className="padding-left-10">
                                                        Add new property
                                                    </span>
                                                </AddNewProperty>
                                            </NavLink>
                                        </Grid>
                                    )}
                                </Grid>
                            </div>
                            <div className="full-screen-height">
                                <div className="main-content">
                                    {mainContent}
                                </div>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            );
        }
        return componentToReturn;
    }
}

const LayoutObservable = inject('loginstore')(observer(Layout));

export default LayoutObservable;
