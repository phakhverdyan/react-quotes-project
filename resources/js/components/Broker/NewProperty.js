/* eslint-disable react/prop-types */
import React from 'react';
import { observer, inject } from 'mobx-react';
import { Formik } from 'formik';
import { SubHeader, Steppler, ComponentLoading } from '../Common/Common';
import BrokerLayout from '../Layouts/BrokerLayout';
import Form from './PropertyForm';
import { propertyValidationSchema } from './Validations';
import { optionalAccess } from './../../utils/common';
import { Redirect } from 'react-router-dom';
import {GetPathAdminBindQuote, GetPathBrokerViewQuote} from '../../constants';
import { withRouter } from 'react-router';
import { BrokerActivity } from './../../constants';
import { GetPathAdminCreateQuote } from './../../constants';
import { GetPathBrokerPropertySummary } from './../../constants';

export class AddNewPropertyForm extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.isNew = true;
        this.id = null;
        if (this.props.match.params.id) {
            this.isNew = false;
            this.id = this.props.match.params.id;
        }
        this.state = {
            showQuote: false,
            showSummary: false,
            error: null,
            id: this.id
        };
    }

    reset = () => {
        this.setState({
            showQuote: false,
            showSummary: false,
            error: null
        });
    };

    showForm = () => {
        this.setState({
            showQuote: false,
            showSummary: false
        });
    };

    showSummary = response => {
        const propertyId = optionalAccess(
            response,
            'payload.property.id',
            false
        );
        if (propertyId) {
            this.setState({
                showSummary: true,
                id: propertyId
            });
        } else {
            this.showError();
        }
    };

    showError = () => {
        this.setState({
            error: 'There was an error submitting the form'
        });
    };

    onSubmit(values) {
        if (this.props.isAdmin) {
            this.setState({
                showQuote: true,
                id: this.id
            });
            return;
        }
        this.setState({
            error: null
        });
        if (this.isNew) {
            this.props.rootstore.propertyStore.newProperty(
                values,
                this.showSummary,
                this.showError
            );
        } else {
            this.props.rootstore.propertyStore.updateProperty(
                values,
                this.id,
                this.showSummary,
                this.showError
            );
        }
    }
    renderForm = props => {
        const rentalTypes = this.props.rootstore.propertyStore.getRentalTypes.map(
            rt => ({
                label: rt.type,
                value: rt.id
            })
        );
        const buildingConstructionOptions = this.props.rootstore.propertyStore.buildingConstructionTypes.map(
            rt => ({
                label: rt.name,
                value: rt.id
            })
        );
        const constructionTypes = this.props.rootstore.propertyStore.getConstructionTypes.map(
            rt => ({
                label: rt.name,
                value: rt.id
            })
        );

        const additionalProps = {};
        if (this.props.readOnly) {
            additionalProps.readOnly = this.props.readOnly;
        }

        return (
            <Form
                {...props}
                formError={this.state.error}
                rentalTypes={rentalTypes}
                buildingConstructionOptions={buildingConstructionOptions}
                constructionTypes={constructionTypes}
                additionalProps={additionalProps}
                propertyId={this.id}
            />
        );
    };

    OnQuoteClick = () => {
        this.setState({
            showQuote: true,
            id: this.id
        });
    };

    goToBind = () => {
        this.setState({
            goToBind: true
        });
    };


    componentWillMount() {
        if (!this.isNew) {
            this.props.rootstore.propertyStore.getByIdAndFormDefaults(
                this.state.id
            );
        } else {
            this.props.rootstore.propertyStore.getByIdAndFormDefaults();
        }
    }

    areLocalStoresFetching = () => {
        return this.props.rootstore.propertyStore.isFormFetching;
    };

    render() {
        let values = {
            companyName: '',
            companySuite: '',
            companyAddress: '',
            companyCity: '',
            companyState: '',
            companyZip: '',
            companyPhone: '',
            companyContactPerson: '',
            propertyName: '',
            propertySuite: '',
            propertyAddress: '',
            propertyCity: '',
            propertyState: '',
            propertyZip: '',
            propertyPhone: '',
            propertyNumUnits: '',
            propertyNameInsured: '',
            propertyIsThereOnSiteManagment: '',
            propertyRentalsType: '',
            propertyAffordableHousing: '',
            numRentRegulatedUnits: '',
            propertyLossRuns: { name: '' },
            propertyRoofConstruction: '',
            propertyWebsite: '',
            propertyBuildingConstruction: '',
            propertyConstructionType: '',
            propertySoftware: '',
        };

        const items = [
            {
                text: 'Locations',
                onClick: null,
                active: true,
                disabled: true
            },
            {
                text: 'Quote',
                onClick: this.OnQuoteClick,
                active: false,
                disabled: this.isNew
            },
            {
                text: 'Bind',
                onClick: this.goToBind,
                active: false,
                disabled: this.isNew
            }
        ];

        let title = 'Add New Property';
        if (!this.isNew) {
            const property = this.props.rootstore.propertyStore.property;
            const company = property.company || {};
            title = property.property_name;
            values = {
                companyName: company.company_name || '',
                companySuite: company.suite || '',
                companyAddress: company.address || '',
                companyCity: company.city || '',
                companyState: company.state || '',
                companyZip: company.zip || '',
                companyPhone: company.phone_no || '',
                companyContactPerson: company.contact_person || '',
                propertyName: property.property_name || '',
                propertySuite: property.suite || '',
                propertyAddress: property.address || '',
                propertyCity: property.city || '',
                propertyState: property.state || '',
                propertyZip: property.zip || '',
                propertyPhone: property.phone_no || '',
                propertyNumUnits: property.total_units || '',
                propertyNameInsured: property.name_insured || '',
                propertyIsThereOnSiteManagment:
                    property.onsite_management === 1 ? 'true' : 'false',
                propertyRentalsType: property.rental_type_id || '',
                propertyAffordableHousing:
                    property.affordable_units === 1 ? 'true' : 'false',
                numRentRegulatedUnits: property.no_of_affordable_units || '',
                propertyLossRuns: { name: property.loss_runs },
                propertyRoofConstruction: property.roof_construction || '',
                propertyWebsite: property.website || '',
                propertySoftware: property.property_software,
                propertyBuildingConstruction:
                    property.building_construction_type_id || '',
                propertyConstructionType: property.construction_type_id || ''
            };
        }

        const itemsCrumb = [
            {
                url: BrokerActivity,
                text: 'Current Activity'
            },
            {
                url: this.props.location.pathname,
                text: title
            }
        ];

        const quotePath = this.props.isAdmin
            ? GetPathAdminCreateQuote(this.state.id)
            : GetPathBrokerViewQuote(this.state.id);

        const summaryPath = GetPathBrokerPropertySummary(this.state.id);
        const bindPath = GetPathAdminBindQuote(
            this.props.rootstore.propertyStore.property.id
        );
        return (
            <>
                {this.state.showQuote && <Redirect push to={quotePath} />}
                {this.state.showSummary && <Redirect push to={summaryPath} />}
                {this.state.goToBind && <Redirect push to={bindPath} />}
                {this.props.rootstore.propertyStore.isFormFetching && (
                    <ComponentLoading />
                )}
                {!this.props.rootstore.propertyStore.isFormFetching && (
                    <>
                        <Steppler items={items} itemsCrumb={itemsCrumb} />
                        <div className="form-div-expanded">
                            <>
                                <SubHeader>{title}</SubHeader>
                                <p className="main-text text-left margin-bottom-20">
                                    Please, add information about the company,
                                    property, building and Additional
                                    Information (if is it needed)
                                </p>
                                <Formik
                                    render={this.renderForm}
                                    onSubmit={this.onSubmit}
                                    initialValues={values}
                                    validationSchema={propertyValidationSchema}
                                />
                            </>
                        </div>
                    </>
                )}
            </>
        );
    }
}

const BrokerNewPropertyObserver = withRouter(
    inject('rootstore')(observer(AddNewPropertyForm))
);

const BrokerNewProperty = () => (
    <BrokerLayout content={<BrokerNewPropertyObserver />} />
);

export default BrokerNewProperty;
