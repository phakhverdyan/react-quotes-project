import { number, string, object, mixed } from 'yup';

// const phoneRegExp = /^\(?\d{3}[.)]?[- ]?\d{3}[- .]?\d{4}$/;
const zipRegExp = /^[0-9]{5}(?:-[0-9]{4})?$/;

// for validating files use something like this
// propertyLossRuns: mixed().test(
//     'fileSize',
//     'File is required',
//     value => value.size > 0
// ),

export const propertyValidationSchema = object({
    companyName: string(),
    companySuite: string(),
    companyAddress: string(),
    companyCity: string(),
    companyState: string()
        .min(2)
        .max(2),
    companyZip: string()
        .matches(zipRegExp, 'Zip is not valid'),
    companyPhone: string(),
    companyContactPerson: string(),
    propertyName: string().required('Name is required'),
    propertySuite: string(),
    propertyAddress: string().required('Address is required'),
    propertyCity: string().required('City is required'),
    propertyState: string()
        .min(2)
        .max(2)
        .required('State is required'),
    propertyZip: string()
        .matches(zipRegExp, 'Zip is not valid')
        .required('Zip is required'),
    propertyPhone: string().required('Phone is required'),
    propertyNumUnits: number()
        .typeError('Must be a number')
        .positive('Must be a positive number')
        .required('Number of units is required'),
    propertyNameInsured: string().required('Name insured is required'),
    propertyIsThereOnSiteManagment: string().required(
        'Is there on site managment is required'
    ),
    propertyRentalsType: string().required('Rentals type is required'),
    propertyRoofConstruction: string().required(
        'Roof construction is required'
    ),
    propertyBuildingConstruction: string().required(
        'Building construction is required'
    ),
    propertyConstructionType: string().required(
        'Construction type is required'
    ),
    propertyAffordableHousing: string().required(
        'Affordable housing is required'
    ),
    propertySoftware: string(),
    numRentRegulatedUnits: number().when('propertyAffordableHousing', {
        is: val => val === 'true',
        then: number()
            .typeError('Must be a number')
            .required('Number of rent regulated units is required')
    }),
    propertyLossRuns: mixed().test(
        'fileSize',
        'File is required',
        // eslint-disable-next-line no-unused-vars
        value => true
    ),
    propertyWebsite: string(),
});
