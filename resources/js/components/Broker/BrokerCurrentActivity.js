/* eslint-disable react/prop-types */
import React from 'react';
import Grid from '@material-ui/core/Grid';
import CustomTable from '../Table';
import { SearchInput } from './../Form/TextInput';
import { observer, inject } from 'mobx-react';
import { ComponentLoading, getAmericanDate } from '../Common/Common';
import BrokerLayout from './../Layouts/BrokerLayout';
import { Redirect } from 'react-router-dom';
import { GetPathBrokerViewQuote } from './../../constants';
import TableAbstact from '../TableAbstract';

class Content extends TableAbstact {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            clickedOn: null
        };
        this.store = this.props.rootstore.propertyStore;
        this.rows = [
            {
                id: 'property_name',
                label: 'Property Name',
                type: 'link'
            },
            {
                id: 'created_at',
                label: 'Create Date'
            },
            { id: 'quote', label: 'Quote #' },
            { id: 'status', label: 'Status' },
            { id: 'state', label: 'State' }
        ];
    }

    componentDidMount() {
        this.store.fetchProperties();
    }

    getData = () => {
        const lowerCaseSearch = this.state.search.toLowerCase();
        let rows = this.store.properties.map(b => ({
            id: b.id,
            property_name: this.getLinkStyle(b.property_name),
            created_at: getAmericanDate(b.created_at),
            quote: b.quote_id,
            status: this.getStatus(b.status),
            state: b.state
        }));

        if (lowerCaseSearch !== '') {
            rows = rows.filter(o =>
                `${o.property_name.props.children} ${o.created_at} ${
                    o.status.props.children
                } ${o.state} ${o.quote}`
                    .toLowerCase()
                    .includes(lowerCaseSearch)
            );
        }

        return rows;
    };

    render() {
        const quotePath = GetPathBrokerViewQuote(this.state.clickedOn);
        return (
            <Grid container>
                <Grid item xs={9}>
                    <h1 className="content-title">Current Activity</h1>
                    {this.state.clickedOn && <Redirect push to={quotePath} />}
                </Grid>
                <Grid item xs={3}>
                    <SearchInput
                        className="search-input"
                        value={this.state.search}
                        onChange={this.handleChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    {this.store.isFetching ? (
                        <ComponentLoading />
                    ) : (
                        <CustomTable
                            rows={this.rows}
                            data={this.getData()}
                            orderBy="property_name"
                            order="asc"
                            rowsPerPage={25}
                            onColClick={this.onColClick}
                            onRowClick={this.onRowClick}
                        />
                    )}
                </Grid>
            </Grid>
        );
    }
}

const ContentObservable = inject('rootstore')(observer(Content));

const BrokerCurrentActivity = () => (
    <BrokerLayout content={<ContentObservable />} />
);

export default BrokerCurrentActivity;
