/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { observer, inject } from 'mobx-react';
import Grid from '@material-ui/core/Grid';
import {
    CustomGridRow,
    Steppler,
    ComponentLoading,
    CustomError
} from '../Common/Common';
import { SubHeader } from './../Common/Common';
import { InfoIcon, EllipseIcon, CustomClockIcon } from './../Icons';
import { AdminSubmitButton } from '../Form/Buttons';
import BrokerLayout from '../Layouts/BrokerLayout';
import { BrokerActivity, GetPathBrokerViewProperty } from './../../constants';
import { SuccesModal } from '../Common/Dialog';
import { Link } from 'react-router-dom';
import { SubmittedWaitingMessage } from './../Common/Common';
import { Redirect } from 'react-router-dom';
import { withRouter } from 'react-router';
import RequestInformation from './RequestInformation';

const SuccessModal = props => {
    const content = (
        <>
            <span className="in-the-center success-text">{props.title}</span>
            <p className="modal-body">{props.content}</p>
            <span className="in-the-center">
                <Link to={BrokerActivity}>
                    <AdminSubmitButton>Continue</AdminSubmitButton>
                </Link>
            </span>
        </>
    );
    return <SuccesModal content={content} />;
};

const SuccessInviteModal = () => (
    <SuccessModal
        title="BIND REQUESTED"
        content="You will receive a notification once bind is completed"
    />
);

const SuccessNoteModal = () => (
    <SuccessModal title="NOTE ADDED" content=" " />
);

class ViewQuotePanel extends React.Component {
    onSelect = () => {
        //dont allow to change options if borker bound the quote RLL-3532
        if(this.props.property.status.name!='broker-bound')
        {
            this.props.onSelected(this.props.quoteId);
        }
    };

    render() {
        const {
            option,
            program,
            coverage,
            enhacements,
            price,
            selected,
            property
        } = this.props;
        let enhacementsText = 'No Enhancements';
        if (enhacements) {
            enhacementsText = 'Added Enhancements';
        }
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2
        });

        const className = selected
            ? 'quote-view-selected'
            : 'quote-panel quote-view';

        const coverageCurrency = formatter.format(coverage);
        console.log(property);
        return (
            <div className={className} onClick={this.onSelect}>
                <div className="info-icon">
                    <InfoIcon />
                </div>
                <h3>Option {option}</h3>
                <hr className="extra-margin" />
                <p>
                    Program <strong>{program}</strong>
                </p>
                <EllipseIcon />
                <p>
                    Personal Coverage
                    <br />
                    <strong>{coverageCurrency}</strong>
                </p>
                <EllipseIcon />
                <p className="extra-padding">
                    <strong>{enhacementsText}</strong>
                </p>
                <hr className="extra-margin" />
                <p className="total">Total price</p>
                <h4>${price}</h4>
                <div className="info-text">
                    <span>
                        Subject to approval and loss runs review
                    </span>
                </div>
            </div>
        );
    }
}

class ShowQuotesBroker extends React.Component {


    onQuoteSelected = option => {
        this.resetError();
        this.setState({
            selected: option
        });
    };

    showSuccessModal = () => {
        this.setState({
            showModal: true
        });
    };
    showNoteModal = () => {
        this.setState({
            showNoteModal: true
        });
    };
    showError = () => {
        this.setState({
            error: 'There was a problem sending the option'
        });
    };
    resetError = () => {
        this.setState({
            error: null
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            selected: 0,
            showModal: false,
            showNoteModal: false,
            error: null
        };

        const quoteSelected = props.quotes.find(q => q.selected > 0);

        if( quoteSelected )
        {
            this.state.selected = quoteSelected.id;
        }


    }

    onSubmit = () => {
        this.resetError();
        this.props.rootstore.quoteStore.submitQuoteBroker(
            {
                quoteId: this.state.selected
            },
            this.props.property.id,
            this.showSuccessModal,
            this.showError
        );
    };

    render() {
        const {
            property,
            quotes,
            pdlwOptionTypes,
            personalCoverageTypes,
            enhancementsTypes
        } = this.props;

        const quoteStoreIsFetching = this.props.rootstore.quoteStore.isFetching;

        const getCoverage = id => personalCoverageTypes.find(c => c.id === id);

        const hasEnhancements = id =>
            parseFloat(enhancementsTypes.find(c => c.id === id).price) > 0;

        const getPdlwOption = id => pdlwOptionTypes.find(c => c.id === id).name;

        const enhancementWithPrice =
            enhancementsTypes.find(c => parseFloat(c.price) > 0) || {};

        const enhancementPrice = parseFloat(enhancementWithPrice.price) || 0;

        const getTotalPrice = q => {
            let price = parseFloat(q.base_price);
            const coveragePrice = parseFloat(
                getCoverage(q.personal_coverage_id).price
            );
            price += coveragePrice;
            if (hasEnhancements(q.enhancement_id)) {
                price += enhancementPrice;
            }
            return price.toFixed(2);
        };

        return (
            <div className="form-div-expanded">
                <SubHeader>{property.property_name}</SubHeader>
                {this.state.showModal && <SuccessInviteModal />}
                {this.state.showNoteModal && <SuccessNoteModal />}
                <div className="main-text  margin-bottom-20 bottom-dashed-line">
                    <div className="text-left">Please, Choose One of this Option</div>
                    <div className="text-right">Quote Number {property.quote_id}</div>
                </div>

                <Grid container>
                    <CustomGridRow>
                        {quotes.map((q, i) => (
                            <Grid item xs={4} key={q.id} className="in-the-center-with-margin">
                                <ViewQuotePanel
                                    option={1 + i}
                                    quoteId={q.id}
                                    program={getPdlwOption(q.pdlw_option_id)}
                                    coverage={parseFloat(
                                        getCoverage(q.personal_coverage_id)
                                            .amount
                                    )}
                                    property={property}
                                    enhacements={hasEnhancements(
                                        q.enhancement_id
                                    )}
                                    price={getTotalPrice(q)}
                                    onSelected={this.onQuoteSelected}
                                    selected={this.state.selected === q.id}
                                />
                            </Grid>
                        ))}
                    </CustomGridRow>
                </Grid>
                <div className="quote-submit">
                    <Grid container>
                        <CustomGridRow>
                            <Grid item xs={9} />
                            <Grid item xs={3}>
                                {this.state.error && this.state.error !== '' && (
                                    <CustomError
                                        name="formError"
                                        className="margin-bottom-20"
                                    >
                                        {this.state.error}
                                    </CustomError>
                                )}
                                <AdminSubmitButton
                                    onClick={this.onSubmit}
                                    id="quote-submit-button"
                                    disabled={
                                        this.state.selected < 1 ||
                                        quoteStoreIsFetching
                                    }
                                >
                                    Request Bind
                                </AdminSubmitButton>
                            </Grid>
                        </CustomGridRow>
                    </Grid>
                    <div className="add-note">
                        <RequestInformation
                            id={this.props.property.id}
                            submitted={this.showNoteModal}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const ShowQuoteBrokerObserver = inject('rootstore')(observer(ShowQuotesBroker));

export const QuoteWaitingSubmitted = () => {
    const [redirectToActivities, setRedirect] = useState(false);

    if (redirectToActivities) {
        return <Redirect push to={BrokerActivity} />;
    }
    const ContinueButton = (
        <AdminSubmitButton type="button" onClick={() => setRedirect(true)}>
            Continue
        </AdminSubmitButton>
    );
    const submittedComponent = (
        <SubmittedWaitingMessage
            icon={<CustomClockIcon />}
            button={ContinueButton}
        >
            Submitted
        </SubmittedWaitingMessage>
    );

    return submittedComponent;
};
class QuoteBroker extends React.Component {
    constructor(props) {
        super(props);

        this.id = null;
        if (this.props.match.params.id) {
            this.isNew = false;
            this.id = this.props.match.params.id;
        }
        this.state = {
            showForm: false,
            error: null,
            id: this.id
        };
    }
    showForm = () => {
        this.setState({
            showForm: true
        });
    };
    componentWillMount() {
        this.props.rootstore.quoteStore.fetchPropertyQuotes(this.state.id);
        this.props.rootstore.propertyStore.getById(this.state.id);
    }

    areLocalStoresFetching = () => {
        return (
            this.props.rootstore.propertyStore.isPropertyByIdFetching ||
            this.props.rootstore.quoteStore.isFetchingDefaultQuotes
        );
    };

    render() {
        const { propertyStore, quoteStore } = this.props.rootstore;
        const property = propertyStore.property;
        const items = [
            {
                text: 'Locations',
                onClick: this.showForm,
                active: false,
                disabled: false
            },
            {
                text: 'Quote',
                onClick: null,
                active: true,
                disabled: true
            }
        ];
        const itemsCrumb = [
            {
                url: BrokerActivity,
                text: 'Current Activity'
            },
            {
                url: this.props.location.pathname,
                text: property.property_name || ''
            }
        ];
        const quotesReady = quoteStore.propertyQuotes.length > 0;

        const pdlwOptionTypes = quoteStore.pdlwOptionTypes;
        const basePriceTypes = quoteStore.basePriceTypes;
        const personalCoverageTypes = quoteStore.personalCoverageTypes;
        const enhancementsTypes = quoteStore.enhancementsTypes;

        const formPath = GetPathBrokerViewProperty(property.id);
        return (
            <>
                {this.areLocalStoresFetching() && <ComponentLoading />}
                {!this.areLocalStoresFetching() && (
                    <>
                        <Steppler items={items} itemsCrumb={itemsCrumb} />
                        {this.state.showForm && <Redirect push to={formPath} />}
                        {!quotesReady && <QuoteWaitingSubmitted />}
                        {quotesReady && (
                            <ShowQuoteBrokerObserver
                                quotes={quoteStore.propertyQuotes}
                                pdlwOptionTypes={pdlwOptionTypes}
                                personalCoverageTypes={personalCoverageTypes}
                                enhancementsTypes={enhancementsTypes}
                                basePriceTypes={basePriceTypes}
                                property={property}
                            />
                        )}
                    </>
                )}
            </>
        );
    }
}

const QuoteBrokerObserver = withRouter(
    inject('rootstore')(observer(QuoteBroker))
);

const QuoteBrokerWithLayout = () => (
    <BrokerLayout content={<QuoteBrokerObserver />} />
);

export default QuoteBrokerWithLayout;
