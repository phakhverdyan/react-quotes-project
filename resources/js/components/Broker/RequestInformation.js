/* eslint-disable react/prop-types */
import React from 'react';
import { CustomError } from './../Common/Common';
import { CustomTextInput } from '../Form/TextInput';
import { AdminSubmitButton } from '../Form/Buttons';
import { Formik } from 'formik';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import LoginStore from "../../stores/LoginStore";

const Form = props => {
    {
        const change = (name, e) => {
            e.persist();
            props.handleChange(e);
            props.setFieldTouched(name, true, false);
        };
        const preSubmit = e => {
            e.preventDefault();
            props.submitForm(e);
        };
        return (
            <>
                <p>
                    If you have any questions, please{' '}
                    <span>REQUEST ADDITIONAL INFORMATION</span>
                </p>
                <form onSubmit={preSubmit} autoComplete="nope">
                    <div id="submission-textarea">
                        <div id="submission-input">
                            <CustomTextInput
                                multiline
                                fullWidth
                                rowsMax={4}
                                rows={4}
                                name="note"
                                id="note"
                                onChange={change.bind(null, 'note')}
                                placeholder="Please, write a note ...
                            (this note will be added to the Request )"
                                value={props.values.note}
                                className="no-margin-bottom"
                            />


                        </div>
                        <div>
                            {props.error && props.error !== '' && (
                                <CustomError
                                    name="formError"
                                    className="in-the-center margin-bottom-20"
                                >
                                    {props.error}
                                </CustomError>
                            )}
                            <AdminSubmitButton
                                type="submit"
                                id="note"
                                disabled={
                                    props.disabled || props.values.note === ''
                                }
                            >
                                Send
                            </AdminSubmitButton>
                        </div>
                    </div>
                </form>
            </>
        );
    }
};

class RequestInformation extends React.Component {
    state = {
        error: null
    };
    constructor(props)
    {
        super(props)
    }

    submitted = () => this.props.submitted();
    showError = () =>
        this.setState({ error: 'There was an error submitting the form' });

    render() {
        const { rootstore, loginstore } = this.props;
        const id = this.props.id;
        const initialValues = {
            note: ''
        };

        const onSubmitSummary = values => {
            const isAdmin = this.props.loginstore.isAdmin;
            console.log("ISLOGGED IN");
            console.log(isAdmin);
            console.log(values);
            this.setState({
                error: null
            });

            values.isAdmin = isAdmin;
            rootstore.propertyStore.postPropertySummary(
                values,
                id,
                this.submitted,
                this.showError
            );
        };
        const disabled = rootstore.propertyStore.isSubmittingPropertySummary;
        return (
            <Formik
                render={props => (
                    <Form
                        {...props}
                        rootstore={rootstore}
                        error={this.state.error}
                        disabled={disabled}
                    />
                )}
                onSubmit={onSubmitSummary}
                initialValues={initialValues}
            />
        );
    }
}

const RequestInformationObserver = withRouter(
    inject('rootstore','loginstore')(observer(RequestInformation))
);

export default RequestInformationObserver;
