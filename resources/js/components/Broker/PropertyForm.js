/* eslint-disable indent */
/* eslint-disable react/prop-types */
import React from 'react';
import {observer, inject} from 'mobx-react';
import {CustomTextInput} from './../Form/TextInput';
import {AdminSubmitButton} from './../Form/Buttons';
import {
    CustomSelectInput,
    CustomRadio,
    CustomLabel,
    CustomUpload
} from './../Form/TextInput';
import Grid from '@material-ui/core/Grid';
import {
    CustomExpansionPanel,
    CustomGridRow,
    CustomError
} from './../Common/Common';
import {UsStates} from '../../constants';
import {PdfIcon, IconDeleteX} from './../Icons';
import {Link} from 'react-router-dom';

class Form extends React.Component {
    change = e => {
        e.persist();
        this.props.handleChange(e);
        this.props.setFieldTouched(e.target.name, true, false);
    };

    preSubmit = e => {
        e.preventDefault();
        this.props.submitForm(e);
    };

    render() {
        const props = this.props;
        const {
            values: {
                companyName,
                companySuite,
                companyAddress,
                companyCity,
                companyState,
                companyZip,
                companyPhone,
                companyContactPerson,
                propertyName,
                propertySuite,
                propertyAddress,
                propertyCity,
                propertyState,
                propertyZip,
                propertyPhone,
                propertyNumUnits,
                propertyNameInsured,
                propertyIsThereOnSiteManagment,
                propertyRentalsType,
                propertyAffordableHousing,
                propertyLossRuns,
                propertySoftware,
                numRentRegulatedUnits,
                propertyRoofConstruction,
                propertyWebsite,
                propertyBuildingConstruction,
                propertyConstructionType
            },
            errors,
            touched,
            setFieldTouched,
            setFieldValue,
            submitCount,
            formError,
            rentalTypes,
            buildingConstructionOptions,
            constructionTypes,
            additionalProps,
            propertyId
        } = props;

        let readOnlyProps = {};

        if (additionalProps && additionalProps.readOnly) {
            readOnlyProps = {disabled: true};
        }

        const changeSelect = (name, option) => {
            setFieldTouched(name, true, false);
            setFieldValue(name, option.value);
        };
        const changeFile = (name, file) => {
            setFieldTouched(name, true, false);
            setFieldValue(name, file);
        };

        const removeFile = name => {
            setFieldValue(name, {file: {name: ''}});
        };

        const hasFile = propertyLossRuns && propertyLossRuns.name;
        let name = hasFile ? propertyLossRuns.name : '';
        const isFileLink = propertyId && hasFile && !propertyLossRuns.size;
        const fileRemotePath = hasFile
            ? '/broker/property/' + propertyId + '/loss-runs/'
            : '';
        if (isFileLink) {
            name = name.replace(/^.*(\\|\/|:)/, '');
        }
        const icon =
            hasFile && name.toLowerCase().includes('.pdf') ? <PdfIcon/> : '';
        const fileDisplay = (
            <div className="extension-panel-inner file-display">
                {isFileLink && (
                    <Link to={fileRemotePath} target="_blank">
                        {icon} {name}
                    </Link>
                )}
                {!isFileLink && (
                    <>
                        {icon} {name}
                    </>
                )}
                <span
                    onClick={removeFile.bind(null, 'propertyLossRuns')}
                    className="pointer"
                >
                    <IconDeleteX/>
                </span>
            </div>
        );

        return (
            <form onSubmit={this.preSubmit} autoComplete="off">
                <input
                    autoComplete="nope"
                    name="hidden"
                    type="text"
                    style={{display: 'none'}}
                />

                <CustomExpansionPanel title="1. Property Information">
                    <Grid container>
                        <CustomGridRow>
                            <Grid item xs={8}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyName}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyName
                                            ? errors.propertyName
                                            : ''
                                    }
                                    error={
                                        touched.propertyName &&
                                        Boolean(errors.propertyName)
                                    }
                                    id="propertyName"
                                    name="propertyName"
                                    placeholder="Property Name"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={5}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyAddress}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyAddress
                                            ? errors.propertyAddress
                                            : ''
                                    }
                                    error={
                                        touched.propertyAddress &&
                                        Boolean(errors.propertyAddress)
                                    }
                                    id="propertyAddress"
                                    name="propertyAddress"
                                    placeholder="Address"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <CustomTextInput
                                    type="text"
                                    value={propertySuite}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertySuite
                                            ? errors.propertySuite
                                            : ''
                                    }
                                    error={
                                        touched.propertySuite &&
                                        Boolean(errors.propertySuite)
                                    }
                                    id="propertySuite"
                                    name="propertySuite"
                                    placeholder="Suite"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={5}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyCity}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyCity
                                            ? errors.propertyCity
                                            : ''
                                    }
                                    error={
                                        touched.propertyCity &&
                                        Boolean(errors.propertyCity)
                                    }
                                    id="propertyCity"
                                    name="propertyCity"
                                    placeholder="City"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <CustomSelectInput
                                    type="text"
                                    value={propertyState}
                                    onChange={changeSelect.bind(
                                        null,
                                        'propertyState'
                                    )}
                                    error={
                                        touched.propertyState &&
                                        Boolean(errors.propertyState)
                                    }
                                    id="propertyState"
                                    name="propertyState"
                                    options={UsStates}
                                    placeholder="State"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                                {errors.propertyState &&
                                touched.propertyState && (
                                    <CustomError name="propertyState">
                                        {errors.propertyState}
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={3}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyZip}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyZip
                                            ? errors.propertyZip
                                            : ''
                                    }
                                    error={
                                        touched.propertyZip &&
                                        Boolean(errors.propertyZip)
                                    }
                                    id="propertyZip"
                                    name="propertyZip"
                                    placeholder="Zip"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                            <Grid item xs={5}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyPhone}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyPhone
                                            ? errors.propertyPhone
                                            : ''
                                    }
                                    error={
                                        touched.propertyPhone &&
                                        Boolean(errors.propertyPhone)
                                    }
                                    id="propertyPhone"
                                    name="propertyPhone"
                                    placeholder="Phone"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={3}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyNumUnits}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyNumUnits
                                            ? errors.propertyNumUnits
                                            : ''
                                    }
                                    error={
                                        touched.propertyNumUnits &&
                                        Boolean(errors.propertyNumUnits)
                                    }
                                    id="propertyNumUnits"
                                    name="propertyNumUnits"
                                    placeholder="Number of Units"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                            <Grid item xs={5}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyNameInsured}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyNameInsured
                                            ? errors.propertyNameInsured
                                            : ''
                                    }
                                    error={
                                        touched.propertyNameInsured &&
                                        Boolean(errors.propertyNameInsured)
                                    }
                                    id="propertyNameInsured"
                                    name="propertyNameInsured"
                                    placeholder="Name Insured (Owners Name)"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={8}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyWebsite}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyWebsite
                                            ? errors.propertyWebsite
                                            : ''
                                    }
                                    error={
                                        touched.propertyWebsite &&
                                        Boolean(errors.propertyWebsite)
                                    }
                                    id="propertyWebsite"
                                    name="propertyWebsite"
                                    placeholder="Property website"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow className="margin-bottom-20">
                            <Grid
                                item
                                xs={4}
                                className="vertical-center height-75"
                            >
                                <span className="extension-panel-inner">
                                    Is there on-site management?
                                </span>
                            </Grid>
                            <Grid item xs={5} className="text-left">
                                <CustomRadio
                                    value="true"
                                    onChange={this.change}
                                    checked={
                                        propertyIsThereOnSiteManagment ===
                                        'true'
                                    }
                                    id="propertyIsThereOnSiteManagment"
                                    name="propertyIsThereOnSiteManagment"
                                    {...readOnlyProps}
                                />
                                <CustomLabel
                                    htmlFor="my-input"
                                    className="extension-panel-inner"
                                >
                                    YES
                                </CustomLabel>
                                <CustomRadio
                                    value="false"
                                    onChange={this.change}
                                    checked={
                                        propertyIsThereOnSiteManagment ===
                                        'false'
                                    }
                                    id="propertyIsThereOnSiteManagment"
                                    name="propertyIsThereOnSiteManagment"
                                    {...readOnlyProps}
                                />
                                <CustomLabel
                                    htmlFor="my-input"
                                    className="extension-panel-inner"
                                >
                                    NO
                                </CustomLabel>
                                {errors.propertyIsThereOnSiteManagment &&
                                touched.propertyIsThereOnSiteManagment && (
                                    <CustomError name="propertyIsThereOnSiteManagment">
                                        {
                                            errors.propertyIsThereOnSiteManagment
                                        }
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow className="margin-bottom-20">
                            <Grid
                                item
                                xs={4}
                                className="vertical-center height-75"
                            >
                                <span className="extension-panel-inner">
                                    Affordable Housing
                                </span>
                            </Grid>
                            <Grid item xs={5} className="text-left">
                                <CustomRadio
                                    value="true"
                                    onChange={this.change}
                                    checked={
                                        propertyAffordableHousing === 'true'
                                    }
                                    id="propertyAffordableHousing"
                                    name="propertyAffordableHousing"
                                    {...readOnlyProps}
                                />
                                <CustomLabel
                                    htmlFor="my-input"
                                    className="extension-panel-inner"
                                >
                                    YES
                                </CustomLabel>
                                <CustomRadio
                                    value="false"
                                    onChange={this.change}
                                    checked={
                                        propertyAffordableHousing === 'false'
                                    }
                                    id="propertyAffordableHousing"
                                    name="propertyAffordableHousing"
                                    {...readOnlyProps}
                                />
                                <CustomLabel
                                    htmlFor="my-input"
                                    className="extension-panel-inner"
                                >
                                    NO
                                </CustomLabel>
                                {errors.propertyAffordableHousing &&
                                touched.propertyAffordableHousing && (
                                    <CustomError name="propertyAffordableHousing">
                                        {errors.propertyAffordableHousing}
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>
                        {propertyAffordableHousing === 'true' && (
                            <CustomGridRow className="margin-bottom-20">
                                <Grid item xs={8}>
                                    <CustomTextInput
                                        type="text"
                                        value={numRentRegulatedUnits}
                                        onChange={this.change}
                                        helperText={
                                            touched.numRentRegulatedUnits
                                                ? errors.numRentRegulatedUnits
                                                : ''
                                        }
                                        error={
                                            touched.numRentRegulatedUnits &&
                                            Boolean(
                                                errors.numRentRegulatedUnits
                                            )
                                        }
                                        id="numRentRegulatedUnits"
                                        name="numRentRegulatedUnits"
                                        placeholder="Number of rent regulated units"
                                        fullWidth
                                        {...readOnlyProps}
                                    />
                                </Grid>
                            </CustomGridRow>
                        )}

                        <CustomGridRow className="margin-bottom-20">
                            <Grid
                                item
                                xs={4}
                                className="vertical-center height-100"
                            >
                                <span className="extension-panel-inner">
                                    Property Loss Runs
                                </span>
                            </Grid>
                            <Grid
                                item
                                xs={5}
                                className="text-left vertical-center"
                            >
                                <CustomUpload
                                    onChange={changeFile.bind(
                                        null,
                                        'propertyLossRuns'
                                    )}
                                    name="propertyLossRuns"
                                    value={propertyLossRuns}
                                />
                                {errors.propertyLossRuns &&
                                touched.propertyLossRuns && (
                                    <CustomError name="propertyLossRuns">
                                        {errors.propertyLossRuns}
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>
                        {hasFile && (
                            <CustomGridRow className="margin-bottom-20">
                                <Grid
                                    item
                                    xs={9}
                                    className="vertical-center height-75"
                                >
                                    {fileDisplay}
                                </Grid>
                            </CustomGridRow>
                        )}

                        <CustomGridRow className="margin-bottom-20">
                            <Grid item xs={8} className="text-left">
                                <CustomSelectInput
                                    type="text"
                                    value={propertyRentalsType}
                                    onChange={changeSelect.bind(
                                        null,
                                        'propertyRentalsType'
                                    )}
                                    error={
                                        touched.propertyRentalsType &&
                                        Boolean(errors.propertyRentalsType)
                                    }
                                    id="propertyRentalsType"
                                    name="propertyRentalsType"
                                    options={rentalTypes}
                                    placeholder="Rental Type"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                                {errors.propertyRentalsType &&
                                touched.propertyRentalsType && (
                                    <CustomError name="propertyRentalsType">
                                        {errors.propertyRentalsType}
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>

                        <CustomGridRow className="margin-bottom-20">
                            <Grid item xs={8} className="text-left">
                                <CustomSelectInput
                                    type="text"
                                    value={propertyConstructionType}
                                    onChange={changeSelect.bind(
                                        null,
                                        'propertyConstructionType'
                                    )}
                                    error={
                                        touched.propertyConstructionType &&
                                        Boolean(errors.propertyConstructionType)
                                    }
                                    id="propertyConstructionType"
                                    name="propertyConstructionType"
                                    options={constructionTypes}
                                    placeholder="Construction Type"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                                {errors.propertyConstructionType &&
                                touched.propertyConstructionType && (
                                    <CustomError name="propertyConstructionType">
                                        {errors.propertyConstructionType}
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>

                        <CustomGridRow className="margin-bottom-20">
                            <Grid item xs={8} className="text-left">
                                <CustomSelectInput
                                    type="text"
                                    value={propertyBuildingConstruction}
                                    onChange={changeSelect.bind(
                                        null,
                                        'propertyBuildingConstruction'
                                    )}
                                    error={
                                        touched.propertyBuildingConstruction &&
                                        Boolean(
                                            errors.propertyBuildingConstruction
                                        )
                                    }
                                    id="propertyBuildingConstruction"
                                    name="propertyBuildingConstruction"
                                    options={buildingConstructionOptions}
                                    placeholder="Building Construction"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                                {errors.propertyBuildingConstruction &&
                                touched.propertyBuildingConstruction && (
                                    <CustomError name="propertyBuildingConstruction">
                                        {
                                            errors.propertyBuildingConstruction
                                        }
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>

                        <CustomGridRow>
                            <Grid item xs={8}>
                                <CustomTextInput
                                    type="text"
                                    value={propertyRoofConstruction}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertyRoofConstruction
                                            ? errors.propertyRoofConstruction
                                            : ''
                                    }
                                    error={
                                        touched.propertyRoofConstruction &&
                                        Boolean(errors.propertyRoofConstruction)
                                    }
                                    id="propertyRoofConstruction"
                                    name="propertyRoofConstruction"
                                    placeholder="Roof Construction"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>


                        <CustomGridRow>
                            <Grid item xs={8} className="text-right">
                                <span className="required-text">
                                    * All fields are required
                                </span>
                            </Grid>
                        </CustomGridRow>
                    </Grid>
                </CustomExpansionPanel>
                <CustomExpansionPanel title="2. Management Company Information">
                    <Grid container>
                        <CustomGridRow>
                            <Grid item xs={8}>
                                <CustomTextInput
                                    type="text"
                                    value={companyName}
                                    onChange={this.change}
                                    helperText={
                                        touched.companyName
                                            ? errors.companyName
                                            : ''
                                    }
                                    error={
                                        touched.companyName &&
                                        Boolean(errors.companyName)
                                    }
                                    id="companyName"
                                    name="companyName"
                                    placeholder="Managment Company Name"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={5}>
                                <CustomTextInput
                                    type="text"
                                    value={companyAddress}
                                    onChange={this.change}
                                    helperText={
                                        touched.companyAddress
                                            ? errors.companyAddress
                                            : ''
                                    }
                                    error={
                                        touched.companyAddress &&
                                        Boolean(errors.companyAddress)
                                    }
                                    id="companyAddress"
                                    name="companyAddress"
                                    placeholder="Street"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <CustomTextInput
                                    type="text"
                                    value={companySuite}
                                    onChange={this.change}
                                    helperText={
                                        touched.companySuite
                                            ? errors.companySuite
                                            : ''
                                    }
                                    error={
                                        touched.companySuite &&
                                        Boolean(errors.companySuite)
                                    }
                                    id="companySuite"
                                    name="companySuite"
                                    placeholder="Suite"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={5}>
                                <CustomTextInput
                                    type="text"
                                    value={companyCity}
                                    onChange={this.change}
                                    helperText={
                                        touched.companyCity
                                            ? errors.companyCity
                                            : ''
                                    }
                                    error={
                                        touched.companyCity &&
                                        Boolean(errors.companyCity)
                                    }
                                    id="companyCity"
                                    name="companyCity"
                                    placeholder="City"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <CustomSelectInput
                                    type="text"
                                    value={companyState}
                                    onChange={changeSelect.bind(
                                        null,
                                        'companyState'
                                    )}
                                    error={
                                        touched.companyState &&
                                        Boolean(errors.companyState)
                                    }
                                    id="companyState"
                                    name="companyState"
                                    options={UsStates}
                                    placeholder="State"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                                {errors.companyState &&
                                touched.companyState && (
                                    <CustomError name="companyState">
                                        {errors.companyState}
                                    </CustomError>
                                )}
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={3}>
                                <CustomTextInput
                                    type="text"
                                    value={companyZip}
                                    onChange={this.change}
                                    helperText={
                                        touched.companyZip
                                            ? errors.companyZip
                                            : ''
                                    }
                                    error={
                                        touched.companyZip &&
                                        Boolean(errors.companyZip)
                                    }
                                    id="companyZip"
                                    name="companyZip"
                                    placeholder="Zip"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                            <Grid item xs={5}>
                                <CustomTextInput
                                    type="text"
                                    value={companyPhone}
                                    onChange={this.change}
                                    helperText={
                                        touched.companyPhone
                                            ? errors.companyPhone
                                            : ''
                                    }
                                    error={
                                        touched.companyPhone &&
                                        Boolean(errors.companyPhone)
                                    }
                                    id="companyPhone"
                                    name="companyPhone"
                                    placeholder="Phone"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={8}>
                                <CustomTextInput
                                    type="text"
                                    value={companyContactPerson}
                                    onChange={this.change}
                                    helperText={
                                        touched.companyContactPerson
                                            ? errors.companyContactPerson
                                            : ''
                                    }
                                    error={
                                        touched.companyContactPerson &&
                                        Boolean(errors.companyContactPerson)
                                    }
                                    id="companyContactPerson"
                                    name="companyContactPerson"
                                    placeholder="Contact Person"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                        <CustomGridRow>
                            <Grid item xs={8}>
                                <CustomTextInput
                                    type="text"
                                    value={propertySoftware}
                                    onChange={this.change}
                                    helperText={
                                        touched.propertySoftware
                                            ? errors.propertySoftware
                                            : ''
                                    }
                                    error={
                                        touched.propertySoftware &&
                                        Boolean(errors.propertySoftware)
                                    }
                                    id="propertySoftware"
                                    name="propertySoftware"
                                    placeholder="Property Software"
                                    fullWidth
                                    {...readOnlyProps}
                                />
                            </Grid>
                        </CustomGridRow>
                    </Grid>
                </CustomExpansionPanel>

                {formError && formError !== '' && (
                    <CustomError
                        name="formError"
                        className="in-the-center margin-bottom-20"
                    >
                        {formError}
                    </CustomError>
                )}
                {Object.keys(errors).length > 0 && submitCount > 0 && (
                    <CustomError
                        name="submitCount"
                        className="in-the-center margin-bottom-20"
                    >
                        Please fill all the required fields
                    </CustomError>
                )}

                <AdminSubmitButton
                    type="submit"
                    disabled={props.rootstore.propertyStore.isFetching}
                >
                    {props.rootstore.propertyStore.isFetching
                        ? 'Submitting'
                        : 'Continue'}
                </AdminSubmitButton>
            </form>
        );
    }
}

export default inject('rootstore')(observer(Form));
