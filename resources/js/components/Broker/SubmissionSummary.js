/* eslint-disable react/prop-types */
import React from 'react';
import { SubHeader, CustomError } from './../Common/Common';
import { CustomTextInput } from '../Form/TextInput';
import { AdminSubmitButton } from '../Form/Buttons';
import { Formik } from 'formik';
import BrokerLayout from './../Layouts/BrokerLayout';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { GetPathBrokerViewQuote } from './../../constants';
import { Redirect } from 'react-router-dom';

const Form = props => {
    {
        const change = (name, e) => {
            e.persist();
            props.handleChange(e);
            props.setFieldTouched(name, true, false);
        };
        const preSubmit = e => {
            e.preventDefault();
            props.submitForm(e);
        };
        return (
            <form onSubmit={preSubmit} autoComplete="nope">
                <div id="submission-textarea">
                    <div id="submission-input">
                        <CustomTextInput
                            multiline
                            fullWidth
                            rowsMax={4}
                            rows={4}
                            name="note"
                            id="note"
                            onChange={change.bind(null, 'note')}
                            placeholder="Please, write a note ...
                            (this note will be added to the Request )"
                            value={props.values.note}
                            className="no-margin-bottom"
                        />
                    </div>
                    <div>
                        {props.error && props.error !== '' && (
                            <CustomError
                                name="formError"
                                className="in-the-center margin-bottom-20"
                            >
                                {props.error}
                            </CustomError>
                        )}
                        <AdminSubmitButton
                            type="submit"
                            disabled={props.disabled}
                        >
                            Request Quote
                        </AdminSubmitButton>
                    </div>
                </div>
            </form>
        );
    }
};

class SubmissionSumary extends React.Component {
    state = {
        showQuote: false,
        error: null
    };

    showQuote = () => this.setState({ showQuote: true });
    showError = () =>
        this.setState({ error: 'There was an error submitting the form' });

    render() {
        const { rootstore, match, loginstore } = this.props;
        const id = match.params.id;
        const initialValues = {
            note: ''
        };
        const onSubmitSummary = values => {
            this.setState({
                error: null
            });
            values.isAdmin = this.props.loginstore.isAdmin;
            rootstore.propertyStore.postPropertySummary(
                values,
                id,
                this.showQuote,
                this.showError
            );
        };
        const quotePath = GetPathBrokerViewQuote(id);
        const disabled = rootstore.propertyStore.isSubmittingPropertySummary;
        return (
            <div className="form-div-expanded" id="submission-summary">
                {this.state.showQuote && <Redirect push to={quotePath} />}
                <SubHeader>Submission Summary</SubHeader>
                <p>
                    Thank you for choosing RLL®. Your application will be
                    submitted to our underwriting group. You will receive a
                    notification on the status of your submission within 48
                    hours. Please contact us at 800-770-9660 with any questions.
                </p>
                <p className="no-capitalize add-a-lot-of-margin-on-the-top">
                    If you have any additional information or have questions,
                    please <span>ADD A NOTE</span>
                </p>
                <Formik
                    render={props => (
                        <Form
                            {...props}
                            rootstore={rootstore}
                            error={this.state.error}
                            disabled={disabled}
                        />
                    )}
                    onSubmit={onSubmitSummary}
                    initialValues={initialValues}
                />
            </div>
        );
    }
}

const BrokerPropertySummaryObserver = withRouter(
    inject('rootstore','loginstore')(observer(SubmissionSumary))
);

const BrokerPropertySummary = () => (
    <BrokerLayout content={<BrokerPropertySummaryObserver />} />
);

export default BrokerPropertySummary;
