/* eslint-disable react/prop-types */
import React from 'react';
import { PROPERTY_STATUS } from '../constants';
import { ColumnQuote } from './Common/Common';

class TableAbstact extends React.Component {
    onColClick = (item, row) => e => {
        e.stopPropagation();
        switch (item.type) {
            case 'link':
                this.onRowClick(row)(e);
                break;
        }
    };

    onRowClick = row => e => {
        e.stopPropagation();
        this.setState({
            clickedOn: row.id
        });
    };

    handleChange = event => {
        this.setState({ search: event.target.value });
    };

    getStatusColorText = (status, isAdmin = false) => {
        let color = '#97C05C';
        let text = status;
        switch (status) {
            case PROPERTY_STATUS.ACTIVE:
            case PROPERTY_STATUS.BROKER_INIT:
                color = '#97C05C';
                text = 'ACTIVE';
                break;
            case PROPERTY_STATUS.BROKER_REQUESTED:
                color = '#ABABAB';
                text = isAdmin
                    ? 'Information Pending'
                    : 'Information Submitted';
                break;
            case PROPERTY_STATUS.ADMIN_ACCEPT_BOUND:
                color = '#404040';
                text = 'Approved';
                break;
            case PROPERTY_STATUS.ADMIN_CREATED:
                color = '#404040';
                text = 'Pending Review';
                break;
            case PROPERTY_STATUS.BROKER_BOUND:
                color = '#404040';
                text = 'Bind Requested';
                break;
            case PROPERTY_STATUS.BROKER_REJECTED:
                color = '#EE772F';
                text = 'Declined';
                break;
            default:
                color = '#EE772F';
                text = '';
                break;
        }

        return {
            color,
            text
        };
    };

    getStatus = (status, isAdmin = false) => {
        const { color, text } = this.getStatusColorText(status.name, isAdmin);
        return <ColumnQuote color={color}>{text}</ColumnQuote>;
    };

    getLinkStyle = text => {
        return <span className="table-link">{text}</span>;
    };
}

export default TableAbstact;
