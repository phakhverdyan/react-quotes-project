/* eslint-disable indent */
/* eslint-disable react/prop-types */
import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import Minimize from '@material-ui/icons/Minimize';
import Grid from '@material-ui/core/Grid';

import FormHelperText from '@material-ui/core/FormHelperText';
import { NavLink } from 'react-router-dom';

export const SubHeader = props => (
    <span className="sub-header-text">{props.children}</span>
);

export const ComponentLoading = () => (
    <div className="loading-circle">
        <CircularProgress />
    </div>
);

export const CustomExpansionPanel = ({ title, children }) => (
    <div className="margin-bottom-20">
        <ExpansionPanel defaultExpanded={false} className="extension-panel">
            <ExpansionPanelSummary expandIcon={<Minimize />}>
                <Typography className="expansion-panel-title">
                    {title}
                </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>{children}</ExpansionPanelDetails>
        </ExpansionPanel>
    </div>
);

export const CustomGridRow = props => (
    <>
        <Grid container item xs={12} spacing={24}>
            {props.children}
        </Grid>
    </>
);

export const SubmittedWaitingMessage = props => {
    const styles = {
        minHeight: '300px',
        justifyContent: 'space-between'
    };
    const divStyle = {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minHeight: '35%',
        paddingTop: '5%',
        justifyContent: 'space-between'
    };
    return (
        <>
            <Grid container>
                <Grid
                    item
                    xs={12}
                    className="vertical-center in-the-center"
                    style={styles}
                >
                    <div style={divStyle}>
                        {props.icon}
                        {props.children}
                    </div>
                    {props.button}
                </Grid>
            </Grid>
        </>
    );
};

export const Steppler = ({ items, itemsCrumb }) => {
    const mainDivStyle = {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    };
    const style = {
        borderBottom: '1px solid #EEEEEE',
        maxHeight: 30,
        paddingRight: 40
    };
    const ulStyle = {
        padding: 0,
        margin: 0,
        maxHeight: 30
    };
    const activeStyle = {
        borderBottom: '2px solid #03C9F1',
        color: '#000C13'
    };
    const liStyle = {
        display: 'inline-block',
        minHeight: 30,
        color: '#03C9F1',
        textAlign: 'center',
        width: 150,
        cursor: 'pointer'
    };
    const styleSpan = {
        marginBottom: 30,
        textTransform: 'capitalize',
        fontStyle: 'normal',
        fontWeight: 500,
        fontSize: 12
    };
    const disabledStyle = {
        cursor: 'default'
    };

    return (
        <div style={mainDivStyle}>
            <div>
                {itemsCrumb && itemsCrumb.length > 0 && (
                    <ul>
                        {itemsCrumb.map((item, i) => (
                            <li
                                key={item.text}
                                style={{
                                    display: 'inline-block',
                                    paddingLeft: 10
                                }}
                            >
                                <NavLink
                                    to={item.url}
                                    activeClassName="breadcrum-link-selected"
                                    className="breadcrum-link"
                                >
                                    {i > 0 && '>'} {item.text}
                                </NavLink>
                            </li>
                        ))}
                    </ul>
                )}
            </div>
            <div style={style}>
                <ul style={ulStyle}>
                    {items.map((item, i) => (
                        <li
                            key={item.text}
                            onClick={!item.disabled ? item.onClick : null}
                            style={
                                item.active
                                    ? { ...liStyle, ...activeStyle }
                                    : item.disabled
                                    ? { ...liStyle, ...disabledStyle }
                                    : liStyle
                            }
                        >
                            <span style={styleSpan}>
                                {i + 1}. {item.text}
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
};

export const CustomError = React.memo(function CustomError({ name, children }) {
    return (
        <FormHelperText
            id={'error' + name}
            error={true}
            classes={{
                root: 'error-text'
            }}
        >
            {children}
        </FormHelperText>
    );
});

export const scrollToTop = () => {
    window.scrollTo(0, 0);
};

export const getAmericanDate = dateString => {
    if (dateString === '') {
        return '';
    }

    const date = new Date(dateString);

    return date.toLocaleDateString();
};

export const ColumnQuote = ({ color, children }) => {
    return <span style={{ color }}>{children}</span>;
};
