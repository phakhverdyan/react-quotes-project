/* eslint-disable react/prop-types */
import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class AlertDialog extends React.Component {
    constructor(props) {
        super(props);
        const open = props.open || true;
        this.state = {
            open
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
    handleClickOpen() {
        this.setState({ open: true });
    }

    handleClose() {
        return;
    }

    render() {
        const { title, content } = this.props;
        return (
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
                maxWidth="md"
            >
                {title && (
                    <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                )}
                <DialogContent>
                    <div style={{ minHeight: 200 }}>{content}</div>
                </DialogContent>
            </Dialog>
        );
    }
}

export const SuccesModal = props => <AlertDialog {...props} />;
