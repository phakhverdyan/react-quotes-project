/* eslint-disable react/prop-types */
import React from 'react';
import { observer, inject } from 'mobx-react';
import { CustomTextInput } from '../Form/TextInput';

const InteractionItem = ({ from, msg, date }) => {
    const dateLocal = date.toLocaleDateString();
    const dateTimeLocal = date.toLocaleTimeString();
    const dateTimeTitle = `${dateLocal} at ${dateTimeLocal}`;
    return (
        <div className="interaction-item">
            <p className="interaction-item-from">{from}</p>
            <p className="interaction-message">{msg}</p>
            <time dateTime={dateLocal} title={dateTimeTitle}>
                {dateLocal}
            </time>
        </div>
    );
};

const InteractionHistory = () => {
    const history = [
        {
            id: 1,
            from: 'You',
            msg:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.',
            date: new Date()
        },
        {
            id: 2,
            from: 'Rod',
            msg:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.',
            date: new Date()
        }
    ];
    const hasHistory = history && history.length > 0;
    return (
        <div id="interaction-history">
            <p>Interaction history</p>
            <div id="chat-history">
                {hasHistory &&
                    history.map(item => (
                        <InteractionItem {...item} key={item.id} />
                    ))}
                {!hasHistory && (
                    <div className="interaction-item">
                        <p className="interaction-message">No Messages</p>
                    </div>
                )}
            </div>
            <div id="interaction-textarea">
                <div id="interaction-input">
                    <CustomTextInput
                        multiline
                        fullWidth
                        rowsMax={4}
                        rows={4}
                        placeholder="Please, write your question ..."
                    />
                </div>
                <span id="send-message">Send Message</span>
            </div>
        </div>
    );
};

export default inject('loginstore')(observer(InteractionHistory));
