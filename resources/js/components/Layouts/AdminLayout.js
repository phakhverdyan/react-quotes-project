/* eslint-disable react/prop-types */
import React from 'react';
import Layout from './../Layout';
import {
    PartnerMenuIcon,
    CurrentActivityMenuIcon,
    BrokerMenuIcon
} from './../Icons';
import {
    AdminActivity,
    PathBrokerManagment,
    AdminPartners
} from '../../constants';

const AdminLayout = ({ content }) => {
    const menuItems = [
        {
            url: AdminActivity,
            text: 'Current Activity',
            icon: <CurrentActivityMenuIcon />
        },
        {
            url: PathBrokerManagment,
            text: 'Broker Managment',
            icon: <BrokerMenuIcon />
        },
        {
            url: AdminPartners,
            text: 'Partner Managment',
            icon: <PartnerMenuIcon />
        }
    ];
    return (<Layout sidebarItems={menuItems} mainContent={content} admin={true} />);
};
export default AdminLayout;
