/* eslint-disable react/prop-types */
import React from 'react';
import Layout from './../Layout';
import { CurrentActivityMenuIcon } from './../Icons';
import { BrokerActivity } from '../../constants';

const BrokerLayout = ({ content }) => {
    const menuItems = [
        {
            url: BrokerActivity,
            text: 'Current Activity',
            icon: <CurrentActivityMenuIcon />
        }
    ];
    return (
        <Layout sidebarItems={menuItems} mainContent={content} admin={false} />
    );
};
export default BrokerLayout;
