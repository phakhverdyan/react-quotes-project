/* eslint-disable react/prop-types */
import React from 'react';

export const CustomDeleteIcon = React.memo(function CustomDeleteIcon() {
    return (
        <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M6 19C6 20.1 6.9 21 8 21H16C17.1 21 18 20.1 18 19V7H6V19ZM8.5 11.9L9.9 10.5L12 12.6L14.1 10.5L15.5 11.9L13.4 14L15.5 16.1L14.1 17.5L12 15.4L9.9 17.5L8.5 16.1L10.6 14L8.5 11.9ZM15.5 4L14.5 3H9.5L8.5 4H5V6H19V4H15.5Z"
                fill="#ABABAB"
            />
        </svg>
    );
});

export const RllHouse = React.memo(function RllHouse(props) {
    return (
        <svg
            width="56"
            height="56"
            viewBox="0 0 56 56"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g clipPath="url(#clip0)">
                <path
                    d="M0.848389 27.9999L27.9999 0.848389L55.1514 27.9999"
                    stroke="white"
                    strokeWidth="2"
                    strokeMiterlimit="10"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
                <path
                    d="M18.6667 28H30.5455"
                    stroke="white"
                    strokeWidth="2"
                    strokeMiterlimit="10"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
                <path
                    d="M7.63647 28V55.1515H18.6668"
                    stroke="white"
                    strokeWidth="2"
                    strokeMiterlimit="10"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
                <path
                    d="M29.6971 44.1213H27.1516L38.1819 55.1516H48.3637V28.0001L32.2425 11.8789"
                    stroke="white"
                    strokeWidth="2"
                    strokeMiterlimit="10"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
                <path
                    d="M30.1211 28C34.5332 28 38.1817 31.6485 38.1817 36.0606C38.1817 40.4727 34.5332 44.1212 30.1211 44.1212"
                    stroke="white"
                    strokeWidth="2"
                    strokeMiterlimit="10"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
                <path
                    d="M7.63647 14.4244V7.63647H14.4244"
                    stroke="white"
                    strokeWidth="2"
                    strokeMiterlimit="10"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect width="56" height="56" fill="white" />
                </clipPath>
            </defs>
        </svg>
    );
});

export const CurrentActivityMenuIcon = React.memo(
    function CurrentActivityMenuIcon() {
        return (
            <svg
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <g clipPath="url(#clip0)">
                    <path
                        d="M20 22V28H28V16"
                        stroke="#027CA4"
                        strokeWidth="2"
                        strokeMiterlimit="10"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                    />
                    <path
                        d="M4 16V28H12V22"
                        stroke="#027CA4"
                        strokeWidth="2"
                        strokeMiterlimit="10"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                    />
                    <path
                        d="M12 22C12 19.8 13.8 18 16 18C18.2 18 20 19.8 20 22"
                        stroke="#027CA4"
                        strokeWidth="2"
                        strokeMiterlimit="10"
                        strokeLinecap="round"
                    />
                    <path
                        d="M1 13L16 4L31 13"
                        stroke="#027CA4"
                        strokeWidth="2"
                        strokeMiterlimit="10"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                    />
                    <path
                        d="M7 3H4V7"
                        stroke="#027CA4"
                        strokeWidth="2"
                        strokeMiterlimit="10"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                    />
                </g>
                <defs>
                    <clipPath id="clip0">
                        <rect width="32" height="32" fill="white" />
                    </clipPath>
                </defs>
            </svg>
        );
    }
);

export const BrokerMenuIcon = React.memo(function BrokerMenuIcon() {
    return (
        <svg
            width="32"
            height="32"
            viewBox="0 0 32 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M11 8C11 11.3 12 13.8 14 14.6C14.7 14.9 15.4 15 16 15"
                stroke="white"
                strokeWidth="2"
                strokeMiterlimit="10"
            />
            <path
                d="M11 8C11 5.2 13.2 3 16 3C18.8 3 21 5.2 21 8"
                stroke="white"
                strokeWidth="2"
                strokeMiterlimit="10"
            />
            <path
                d="M21 8C21 11.3 20 13.8 18 14.6C17.3 14.9 16.6 15 16 15"
                stroke="white"
                strokeWidth="2"
                strokeMiterlimit="10"
            />
            <path
                d="M20 28H28C28 23.3 25.6 19.2 22 17"
                stroke="white"
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M10 17C6.4 19.2 4 23.3 4 28H12"
                stroke="white"
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M12 28C12 25.8 13.8 24 16 24C18.2 24 20 25.8 20 28"
                stroke="white"
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
            />
        </svg>
    );
});

export const PartnerMenuIcon = React.memo(function PartnerMenuIcon() {
    return (
        <svg
            width="32"
            height="32"
            viewBox="0 0 32 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M12 4C12 2.9 12.9 2 14 2H18C19.1 2 20 2.9 20 4"
                stroke="#027CA4"
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M5 20V25C5 26.1 5.9 27 7 27H25C26.1 27 27 26.1 27 25V20"
                stroke="#027CA4"
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M13 16H6C4.9 16 4 15.1 4 14V10C4 8.9 4.9 8 6 8H26C27.1 8 28 8.9 28 10V14C28 15.1 27.1 16 26 16H19"
                stroke="#027CA4"
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
            />
            <path
                d="M19 16C19 17.7 17.7 19 16 19C14.3 19 13 17.7 13 16"
                stroke="#027CA4"
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
            />
        </svg>
    );
});

export const CustomClockIcon = React.memo(function CLock() {
    return (
        <svg
            width="53"
            height="64"
            viewBox="0 0 53 64"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M26.5 28.8001C27.0854 28.8001 27.56 28.3225 27.56 27.7334C27.56 27.1443 27.0854 26.6667 26.5 26.6667C25.9146 26.6667 25.44 27.1443 25.44 27.7334C25.44 28.3225 25.9146 28.8001 26.5 28.8001Z"
                fill="#014971"
            />
            <path
                d="M29.68 26.6668C30.2654 26.6668 30.74 26.1892 30.74 25.6001C30.74 25.011 30.2654 24.5334 29.68 24.5334C29.0946 24.5334 28.62 25.011 28.62 25.6001C28.62 26.1892 29.0946 26.6668 29.68 26.6668Z"
                fill="#014971"
            />
            <path
                d="M23.32 26.6668C23.9054 26.6668 24.38 26.1892 24.38 25.6001C24.38 25.011 23.9054 24.5334 23.32 24.5334C22.7346 24.5334 22.26 25.011 22.26 25.6001C22.26 26.1892 22.7346 26.6668 23.32 26.6668Z"
                fill="#014971"
            />
            <path
                d="M20.14 24.5332C20.7254 24.5332 21.2 24.0557 21.2 23.4666C21.2 22.8775 20.7254 22.3999 20.14 22.3999C19.5546 22.3999 19.08 22.8775 19.08 23.4666C19.08 24.0557 19.5546 24.5332 20.14 24.5332Z"
                fill="#014971"
            />
            <path
                d="M32.86 24.5332C33.4454 24.5332 33.92 24.0557 33.92 23.4666C33.92 22.8775 33.4454 22.3999 32.86 22.3999C32.2746 22.3999 31.8 22.8775 31.8 23.4666C31.8 24.0557 32.2746 24.5332 32.86 24.5332Z"
                fill="#014971"
            />
            <path
                d="M36.04 22.3999C36.6254 22.3999 37.1 21.9224 37.1 21.3333C37.1 20.7442 36.6254 20.2666 36.04 20.2666C35.4546 20.2666 34.98 20.7442 34.98 21.3333C34.98 21.9224 35.4546 22.3999 36.04 22.3999Z"
                fill="#014971"
            />
            <path
                d="M16.96 22.3999C17.5454 22.3999 18.02 21.9224 18.02 21.3333C18.02 20.7442 17.5454 20.2666 16.96 20.2666C16.3746 20.2666 15.9 20.7442 15.9 21.3333C15.9 21.9224 16.3746 22.3999 16.96 22.3999Z"
                fill="#014971"
            />
            <path
                d="M26.5 24.5332C27.0854 24.5332 27.56 24.0557 27.56 23.4666C27.56 22.8775 27.0854 22.3999 26.5 22.3999C25.9146 22.3999 25.44 22.8775 25.44 23.4666C25.44 24.0557 25.9146 24.5332 26.5 24.5332Z"
                fill="#014971"
            />
            <path
                d="M26.5 37.3333C27.0854 37.3333 27.56 36.8557 27.56 36.2666C27.56 35.6775 27.0854 35.2 26.5 35.2C25.9146 35.2 25.44 35.6775 25.44 36.2666C25.44 36.8557 25.9146 37.3333 26.5 37.3333Z"
                fill="#014971"
            />
            <path
                d="M26.5 40.5332C27.0854 40.5332 27.56 40.0557 27.56 39.4666C27.56 38.8775 27.0854 38.3999 26.5 38.3999C25.9146 38.3999 25.44 38.8775 25.44 39.4666C25.44 40.0557 25.9146 40.5332 26.5 40.5332Z"
                fill="#014971"
            />
            <path
                d="M26.5 43.7334C27.0854 43.7334 27.56 43.2559 27.56 42.6668C27.56 42.0777 27.0854 41.6001 26.5 41.6001C25.9146 41.6001 25.44 42.0777 25.44 42.6668C25.44 43.2559 25.9146 43.7334 26.5 43.7334Z"
                fill="#014971"
            />
            <path
                d="M26.5 46.9334C27.0854 46.9334 27.56 46.4558 27.56 45.8667C27.56 45.2776 27.0854 44.8 26.5 44.8C25.9146 44.8 25.44 45.2776 25.44 45.8667C25.44 46.4558 25.9146 46.9334 26.5 46.9334Z"
                fill="#014971"
            />
            <path
                d="M26.5 50.1333C27.0854 50.1333 27.56 49.6558 27.56 49.0667C27.56 48.4776 27.0854 48 26.5 48C25.9146 48 25.44 48.4776 25.44 49.0667C25.44 49.6558 25.9146 50.1333 26.5 50.1333Z"
                fill="#014971"
            />
            <path
                d="M26.5 53.3335C27.0854 53.3335 27.56 52.856 27.56 52.2669C27.56 51.6778 27.0854 51.2002 26.5 51.2002C25.9146 51.2002 25.44 51.6778 25.44 52.2669C25.44 52.856 25.9146 53.3335 26.5 53.3335Z"
                fill="#014971"
            />
            <path
                d="M51.94 61.8667H48.76V57.6H43.46V46.72C43.46 43.84 42.082 41.1733 39.644 39.5733L28.408 32L39.644 24.4267C41.976 22.8267 43.46 20.16 43.46 17.28V6.4H48.76V2.13333H51.94C52.576 2.13333 53 1.70667 53 1.06667C53 0.426667 52.576 0 51.94 0H48.76H47.7H5.3H4.24H1.06C0.424 0 0 0.426667 0 1.06667C0 1.70667 0.424 2.13333 1.06 2.13333H4.24V6.4H9.54V17.28C9.54 20.16 10.918 22.8267 13.356 24.4267L24.592 32L13.356 39.5733C11.024 41.1733 9.54 43.84 9.54 46.72V57.6H4.24V61.8667H1.06C0.424 61.8667 0 62.2933 0 62.9333C0 63.5733 0.424 64 1.06 64H4.24H5.3H47.7H48.76H51.94C52.576 64 53 63.5733 53 62.9333C53 62.2933 52.576 61.8667 51.94 61.8667ZM14.522 22.72C12.72 21.5467 11.66 19.52 11.66 17.3867V6.4H41.34V17.28C41.34 19.4133 40.28 21.44 38.478 22.6133L26.5 30.72L14.522 22.72ZM11.66 46.72C11.66 44.5867 12.72 42.56 14.522 41.3867L26.5 33.28L38.478 41.28C40.28 42.4533 41.34 44.48 41.34 46.6133V57.6H11.66V46.72Z"
                fill="#014971"
            />
        </svg>
    );
});

export const InfoIcon = () => (
    <svg
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M11 11H9V5H11V11ZM11 15H9V13H11V15ZM10 0C4.5 0 0 4.5 0 10C0 15.5 4.5 20 10 20C15.5 20 20 15.5 20 10C20 4.5 15.5 0 10 0Z"
            fill="#ABABAB"
        />
    </svg>
);

export const EllipseIcon = () => (
    <svg
        width="8"
        height="8"
        viewBox="0 0 8 8"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <circle cx="4" cy="4" r="4" fill="#E5E5E5" />
    </svg>
);

export const PdfIcon = () => (
    <svg
        width="16"
        height="20"
        viewBox="0 0 16 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M9 7H14.5L9 1.5V7ZM2 0H10L16 6V18C16 19.1 15.1 20 14 20H2C0.9 20 0 19.1 0 18V2C0 0.9 0.9 0 2 0ZM6.1 9.4C6.1 9.4 5.8 11.2 4 14.1C4 14.1 0.5 15.9 1.3 17.3C2 18.4 3.6 17.3 5 14.6C5 14.6 6.8 14 9.2 13.8C9.2 13.8 13.1 15.5 13.6 13.7C14.1 11.8 10.5 12.3 9.9 12.5C9.9 12.5 7.9 11.2 7.4 9.3C7.4 9.3 8.5 5.4 6.8 5.4C5.1 5.3 5.8 8.4 6.1 9.4ZM6.9 10.4C6.9 10.4 7.4 11.6 8.8 12.9C8.8 12.9 6.5 13.4 5.4 13.8C5.4 13.8 6.4 12.1 6.9 10.4ZM10.8 13.2C11.4 13 13.1 13.4 13.1 13.7C13 14 10.8 13.2 10.8 13.2ZM3.8 15C3.3 16.2 2.4 17 2.1 17C1.8 17 2.8 15.4 3.8 15ZM6.9 8.1C6.9 8 6.5 5.9 6.9 6C7.4 6 6.9 8 6.9 8.1Z"
            fill="#ABABAB"
        />
    </svg>
);

export const CustomRadioIcon = () => (
    <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <circle cx="12" cy="12" r="11.5" stroke="#E5E5E5" />
    </svg>
);

export const CustomRadioIconChecked = () => (
    <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <circle cx="12" cy="12" r="11.5" stroke="#E5E5E5" />
        <circle cx="12" cy="12" r="5" fill="#404040" />
    </svg>
);

export const CustomCheckboxIcon = () => (
    <svg
        width="32"
        height="32"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <rect
            x="0.5"
            y="0.5"
            width="31"
            height="31"
            rx="3.5"
            fill="white"
            stroke="#EEEEEE"
        />
    </svg>
);

export const CustomCheckboxIconCheked = () => (
    <svg
        width="32"
        height="32"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <rect
            x="0.5"
            y="0.5"
            width="31"
            height="31"
            rx="3.5"
            fill="white"
            stroke="#EEEEEE"
        />
        <path
            d="M12.6 19.6L8.4 15.4L7 16.8L12.6 22.4L24.6 10.4L23.2 9L12.6 19.6Z"
            fill="#404040"
        />
    </svg>
);

export const IconDeleteX = () => (
    <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M8.46446 9.87868L9.87867 8.46447L15.5355 14.1213L14.1213 15.5355L8.46446 9.87868Z"
            fill="#404040"
        />
        <path
            d="M8.46446 14.1213L14.1213 8.46447L15.5355 9.87868L9.87867 15.5355L8.46446 14.1213Z"
            fill="#404040"
        />
    </svg>
);

export const CustomSwitchOffIcon = () => (
    <svg
        width="48"
        height="32"
        viewBox="0 0 48 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <rect
            x="40"
            y="23"
            width="40"
            height="16"
            rx="8"
            transform="rotate(-180 40 23)"
            fill="#ABABAB"
        />
        <g filter="url(#filter0_d)">
            <circle
                cx="32"
                cy="15"
                r="12"
                transform="rotate(-180 32 15)"
                fill="#404040"
            />
        </g>
        <defs>
            <filter
                id="filter0_d"
                x="16"
                y="0"
                width="32"
                height="32"
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity="0" result="BackgroundImageFix" />
                <feColorMatrix
                    in="SourceAlpha"
                    type="matrix"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                />
                <feOffset dy="1" />
                <feGaussianBlur stdDeviation="2" />
                <feColorMatrix
                    type="matrix"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
                />
                <feBlend
                    mode="normal"
                    in2="BackgroundImageFix"
                    result="effect1_dropShadow"
                />
                <feBlend
                    mode="normal"
                    in="SourceGraphic"
                    in2="effect1_dropShadow"
                    result="shape"
                />
            </filter>
        </defs>
    </svg>
);

export const LogoutIcon = () => (
    <svg
        width="32"
        height="32"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M20 21V18H13V14H20V11L25 16L20 21ZM18 6C19.1 6 20 6.9 20 8V10H18V8H9V24H18V22H20V24C20 25.1 19.1 26 18 26H9C7.9 26 7 25.1 7 24V8C7 6.9 7.9 6 9 6H18Z"
            fill="#ABABAB"
        />
    </svg>
);

export const SettingsIcon = () => (
    <svg
        width="32"
        height="32"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M15.92 19.5C13.9787 19.5 12.3439 17.9 12.3439 16C12.3439 14.1 13.9787 12.5 15.92 12.5C17.8612 12.5 19.496 14.1 19.496 16C19.496 17.9 17.9634 19.5 15.92 19.5ZM23.5829 17C23.5829 16.7 23.6851 16.4 23.6851 16C23.6851 15.6 23.6851 15.3 23.5829 15L25.7285 13.4C25.9329 13.3 25.9329 13 25.8307 12.8L23.7873 9.3C23.6851 9.1 23.3786 9 23.1742 9.1L20.6199 10.1C20.109 9.7 19.496 9.4 18.883 9.1L18.4743 6.4C18.4743 6.2 18.2699 6 17.9634 6H13.8765C13.6722 6 13.4678 6.2 13.3656 6.4L13.0591 9.1C12.4461 9.3 11.8331 9.7 11.3222 10.1L8.76788 9.1C8.56353 9 8.25701 9.1 8.15484 9.3L6.11139 12.8C5.90704 12.9 6.00921 13.2 6.21356 13.4L8.35918 15C8.35918 15.3 8.25701 15.7 8.25701 16C8.25701 16.3 8.25701 16.6 8.35918 17L6.21356 18.7C6.00921 18.8 6.00921 19.1 6.11139 19.3L8.15484 22.8C8.25701 23 8.56353 23 8.76788 23L11.3222 22C11.8331 22.4 12.4461 22.7 13.0591 23L13.4678 25.6C13.4678 25.8 13.6722 26 13.9787 26H18.0656C18.2699 26 18.5765 25.8 18.5765 25.6L18.9851 23C19.5982 22.7 20.2112 22.4 20.7221 22L23.2764 23C23.4807 23.1 23.7873 23 23.8894 22.8L25.9329 19.3C26.0351 19.1 26.0351 18.8 25.8307 18.7L23.5829 17Z"
            fill="#ABABAB"
        />
    </svg>
);
