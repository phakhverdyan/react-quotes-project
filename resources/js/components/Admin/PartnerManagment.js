/* eslint-disable react/prop-types */
import React from 'react';
import AdminLayout from '../Layouts/AdminLayout';
import Grid from '@material-ui/core/Grid';
import CustomTable from '../Table';
import { SearchInput } from './../Form/TextInput';
import { observer, inject } from 'mobx-react';
import { ComponentLoading } from '../Common/Common';
import { CustomDeleteIcon } from './../Icons';

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ''
        };
        this.store = this.props.rootstore.partnerStore;
        this.getData = this.getData.bind(this);
        this.rows = [
            {
                id: 'name',
                label: 'Partner Name'
            },
            {
                id: 'email',
                label: 'Email'
            },
            { id: 'delete', label: '', type: 'delete' }
        ];
    }

    onColClick = (item, row) => e => {
        e.stopPropagation();
        switch (item.type) {
            case 'delete':
                alert('Delete ' + row.name);
                break;
            default:
                this.onRowClick(row)(e);
                break;
        }
    };

    onRowClick = row => e => {
        e.stopPropagation();
        alert('You clicked ' + row.name);
    };

    componentDidMount() {
        this.store.fetch();
    }

    handleChange = event => {
        this.setState({ search: event.target.value });
    };

    getData() {
        const lowerCaseSearch = this.state.search.toLowerCase();
        let rows = this.store.partners.map(b => ({
            id: b.id,
            name: b.name,
            email: '',
            delete: <CustomDeleteIcon />
        }));

        if (lowerCaseSearch !== '') {
            rows = rows.filter(o =>
                `${o.name} ${o.email}`
                    .toLowerCase()
                    .includes(lowerCaseSearch)
            );
        }

        return rows;
    }

    render() {
        return (
            <Grid container>
                <Grid item xs={9}>
                    <h1 className="content-title">Partners Managment</h1>
                </Grid>
                <Grid item xs={3}>
                    <SearchInput
                        className="search-input"
                        value={this.state.search}
                        onChange={this.handleChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    {this.store.isFetching ? (
                        <ComponentLoading />
                    ) : (
                        <CustomTable
                            rows={this.rows}
                            data={this.getData()}
                            orderBy="name"
                            order="asc"
                            rowsPerPage={25}
                            onColClick={this.onColClick}
                            onRowClick={this.onRowClick}
                        />
                    )}
                </Grid>
            </Grid>
        );
    }
}

const ContentObservable = inject('rootstore')(observer(Content));

const PartnerManagment = () => <AdminLayout content={<ContentObservable />} />;

export default PartnerManagment;
