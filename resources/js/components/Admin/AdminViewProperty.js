import React from 'react';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { AddNewPropertyForm } from '../Broker/NewProperty';
import AdminLayout from '../Layouts/AdminLayout';

const FormWrapper = props => <AddNewPropertyForm readOnly={true} {...props} isAdmin={true} />;

const AdminPropertyObserver = withRouter(
    inject('rootstore')(observer(FormWrapper))
);

const AdminViewProperty = () => (
    <AdminLayout content={<AdminPropertyObserver />} />
);

export default AdminViewProperty;
