/* eslint-disable react/prop-types */
import React from 'react';
import { observer, inject } from 'mobx-react';
import Grid from '@material-ui/core/Grid';
import {
    CustomGridRow,
    Steppler,
    ComponentLoading,
    CustomError
} from '../Common/Common';
import { SubHeader } from './../Common/Common';
import {
    CustomTextInput,
    CustomMaterialSelectInput,
    CustomSwitchInput,
    CustomCheckbox
} from '../Form/TextInput';
import { Formik } from 'formik';
import { quoteValidationSchema } from './Validations';
import AdminLayout from './../Layouts/AdminLayout';
import { AdminSubmitButton } from '../Form/Buttons';
import { withRouter } from 'react-router';
import {
    GetAdminBrokerViewProperty,
    PathBrokerManagment,
    AdminActivity,
    PROPERTY_STATUS,
    GetPathAdminBindQuote
} from '../../constants';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { SuccesModal } from './../Common/Dialog';

class QuotePanel extends React.Component {
    change = e => {
        e.persist();
        this.props.handleChange(e);
        this.props.setFieldTouched(e.target.name, true, false);
    };
    render() {
        const { option } = this.props;
        const {
            values: { price, coverage, enhacements, include, program },
            errors,
            touched,
            coverages,
            coveragesSelect,
            programSelect,
            enhancementPrice,
            quoteDefaults,
            selected
        } = this.props;

        const coverageSelected = coverages.find(c => c.id === coverage);
        let coverageAdditional = 0;
        if (coverageSelected) {
            coverageAdditional = parseFloat(coverageSelected.price);
        }

        let enhacementsTotal = 0;

        if (enhacements && enhancementPrice) {
            enhacementsTotal = enhancementPrice;
        }

        const priceDouble = parseFloat(price);
        const totalPrice = priceDouble + coverageAdditional + enhacementsTotal;

        let additionalProps = {};
        if (quoteDefaults.property_quote_id) {
            additionalProps['property_quote_id'] =
                quoteDefaults.property_quote_id;
        }

        this.props.OnChange({
            option: parseInt(option),
            program,
            totalPrice,
            price: priceDouble,
            coverageAdditional,
            coverage,
            enhancements: enhacements,
            enhancementsTotal: enhacementsTotal,
            include,
            quoteDefaults,
            ...additionalProps
        });


        const className = selected
            ? 'quote-panel quote-view-selected'
            : 'quote-panel quote-view';
        return (
            <>
                <div className={className}>
                    <h3>Option {option}</h3>
                    <hr />
                    <p>Program</p>
                    <div className="program-select-div">
                        <div className="full-width">
                            <CustomMaterialSelectInput
                                value={program}
                                id="program"
                                name="program"
                                options={programSelect}
                                onChange={this.change}
                                placeholder="Program"
                                fullWidth
                            />
                        </div>
                    </div>

                    <div className="price-div">
                        <span>$</span>
                        <CustomTextInput
                            value={price}
                            id="price"
                            name="price"
                            onChange={this.change}
                            fullWidth
                            helperText={touched.price ? errors.price : ''}
                            error={touched.price && Boolean(errors.price)}
                        />
                    </div>
                    <p>Personal Coverage</p>
                    <div className="price-div">
                        <span>$</span>
                        <div className="full-width">
                            <CustomMaterialSelectInput
                                value={coverage}
                                id="coverage"
                                name="coverage"
                                options={coveragesSelect}
                                onChange={this.change}
                                placeholder="Coverage"
                                fullWidth
                            />
                        </div>
                    </div>
                    <p>
                        <span>+ ${coverageAdditional.toFixed(2)}</span>
                    </p>
                    <p>Enhancements</p>
                    <CustomSwitchInput
                        value={enhacements}
                        id="enhacements"
                        name="enhacements"
                        checked={enhacements}
                        onChange={this.change}
                    />
                    <p className="margin-bottom-40">
                        <span>+ ${enhacementsTotal.toFixed(2)}</span>
                    </p>
                    <hr />
                    <p className="total">Total price</p>
                    <h4>${totalPrice.toFixed(2)}</h4>
                </div>
                <div className="quote-include">
                    <CustomCheckbox
                        id="include"
                        name="include"
                        onChange={this.change}
                        checked={include}
                    />
                    <label>Include This Option in the Quote</label>
                </div>
            </>
        );
    }
}

const QuoteForm = props => (
    <Formik
        render={renderProps => <QuotePanel {...props} {...renderProps} />}
        initialValues={props.initialValues}
        validationSchema={quoteValidationSchema}
    />
);

const SuccessSaveQuoteModal = () => {
    const content = (
        <>
            <span className="in-the-center success-text">Success</span>
            <p className="modal-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.{' '}
            </p>
            <span className="in-the-center">
                <Link to={PathBrokerManagment}>
                    <AdminSubmitButton>Broker Managment</AdminSubmitButton>
                </Link>
            </span>
        </>
    );
    return <SuccesModal content={content} />;
};

class QuoteBroker extends React.Component {
    constructor(props) {
        super(props);
        this.propertyId = this.props.match.params.id;
        this.state = {
            goToLocation: false,
            goToBind: false,
            error: null,
            showModal: false,
            hasOneQuoteBeenSelected: false,
            selected: 0,

        };



    }
    quotes = [];

    goToLocation = () => {
        this.setState({
            goToLocation: true
        });
    };

    goToBind = () => {
        this.setState({
            goToBind: true
        });
    };

    onQuoteChange = values => {
        this.quotes[values.option - 1] = this.quotes[values.option - 1] || {};
        this.quotes[values.option - 1] = values;
    };

    componentWillMount() {
        this.props.rootstore.quoteStore.fetchPropertyQuotes(this.propertyId);
        this.props.rootstore.propertyStore.getById(this.propertyId);
    }

    onSuccessSave = () => {
        this.setState({
            showModal: true
        });
    };

    onErrorSave = () => {
        this.setState({
            error: 'There was an error submitting the form'
        });
    };

    resetError = () => {
        this.setState({
            error: null
        });
    };

    onSubmit = () => {
        const hasOneQuoteBeenSelected = this.quotes.some(q => q.include);
        if (!hasOneQuoteBeenSelected) {
            this.setState({
                error: 'Please Select at least one quote'
            });
            return;
        }
        this.resetError();
        const propertyId = this.propertyId;
        this.props.rootstore.quoteStore.createQuotes(
            {
                propertyId,
                quotes: this.quotes
            },
            this.onSuccessSave,
            this.onErrorSave
        );
    };

    render() {
        const { quoteStore, propertyStore } = this.props.rootstore;

        const areStoresFetching =
            quoteStore.isFetchingDefaultQuotes ||
            propertyStore.isPropertyByIdFetching;

        if (areStoresFetching) {
            return <ComponentLoading />;
        }

        const property = propertyStore.property;
        const status = propertyStore.status;

        const locationPath = GetAdminBrokerViewProperty(
            propertyStore.property.id
        );

        const bindPath = GetPathAdminBindQuote(
            propertyStore.property.id
        );

        console.log("propertyStore");
        console.log(propertyStore);
        const messageNote = (propertyStore.messages && propertyStore.messages[0]) ? propertyStore.messages[0].text : '';
        const letGoToBind =
            status.name === PROPERTY_STATUS.BROKER_BOUND ||
            PROPERTY_STATUS.BROKER_REJECTED;

        const items = [
            {
                text: 'Locations',
                onClick: this.goToLocation,
                active: false,
                disabled: false
            },
            {
                text: 'Quote',
                onClick: null,
                active: true,
                disabled: true
            },
            {
                text: 'Bind',
                onClick: this.goToBind,
                active: false,
                disabled: !letGoToBind
            }
        ];
        const itemsCrumb = [
            {
                url: AdminActivity,
                text: 'Current Activity'
            },
            {
                url: this.props.location.pathname,
                text: property.property_name
            }
        ];

        const coverages = quoteStore.personalCoverageTypes;
        const basePrices = quoteStore.basePriceTypes;
        const enhancements = quoteStore.enhancementsTypes;
        const pdlwOptions = quoteStore.pdlwOptionTypes;

        const getCoverageId = id => coverages.find(c => c.id === id).id;
        const getBasePrice = id =>
            parseFloat(basePrices.find(c => c.id === id).price);
        const hasEnhancements = id =>
            parseFloat(enhancements.find(c => c.id === id).price) > 0;
        const getPdlwOption = id => pdlwOptions.find(c => c.id === id).id;

        const enhancementWithPrice =
            enhancements.find(c => parseFloat(c.price) > 0) || {};

        const enhancementPrice = parseFloat(enhancementWithPrice.price) || 0;

        let viewQuotes = [];

        if (quoteStore.propertyQuotes.length > 0) {
            const defaultQuotes = quoteStore.propertyQuotes;

            viewQuotes = defaultQuotes.map(q => ({
                quote: { ...q, type: 'update', property_quote_id: q.id },
                initialValues: {
                    program: getPdlwOption(q.pdlw_option_id),
                    price: parseFloat(q.base_price),
                    coverage: getCoverageId(q.personal_coverage_id),
                    enhacements: hasEnhancements(q.enhancement_id),
                    include: q.viewable > 0,
                    selected: q.selected ? 1 : 0,
                }
            }));
        } else {
            const defaultQuotes = quoteStore.defaultQuotesTypes;
            viewQuotes = defaultQuotes.map(q => ({
                quote: { ...q, type: 'new' },
                initialValues: {
                    program: getPdlwOption(q.pdlw_option_id),
                    price: getBasePrice(q.base_price_id),
                    coverage: getCoverageId(q.personal_coverage_id),
                    enhacements: hasEnhancements(q.enhancement_id),
                    include: true,
                    selected: 0,
                }
            }));
        }


        const coveragesSelect = coverages.map(pc => ({
            label: parseFloat(pc.amount).toLocaleString(),
            value: pc.id
        }));
        const programSelect = pdlwOptions.map(p => ({
            label: p.name,
            value: p.id
        }));

        let button = '';
         if(property.status.name != 'broker-bound')
         {
            button = <AdminSubmitButton
                onClick={this.onSubmit}
                disabled={quoteStore.isFetching}
                id="quote-submit-button"
            >
                submit selected QUOTES TO broker
            </AdminSubmitButton>;
         }
        else {
            button = <AdminSubmitButton
                onClick={this.goToBind}
                disabled={quoteStore.isFetching}
                id="quote-submit-button"
            >
                Continue to Bind
            </AdminSubmitButton>;
        }
        return (
            <>
                {this.state.goToLocation && <Redirect push to={locationPath} />}
                {this.state.goToBind && <Redirect push to={bindPath} />}
                {this.state.showModal && <SuccessSaveQuoteModal />}
                {!areStoresFetching && (
                    <>
                        <Steppler items={items} itemsCrumb={itemsCrumb} />
                        <div className="form-div-expanded">
                            <div className="sub-header-extra-margin">
                                <SubHeader>{property.property_name}</SubHeader>
                            </div>
                            {messageNote && (
                                <div className="summary" >
                                    <strong>Summary Info</strong>
                                    <p>
                                        {messageNote}

                                        <time>{property.created_at}</time>
                                    </p>
                                </div>
                            )}


                            <Grid container>
                                <CustomGridRow>
                                    {viewQuotes.map((value, i) => (
                                        <Grid item xs={4} key={value.quote.id}>
                                            <QuoteForm
                                                option={i + 1}
                                                quoteDefaults={value.quote}
                                                initialValues={
                                                    value.initialValues
                                                }
                                                OnChange={this.onQuoteChange}
                                                enhancementPrice={
                                                    enhancementPrice
                                                }
                                                coverages={coverages}
                                                coveragesSelect={
                                                    coveragesSelect
                                                }
                                                programSelect={programSelect}
                                                selected={value.quote.selected}
                                            />
                                        </Grid>
                                    ))}
                                </CustomGridRow>
                            </Grid>
                            <div className="quote-submit">
                                <Grid container>
                                    <CustomGridRow>
                                        <Grid item xs={8} />
                                        <Grid item xs={4}>
                                            {this.state.error &&
                                                this.state.error !== '' && (
                                                    <CustomError
                                                        name="formError"
                                                        className="margin-bottom-20"
                                                    >
                                                        {this.state.error}
                                                    </CustomError>
                                                )}
                                            {button}

                                        </Grid>
                                    </CustomGridRow>
                                </Grid>
                            </div>
                        </div>
                    </>
                )}
            </>
        );
    }
}

const QuoteBrokerObserver = withRouter(
    inject('rootstore')(observer(QuoteBroker))
);

const QuoteBrokerWithLayout = () => (
    <AdminLayout content={<QuoteBrokerObserver />} />
);

export default QuoteBrokerWithLayout;
