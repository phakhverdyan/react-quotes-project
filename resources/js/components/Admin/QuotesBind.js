/* eslint-disable react/prop-types */
import React from 'react';
import { SubmittedWaitingMessage, ComponentLoading } from './../Common/Common';
import { AdminSubmitButton } from '../Form/Buttons';
import { Redirect } from 'react-router-dom';
import {
    AdminActivity,
    PROPERTY_STATUS,
    PathBrokerManagment
} from './../../constants';
import { RllHouse } from '../Icons';
import AdminLayout from './../Layouts/AdminLayout';
import { withRouter } from 'react-router';
import { observer, inject } from 'mobx-react';
import { Steppler } from './../Common/Common';
import { SubHeader } from './../Common/Common';
import { GetAdminBrokerViewProperty } from './../../constants';
import { GetPathAdminCreateQuote } from './../../constants';
import { QuoteWaitingSubmitted } from '../Broker/ViewQuotes';
import { SuccesModal } from './../Common/Dialog';
import { Link } from 'react-router-dom';
import { CustomError } from './../Common/Common';

const SuccessBindQuoteModal = () => {
    const content = (
        <>
            <span className="in-the-center success-text">Success</span>
            <p className="modal-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.{' '}
            </p>
            <span className="in-the-center">
                <Link to={PathBrokerManagment}>
                    <AdminSubmitButton>Broker Managment</AdminSubmitButton>
                </Link>
            </span>
        </>
    );
    return <SuccesModal content={content} />;
};

class QuotesAcceptedMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            error: null
        };
        this.propertyId = this.props.match.params.id;
    }

    onSuccessSave = () => {
        this.setState({
            showModal: true
        });
    };

    onErrorSave = () => {
        this.setState({
            error: 'There was an error binding the quote.'
        });
    };

    onSubmit = () => {
        this.props.rootstore.quoteStore.bindQuote(
            this.propertyId,
            this.onSuccessSave,
            this.onErrorSave
        );
    };

    render() {
        if (this.state.showModal) {
            return <SuccessBindQuoteModal />;
        }

        const { quoteStore } = this.props.rootstore;

        const ContinueButton = (
            <>
                {this.state.error && this.state.error !== '' && (
                    <CustomError name="formError" className="margin-bottom-20">
                        {this.state.error}
                    </CustomError>
                )}
                <AdminSubmitButton
                    type="button"
                    onClick={this.onSubmit}
                    disabled={quoteStore.isFetching}
                >
                    Bind
                </AdminSubmitButton>
            </>
        );
        const submittedComponent = (
            <SubmittedWaitingMessage
                icon={<RllHouse className="icon-color-content" />}
                button={ContinueButton}
            >
                Quotes were accepted
            </SubmittedWaitingMessage>
        );

        return submittedComponent;
    }
}

const QuotesAcceptedMessageObserver = withRouter(
    inject('rootstore')(observer(QuotesAcceptedMessage))
);

const QuoteMessage = ({ text, date }) => {
    const jsDate = new Date(date);
    const dateLocal = jsDate.toLocaleDateString();
    const dateTimeLocal = jsDate.toLocaleTimeString();
    const dateTimeTitle = `${dateLocal} at ${dateTimeLocal}`;
    return (
        <div>
            <p>{text}</p>
            <time dateTime={dateLocal} title={dateTimeTitle}>
                {dateLocal}
            </time>
        </div>
    );
};

const QuotesNotAccepted = ({ messages }) => (
    <div id="quotes-not-accepted">
        <div id="quotes-not-accepted-messages">
            <h3>
                Broker Didn’t Accept Quotes and Requested Additional Informaiton
            </h3>
            {messages.map(m => (
                <QuoteMessage key={m.id} text={m.text} date={m.created_at} />
            ))}
        </div>
        <div>
            <AdminSubmitButton disabled={true}>Bind</AdminSubmitButton>
        </div>
    </div>
);

class QuoteBind extends React.Component {
    constructor(props) {
        super(props);
        this.propertyId = this.props.match.params.id;
        this.state = {
            goToLocation: false,
            goToQuotes: false
        };
    }

    componentWillMount() {
        // load property
        this.props.rootstore.propertyStore.getById(this.propertyId);
    }

    goToLocation = () => {
        this.setState({
            goToLocation: true
        });
    };

    goToQuotes = () => {
        this.setState({
            goToQuotes: true
        });
    };

    render() {
        const { propertyStore } = this.props.rootstore;

        const areStoresFetching = propertyStore.isPropertyByIdFetching;

        if (areStoresFetching) {
            return <ComponentLoading />;
        }

        const property = propertyStore.property;
        const status = propertyStore.status;
        const messages = propertyStore.messages;

        const locationPath = GetAdminBrokerViewProperty(
            propertyStore.property.id
        );
        const quotesPath = GetPathAdminCreateQuote(propertyStore.property.id);

        const items = [
            {
                text: 'Locations',
                onClick: this.goToLocation,
                active: false,
                disabled: false
            },
            {
                text: 'Quote',
                onClick: this.goToQuotes,
                active: false,
                disabled: false
            },
            {
                text: 'Bind',
                onClick: null,
                active: true,
                disabled: true
            }
        ];
        const itemsCrumb = [
            {
                url: AdminActivity,
                text: 'Current Activity'
            },
            {
                url: this.props.location.pathname,
                text: property.property_name
            }
        ];

        let component = <QuoteWaitingSubmitted />;

        if (status.name === PROPERTY_STATUS.BROKER_BOUND) {
            component = <QuotesAcceptedMessageObserver />;
        }

        if (status.name === PROPERTY_STATUS.BROKER_REJECTED) {
            component = <QuotesNotAccepted messages={messages} />;
        }

        return (
            <>
                {this.state.goToLocation && <Redirect push to={locationPath} />}
                {this.state.goToQuotes && <Redirect push to={quotesPath} />}
                {!areStoresFetching && (
                    <>
                        <Steppler items={items} itemsCrumb={itemsCrumb} />

                        <div className="form-div-expanded">
                            <div className="sub-header-extra-margin">
                                <SubHeader>{property.property_name}</SubHeader>
                            </div>
                            {component}
                        </div>
                    </>
                )}
            </>
        );
    }
}

const QuoteBindObserver = withRouter(inject('rootstore')(observer(QuoteBind)));

const QuoteBrokerWithLayout = () => (
    <AdminLayout content={<QuoteBindObserver />} />
);

export default QuoteBrokerWithLayout;
