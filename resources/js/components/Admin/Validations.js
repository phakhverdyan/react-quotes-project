import { number, object, string } from 'yup';

export const quoteValidationSchema = object({
    price: number()
        .typeError('Must be a number')
        .positive('Must be a positive number')
        .max(1000, 'Maximum price is $1000')
        .required('Price is required'),
    coverage: number()
        .typeError('Must be a number')
        .positive('Must be a positive number')
        .required('Price is required')
});

export const inviteBrokerValidationSchema = object({
    partnerId: number()
        .positive()
        .required('Partner Id is required'),
    firstName: string().required('First Name is required'),
    lastName: string().required('Last Name is required'),
    email: string()
        .email('Invalid Email')
        .required('Email is required'),
    uniqueEmail: string().required('Email is already taken')
});
