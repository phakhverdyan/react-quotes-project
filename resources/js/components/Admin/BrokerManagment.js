/* eslint-disable react/prop-types */
import React from 'react';
import AdminLayout from '../Layouts/AdminLayout';
import Grid from '@material-ui/core/Grid';
import CustomTable from '../Table';
import { SearchInput } from './../Form/TextInput';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { ComponentLoading } from '../Common/Common';
import { CustomDeleteIcon } from './../Icons';

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ''
        };
        this.store = this.props.rootstore.brokerStore;
        this.getBrokers = this.getBrokers.bind(this);
        this.rows = [
            {
                id: 'firstName',
                label: 'First Name'
            },
            {
                id: 'lastName',
                label: 'Last Name'
            },
            { id: 'email', label: 'Email' },
            { id: 'delete', label: '', type: 'delete' }
        ];
    }

    onColClick = (item, row) => e => {
        e.stopPropagation();
        switch (item.type) {
            case 'delete':
                alert('Delete ' + row.email);
                break;
            default:
                this.onRowClick(row)(e);
                break;
        }
    };

    onRowClick = row => e => {
        e.stopPropagation();
        alert('You clicked ' + row.email);
    };

    componentDidMount() {
        this.store.fetchBrokers();
    }

    handleChange = event => {
        this.setState({ search: event.target.value });
    };

    getBrokers() {
        const lowerCaseSearch = this.state.search.toLowerCase();
        let rows = this.store.brokers.map(b => ({
            id: b.id,
            firstName: b.firstname,
            lastName: b.lastname,
            email: b.email,
            delete: <CustomDeleteIcon />
        }));

        if (lowerCaseSearch !== '') {
            rows = rows.filter(o =>
                `${o.firstName} ${o.lastName} ${o.email} ${o.status}`
                    .toLowerCase()
                    .includes(lowerCaseSearch)
            );
        }

        return rows;
    }

    render() {
        return (
            <Grid container>
                <Grid item xs={9}>
                    <h1 className="content-title">Broker Managment</h1>
                    <Link
                        to="/admin/brokers/invite"
                        className="main-content-action"
                    >
                        <span className="padding-right-10">+</span> Invite new
                        broker
                    </Link>
                </Grid>
                <Grid item xs={3}>
                    <SearchInput
                        className="search-input"
                        value={this.state.search}
                        onChange={this.handleChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    {this.store.isFetching ? (
                        <ComponentLoading />
                    ) : (
                        <CustomTable
                            rows={this.rows}
                            data={this.getBrokers()}
                            orderBy="firstName"
                            order="asc"
                            rowsPerPage={25}
                            onColClick={this.onColClick}
                            onRowClick={this.onRowClick}
                        />
                    )}
                </Grid>
            </Grid>
        );
    }
}

const ContentObservable = inject('rootstore')(observer(Content));

const BrokerManagment = () => <AdminLayout content={<ContentObservable />} />;

export default BrokerManagment;
