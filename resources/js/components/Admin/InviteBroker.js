/* eslint-disable react/prop-types */
import React from 'react';
import { observer, inject } from 'mobx-react';
import { Formik } from 'formik';
import { CustomTextInput } from './../Form/TextInput';
import { AdminSubmitButton } from './../Form/Buttons';
import AdminLayout from './../Layouts/AdminLayout';
import { CustomSelectInput } from './../Form/TextInput';
import {
    ComponentLoading,
    CustomError,
    Steppler
} from '../Common/Common';
import { SuccesModal } from '../Common/Dialog';
import { Link } from 'react-router-dom';
import { PathBrokerManagment } from '../../constants';
import { inviteBrokerValidationSchema } from './Validations';
import { BrokerActivity } from './../../constants';

const Form = props => {
    const {
        values: { partnerId, firstName, lastName, middlename, email },
        errors,
        touched,
        handleChange,
        submitCount,
        setFieldTouched,
        setFieldValue,
        formError,
        handleBlur,
        validateField,
        rootstore,
        submitForm,
        isValid
    } = props;

    const preSubmit = e => {
        e.preventDefault();
        submitForm(e);
    };

    const change = (name, e) => {
        e.persist();
        handleChange(e);
        setFieldTouched(name, true, false);
    };
    const changeSelect = (name, option) => {
        setFieldTouched(name, true, false);
        setFieldValue(name, option.value);
    };
    const partners = (props.rootstore.partnerStore.getPartners || []).map(
        p => ({
            value: p.id,
            label: p.name
        })
    );

    const validEmailHelperText =
        touched.email && errors.email ? errors.email : '';
    const uniqueEmailHelperText =
        !errors.email && touched.uniqueEmail && errors.uniqueEmail
            ? errors.uniqueEmail
            : '';

    const emailHelperText = validEmailHelperText + ' ' + uniqueEmailHelperText;

    const emailHasError =
        (touched.email && Boolean(errors.email)) ||
        (touched.uniqueEmail && Boolean(errors.uniqueEmail));
    return (
        <form onSubmit={preSubmit} autoComplete="off">
            <input
                autoComplete="nope"
                name="hidden"
                type="text"
                style={{ display: 'none' }}
            />
            <CustomSelectInput
                type="text"
                value={partnerId}
                onChange={changeSelect.bind(null, 'partnerId')}
                error={touched.partnerId && Boolean(errors.partnerId)}
                id="partnerId"
                name="partnerId"
                options={partners}
                placeholder="Choose a partner"
                fullWidth
            />
            {errors.partnerId && touched.partnerId && (
                <CustomError name="partnerId">{errors.partnerId}</CustomError>
            )}
            <CustomTextInput
                type="text"
                value={firstName}
                onChange={change.bind(null, 'firstName')}
                helperText={touched.firstName ? errors.firstName : ''}
                error={touched.firstName && Boolean(errors.firstName)}
                id="firstName"
                name="firstName"
                placeholder="First Name"
                fullWidth
            />
            <CustomTextInput
                type="text"
                value={middlename}
                onChange={change.bind(null, 'middlename')}
                helperText={touched.middlename ? errors.middlename : ''}
                error={touched.middlename && Boolean(errors.middlename)}
                id="middlename"
                name="middlename"
                placeholder="Middle Name"
                fullWidth
            />
            <CustomTextInput
                type="text"
                value={lastName}
                onChange={change.bind(null, 'lastName')}
                helperText={touched.lastName ? errors.lastName : ''}
                error={touched.lastName && Boolean(errors.lastName)}
                id="lastName"
                name="lastName"
                placeholder="Last Name"
                fullWidth
            />
            <CustomTextInput
                type="text"
                value={email}
                onChange={change.bind(null, 'email')}
                autoComplete="nope"
                helperText={emailHelperText}
                error={emailHasError}
                id="email"
                name="email"
                placeholder="Email"
                fullWidth
                onBlur={e => {
                    handleBlur(e);
                    rootstore.emailExists(e.target.value).then(resp => {
                        if (!resp) {
                            setFieldValue('uniqueEmail', 'true');
                        } else {
                            setFieldValue('uniqueEmail', '');
                        }
                        setFieldTouched('uniqueEmail', true, false);
                        validateField('uniqueEmail');
                    });
                }}
            />

            {formError && formError !== '' && (
                <CustomError
                    name="formError"
                    className="in-the-center margin-bottom-20"
                >
                    {formError}
                </CustomError>
            )}

            {submitCount > 0 && !isValid && (
                <CustomError
                    name="submitCount"
                    className="in-the-center margin-bottom-20"
                >
                    Please fill all the required fields
                </CustomError>
            )}

            <AdminSubmitButton
                type="submit"
                disabled={props.rootstore.areStoresFetching}
            >
                {props.rootstore.brokerStore.isFetching
                    ? 'Sending Invite'
                    : 'Invite Broker'}
            </AdminSubmitButton>
        </form>
    );
};

const FormObserver = inject('rootstore')(observer(Form));

const SuccessInviteModal = () => {
    const content = (
        <>
            <span className="in-the-center success-text">BROKER INVITED</span>
            <p className="modal-body">
                The user will recive email notification with further
                instructions and will be able to login to the RLL® Quote System.
            </p>
            <span className="in-the-center">
                <Link to={PathBrokerManagment}>
                    <AdminSubmitButton>Broker Managment</AdminSubmitButton>
                </Link>
            </span>
        </>
    );
    return <SuccesModal content={content} />;
};

class InviteBrokerForm extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.showInviteModal = this.showInviteModal.bind(this);
        this.showError = this.showError.bind(this);
        this.state = {
            showModal: false,
            error: null
        };
    }

    componentDidMount() {
        this.props.rootstore.partnerStore.fetch();
    }

    showInviteModal() {
        this.setState({
            showModal: true
        });
    }

    showError() {
        this.setState({
            error: 'There was an error sending the request'
        });
    }

    onSubmit(values) {
        this.setState({
            error: null
        });
        this.props.rootstore.brokerStore.invite(
            values,
            this.showInviteModal,
            this.showError
        );
    }
    render() {
        const values = {
            partnerId: '',
            firstName: '',
            lastName: '',
            middlename: '',
            email: '',
            uniqueEmail: ''
        };

        const itemsCrumb = [
            {
                url: BrokerActivity,
                text: 'Current Activity'
            },
            {
                url: '/admin/brokers/invite',
                text: 'Invite new Broker'
            }
        ];

        return (
            <>
                <Steppler items={[]} itemsCrumb={itemsCrumb} />
                <div>
                    <span
                        className="sub-header-text"
                        style={{ paddingLeft: 50 }}
                    >
                        Invite new broker
                    </span>
                </div>
                {this.state.showModal && <SuccessInviteModal />}
                <div className="form-div" {...this.props}>
                    {this.props.rootstore.partnerStore.isFetching && (
                        <ComponentLoading />
                    )}
                    {!this.props.rootstore.partnerStore.isFetching && (
                        <Formik
                            render={props => (
                                <FormObserver
                                    {...props}
                                    formError={this.state.error}
                                />
                            )}
                            onSubmit={this.onSubmit}
                            initialValues={values}
                            validationSchema={inviteBrokerValidationSchema}
                        />
                    )}
                </div>
            </>
        );
    }
}

const BrokerInviteObserver = inject('rootstore')(observer(InviteBrokerForm));

const BrokerInvite = () => <AdminLayout content={<BrokerInviteObserver />} />;

export default BrokerInvite;
