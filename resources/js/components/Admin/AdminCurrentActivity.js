/* eslint-disable react/prop-types */
import React from 'react';
import Grid from '@material-ui/core/Grid';
import CustomTable from '../Table';
import { SearchInput } from './../Form/TextInput';
import { observer, inject } from 'mobx-react';
import { ComponentLoading, getAmericanDate } from '../Common/Common';
import { Redirect } from 'react-router-dom';
import { GetAdminBrokerViewProperty } from './../../constants';
import AdminLayout from './../Layouts/AdminLayout';
import TableAbstact from '../TableAbstract';

class Content extends TableAbstact {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            clickedOn: null
        };
        this.store = this.props.rootstore.propertyStore;
        this.rows = [
            {
                id: 'partner',
                label: 'Partner'
            },
            {
                id: 'broker_name',
                label: 'Broker Name'
            },
            {
                id: 'property_name',
                label: 'Property Name',
                type: 'link'
            },
            {
                id: 'created_at',
                label: 'Create Date'
            },
            {
                id: 'quote',
                label: 'Quote #'
            },
            { id: 'status', label: 'Status' },
            { id: 'state', label: 'State' }
        ];
    }

    componentDidMount() {
        this.store.fetchProperties();
    }

    getPartnerName = property => {
        return (
            property.company &&
            property.company.user &&
            property.company.user.partner.name
        );
    };

    getBrokerName = property => {
        const firstName =
            (property.company &&
                property.company.user &&
                property.company.user.firstname) ||
            'N/A';

        const lastName =
            (property.company &&
                property.company.user &&
                property.company.user.lastname) ||
            'N/A';

        return firstName + ' ' + lastName;
    };

    getData = () => {
        const lowerCaseSearch = this.state.search.toLowerCase();
        let rows = this.store.properties.map(b => ({
            id: b.id,
            partner: this.getPartnerName(b),
            broker_name: this.getBrokerName(b),
            property_name: this.getLinkStyle(b.property_name),
            created_at: getAmericanDate(b.created_at),
            quote: b.quote_id,
            status: this.getStatus(b.status, true),
            state: b.state
        }));

        if (lowerCaseSearch !== '') {
            rows = rows.filter(o => {
                return `${o.property_name.props.children} ${o.created_at} ${
                    o.status.props.children
                } ${o.partner} ${o.broker_name} ${o.state} ${o.quote}`
                    .toLowerCase()
                    .includes(lowerCaseSearch);
            });
        }

        return rows;
    };

    render() {
        const propertyPath = GetAdminBrokerViewProperty(this.state.clickedOn);
        return (
            <Grid container>
                <Grid item xs={9}>
                    <h1 className="content-title">Current Activity</h1>
                    {this.state.clickedOn && (
                        <Redirect push to={propertyPath} />
                    )}
                </Grid>
                <Grid item xs={3}>
                    <SearchInput
                        className="search-input"
                        value={this.state.search}
                        onChange={this.handleChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    {this.store.isFetching ? (
                        <ComponentLoading />
                    ) : (
                        <CustomTable
                            rows={this.rows}
                            data={this.getData()}
                            orderBy="property_name"
                            order="asc"
                            rowsPerPage={25}
                            onColClick={this.onColClick}
                            onRowClick={this.onRowClick}
                        />
                    )}
                </Grid>
            </Grid>
        );
    }
}

const ContentObservable = inject('rootstore')(observer(Content));

const BrokerCurrentActivity = () => (
    <AdminLayout content={<ContentObservable />} />
);

export default BrokerCurrentActivity;
