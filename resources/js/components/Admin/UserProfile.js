import React from 'react';
import AdminLayout from './../Layouts/AdminLayout';
import {withRouter} from 'react-router';
import {observer, inject} from 'mobx-react';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from '@material-ui/core/TextField';

import {
    CustomGridRow,
    Steppler,
    ComponentLoading,
    CustomError
} from '../Common/Common';
import {Redirect} from 'react-router-dom';
import {Link} from 'react-router-dom';
import {
    CustomCheckbox,
    CustomMaterialSelectInput,
    CustomSelectInput,
    CustomSwitchInput,
    CustomTextInput
} from "../Form/TextInput";
import {AdminSubmitButton} from "../Form/Buttons";


class UserProfile extends React.Component {

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);


    }


    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        fetch('/api/form-submit-url', {
            method: 'POST',
            body: data,
        });
    }

    render() {


        const preSubmit = e => {
            e.preventDefault();
            this.submitForm(e);
        };
        return (
            <>
                <h1>Settings</h1>

                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <h3>PASSWORD DETAILS</h3>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Grid container>

                            <form onSubmit={this.handleSubmit} autoComplete="off">
                                <input
                                    autoComplete="nope"
                                    name="hidden"
                                    type="text"
                                    style={{display: 'none'}}
                                />
                                <TextField
                                    id="current_password"
                                    label="Current Password"
                                    margin="normal"
                                    fullWidth
                                />
                                <TextField
                                    id="new_password"
                                    label="New Password"
                                    margin="normal"
                                    fullWidth
                                />
                                <TextField
                                    id="confirm_new_password"
                                    label="Confirm new password"
                                    margin="normal"
                                    fullWidth
                                />

                                <AdminSubmitButton
                                    type="submit"
                                >
                                    SAVE PASSWORD
                                </AdminSubmitButton>
                            </form>
                        </Grid>
                    </ExpansionPanelDetails>
                </ExpansionPanel>


            </>
        );
    }
}

const UserProfileObservable = inject('rootstore')(observer(UserProfile));

const UserProfileObs = () => <AdminLayout content={<UserProfileObservable/>}/>;

export default UserProfileObs;
