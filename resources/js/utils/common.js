/**
 Returns the value of the property/object or the def value
 */
export function optionalAccess(obj, path, def) {
    const propNames = path.replace(/\]|\)/, '').split(/\.|\[|\(/);

    return propNames.reduce((acc, prop) => acc[prop] || def, obj);
}
