import { types, getEnv, flow } from 'mobx-state-tree';
import axios from 'axios';
import { RentalTypesArray } from './RentalTypesStore';
import { profileItem } from './LoginStore';

const StatusTypeModel = types.model('StatusTypeModel', {
    id: types.maybeNull(types.number),
    status: types.maybeNull(types.number),
    description: types.maybeNull(types.string),
    name: types.maybeNull(types.string),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const MessagesTypeModel = types.model('MessagesTypeModel', {
    id: types.maybeNull(types.number),
    text: types.maybeNull(types.string),
    user_id: types.maybeNull(types.number),
    property_id: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const BuildingConstructionTypeModel = types.model(
    'BuildingConstructionTypeModel',
    {
        id: types.maybeNull(types.number),
        name: types.maybeNull(types.string),
        status: types.maybeNull(types.number),
        created_at: types.maybeNull(types.string),
        updated_at: types.maybeNull(types.string),
        deleted_at: types.maybeNull(types.string)
    }
);

const ConstructionTypeModel = types.model('ConstructionTypeModel', {
    id: types.maybeNull(types.number),
    name: types.maybeNull(types.string),
    status: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const CompanyItemModel = types.model('CompanyItemModel', {
    id: types.maybeNull(types.number),
    user_id: types.maybeNull(types.number),
    company_name: types.maybeNull(types.string),
    contact_person: types.maybeNull(types.string),
    phone_no: types.maybeNull(types.string),
    address: types.maybeNull(types.string),
    suite: types.maybeNull(types.string),
    city: types.maybeNull(types.string),
    state: types.maybeNull(types.string),
    zip: types.maybeNull(types.string),
    user: types.maybeNull(profileItem),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const PropertyItemModel = types.model('PropertyItemModel', {
    id: types.maybeNull(types.number),
    company_id: types.maybeNull(types.number),
    rental_type_id: types.maybeNull(types.number),
    construction_type_id: types.maybeNull(types.number),
    building_construction_type_id: types.maybeNull(types.number),
    quote_id: types.maybeNull(types.number),
    property_name: types.maybeNull(types.string),
    roof_construction: types.maybeNull(types.string),
    property_software: types.maybeNull(types.string),
    website: types.maybeNull(types.string),
    phone_no: types.maybeNull(types.string),
    address: types.maybeNull(types.string),
    suite: types.maybeNull(types.string),
    city: types.maybeNull(types.string),
    state: types.maybeNull(types.string),
    zip: types.maybeNull(types.string),
    total_units: types.maybeNull(types.number),
    affordable_units: types.maybeNull(types.number),
    no_of_affordable_units: types.maybeNull(types.number),
    name_insured: types.maybeNull(types.string),
    loss_runs: types.maybeNull(types.string),
    onsite_management: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string),
    company: types.maybeNull(CompanyItemModel),
    status: types.optional(StatusTypeModel, {})
});

const MessagesTypeModelArray = types.optional(
    types.array(MessagesTypeModel),
    []
);

const PropertyItemModelArray = types.optional(
    types.array(PropertyItemModel),
    []
);

const ConstructionTypeModelArray = types.optional(
    types.array(ConstructionTypeModel),
    []
);
const BuildingConstructionTypeModelArray = types.optional(
    types.array(BuildingConstructionTypeModel),
    []
);

const PropertyItem = types
    .model('PropertyItem', {
        properties: types.optional(PropertyItemModelArray, []),
        property: types.optional(PropertyItemModel, {}),
        rentalTypes: types.optional(RentalTypesArray, []),
        constructionTypes: types.optional(ConstructionTypeModelArray, []),
        messages: types.optional(MessagesTypeModelArray, []),
        status: types.optional(StatusTypeModel, {}),
        buildingConstructionTypes: types.optional(
            BuildingConstructionTypeModelArray,
            []
        ),
        isFetching: types.optional(types.boolean, false),
        isFormFetching: types.optional(types.boolean, false),
        isPropertyByIdFetching: types.optional(types.boolean, false),
        isSubmittingPropertySummary: types.optional(types.boolean, false)
    })
    .actions(self => {
        const logger = getEnv(self).Logger;

        function* getById(id) {
            logger.info('PropertyItem:fetch');
            try {
                self.isFetching = true;
                self.isPropertyByIdFetching = true;
                const res = yield axios.get('/api/broker/property/' + id);
                logger.info(res, 'PropertyItem:fetch');
                self.isPropertyByIdFetching = false;
                self.isFetching = false;
                self.property = PropertyItemModel.create(
                    res.data.payload.property
                );

                if (res.data.payload.messages) {
                    self.messages = MessagesTypeModelArray.create(
                        res.data.payload.messages
                    );
                }

                if (res.data.payload.status) {
                    self.status = StatusTypeModel.create(
                        res.data.payload.status
                    );
                }

            } catch (err) {
                self.isPropertyByIdFetching = false;
                self.isFetching = false;
                logger.error(err, 'PropertyItem:fetch');
            }
        }
        function* fetchFormDefaults() {
            logger.info('PropertyItem:fetchFormDefaults');
            try {
                self.isFetching = true;
                const res = yield axios.get('/api/property/defaults');
                logger.info(res, 'PropertyItem:fetchFormDefaults');
                self.rentalTypes = RentalTypesArray.create(
                    res.data.rental_types
                );
                self.constructionTypes = ConstructionTypeModelArray.create(
                    res.data.construction_types
                );
                self.buildingConstructionTypes = BuildingConstructionTypeModelArray.create(
                    res.data.building_construction_types
                );
                self.isFetching = false;
            } catch (err) {
                self.isFetching = false;
                logger.error(err, 'fetchFormDefaults:fetch');
            }
        }

        function* fetchProperties() {
            logger.info('PropertyItem:fetchProperties');
            try {
                self.isFetching = true;
                const res = yield axios.get('/api/broker/property');
                logger.info(res, 'PropertyItem:fetchProperties');
                self.isFetching = false;
                self.properties = PropertyItemModelArray.create(
                    res.data.payload.properties
                );
            } catch (err) {
                self.isFetching = false;
                logger.error(err, 'fetchProperties:fetch');
            }
        }

        function* newProperty(values, callback, errorCb) {
            logger.info(values);
            self.isFetching = true;
            let formData = new FormData();
            formData.append('file', values.propertyLossRuns);
            formData.append('values', JSON.stringify(values));
            try {
                const r = yield axios.post('/api/broker/property', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                });
                self.isFetching = false;
                logger.info(r);
                callback(r.data);
            } catch (err) {
                self.isFetching = false;
                logger.warn(err);
                errorCb();
            }
        }

        function* updateProperty(values, id, callback, errorCb) {
            logger.info(values);
            self.isFetching = true;
            let formData = new FormData();
            // only include the file if it is a valid file
            if (values.propertyLossRuns && values.propertyLossRuns.size > 0) {
                formData.append('file', values.propertyLossRuns);
            }
            formData.append('_method', 'PUT');
            formData.append('values', JSON.stringify(values));
            try {
                const r = yield axios.post(
                    '/api/broker/property/' + id,
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                );
                self.isFetching = false;
                logger.info(r);
                callback(r.data);
            } catch (err) {
                self.isFetching = false;
                logger.warn(err);
                errorCb();
            }
        }

        function* postPropertySummary(values, id, callback, errorCb) {
            logger.info(values);
            self.isSubmittingPropertySummary = true;
            try {
                const r = yield axios.post(
                    '/api/broker/property/' + id + '/summary',
                    { ...values, _method: 'PUT' }
                );
                self.isSubmittingPropertySummary = false;
                logger.info(r);
                callback(r.data);
            } catch (err) {
                self.isSubmittingPropertySummary = false;
                logger.warn(err);
                errorCb();
            }
        }

        function* getByIdAndFormDefaults(id) {
            logger.info('getByIdAndFormDefaults:start');
            self.isFormFetching = true;
            if (id) {
                yield self.getById(id);
            }
            yield self.fetchFormDefaults();
            logger.info('getByIdAndFormDefaults:end');
            self.isFormFetching = false;
        }

        return {
            getByIdAndFormDefaults: flow(getByIdAndFormDefaults),
            getById: flow(getById),
            fetchFormDefaults: flow(fetchFormDefaults),
            fetchProperties: flow(fetchProperties),
            newProperty: flow(newProperty),
            updateProperty: flow(updateProperty),
            postPropertySummary: flow(postPropertySummary)
        };
    })
    .views(self => ({
        get getRentalTypes() {
            return self.rentalTypes;
        },
        get getConstructionTypes() {
            return self.constructionTypes;
        }
    }));

export default PropertyItem;
