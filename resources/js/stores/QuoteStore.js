import { types, getEnv, flow } from 'mobx-state-tree';
import axios from 'axios';
import { PersonalCoveragesArray } from './PersonalCoveragesStore';

const PropertyQuoteTypeModel = types.model('PropertyQuoteTypeModel', {
    id: types.maybeNull(types.number),
    property_id: types.maybeNull(types.number),
    pdlw_option_id: types.maybeNull(types.number),
    base_price: types.maybeNull(types.string),
    personal_coverage_id: types.maybeNull(types.number),
    enhancement_id: types.maybeNull(types.number),
    selected: types.maybeNull(types.number),
    status: types.maybeNull(types.number),
    viewable: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const DefaultQuoteTypeItem = types.model('DefaultQuoteTypeItem', {
    id: types.maybeNull(types.number),
    pdlw_option_id: types.maybeNull(types.number),
    base_price_id: types.maybeNull(types.number),
    personal_coverage_id: types.maybeNull(types.number),
    enhancement_id: types.maybeNull(types.number),
    type: types.maybeNull(types.string),
    status: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const PdlwOptionTypeItem = types.model('PdlwOptionTypeItem', {
    id: types.maybeNull(types.number),
    name: types.maybeNull(types.string),
    status: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const BasePriceTypeItem = types.model('BasePriceTypeItem', {
    id: types.maybeNull(types.number),
    rental_type_id: types.maybeNull(types.number),
    status: types.maybeNull(types.number),
    price: types.maybeNull(types.string),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const EnhancementsTypeItem = types.model('EnhancementsTypeItem', {
    id: types.maybeNull(types.number),
    status: types.maybeNull(types.number),
    name: types.maybeNull(types.string),
    price: types.maybeNull(types.string),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const PropertyQuoteTypeModelArray = types.optional(
    types.array(PropertyQuoteTypeModel),
    []
);

export const DefaultQuoteTypesArray = types.optional(
    types.array(DefaultQuoteTypeItem),
    []
);
export const DefaultPdlwOptionTypesArray = types.optional(
    types.array(PdlwOptionTypeItem),
    []
);
export const BasePriceTypesArray = types.optional(
    types.array(BasePriceTypeItem),
    []
);
export const EnhancementTypesArray = types.optional(
    types.array(EnhancementsTypeItem),
    []
);

const QuoteItem = types
    .model({
        defaultQuotesTypes: types.optional(DefaultQuoteTypesArray, []),
        pdlwOptionTypes: types.optional(DefaultPdlwOptionTypesArray, []),
        basePriceTypes: types.optional(BasePriceTypesArray, []),
        personalCoverageTypes: types.optional(PersonalCoveragesArray, []),
        enhancementsTypes: types.optional(EnhancementTypesArray, []),
        propertyQuotes: types.optional(PropertyQuoteTypeModelArray, []),
        isFetchingDefaultQuotes: types.optional(types.boolean, false),
        isFetching: types.optional(types.boolean, false),
    })
    .actions(self => {
        const logger = getEnv(self).Logger;

        function* fetchPropertyQuotes(propertyId) {
            logger.info('QuoteItem:fetchPropertyQuotes');
            try {
                self.isFetchingDefaultQuotes = true;
                const res = yield axios.get('/api/property-quotes/'+ propertyId);
                logger.info(res, 'QuoteItem:fetchPropertyQuotes');
                self.isFetchingDefaultQuotes = false;

                self.propertyQuotes = PropertyQuoteTypeModelArray.create(
                    res.data.payload.quotes
                );
                self.defaultQuotesTypes = DefaultQuoteTypesArray.create(
                    res.data.payload.default_quotes
                );
                self.pdlwOptionTypes = DefaultPdlwOptionTypesArray.create(
                    res.data.payload.pdwl_options
                );
                self.basePriceTypes = BasePriceTypesArray.create(
                    res.data.payload.base_prices
                );
                self.personalCoverageTypes = PersonalCoveragesArray.create(
                    res.data.payload.personal_coverages
                );
                self.enhancementsTypes = EnhancementTypesArray.create(
                    res.data.payload.enhancements
                );
            } catch (err) {
                self.isFetchingDefaultQuotes = false;
                logger.error(err, 'QuoteItem:fetchPropertyQuotes');
            }
        }

        function* createQuotes(values, callback, errorCb) {
            logger.info(values);
            self.isFetching = true;
            try {
                const r = yield axios.post('/api/admin/quote', values);
                self.isFetching = false;
                logger.info(r);
                callback(r.data);
            } catch (err) {
                self.isFetching = false;
                logger.warn(err);
                errorCb();
            }
        }

        function* submitQuoteBroker(values, propertyId, callback, errorCb) {
            logger.info('VALUES');
            logger.info(values);
            self.isFetching = true;
            try {
                const r = yield axios.post(
                    '/api/broker/quote/' + propertyId,
                    values
                );
                self.isFetching = false;
                logger.info(r);
                callback(r.data);
            } catch (err) {
                self.isFetching = false;
                logger.warn(err);
                errorCb();
            }
        }

        function* bindQuote(propertyId, callback, errorCb) {
            logger.info(propertyId);
            self.isFetching = true;
            try {
                const r = yield axios.post(
                    '/api/admin/quote/bind/' + propertyId
                );
                self.isFetching = false;
                logger.info(r);
                callback(r.data);
            } catch (err) {
                self.isFetching = false;
                logger.warn(err);
                errorCb();
            }
        }

        return {
            fetchPropertyQuotes: flow(fetchPropertyQuotes),
            createQuotes: flow(createQuotes),
            submitQuoteBroker: flow(submitQuoteBroker),
            bindQuote: flow(bindQuote),
        };
    });

export default QuoteItem;
