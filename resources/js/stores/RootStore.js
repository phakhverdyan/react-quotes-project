import { types, flow, getEnv } from 'mobx-state-tree';
import BrokerStore from './BrokerStore';
import PartnerStore from './PartnerStore';
import RentalTypesStore from './RentalTypesStore';
import PersonalCoveragesStore from './PersonalCoveragesStore';
import PropertyStore from './PropertyStore';
import QuoteStore from './QuoteStore';
import Logger from '../utils/Logger';
import makeInspectable from 'mobx-devtools-mst';
import axios from 'axios';

const RootStore = types
    .model('RootStore', {
        isFetching: types.optional(types.boolean, false),
        brokerStore: types.optional(BrokerStore, {}),
        partnerStore: types.optional(PartnerStore, {}),
        rentalTypesStore: types.optional(RentalTypesStore, {}),
        personalCoveragesStore: types.optional(PersonalCoveragesStore, {}),
        propertyStore: types.optional(PropertyStore, {}),
        quoteStore: types.optional(QuoteStore, {}),
    })
    .actions(self => {
        const logger = getEnv(self).Logger;
        return {
            emailExists: flow(function* emailExists(email) {
                try {
                    self.isFetching = true;
                    const r = yield axios.post('/api/admin/check-email', {
                        email: email
                    });
                    self.isFetching = false;
                    return r && r.data && r.data.exists;
                } catch (err) {
                    self.isFetching = false;
                    logger.error(err, 'emailExists');
                    return true;
                }
            }),
        };
    })
    .views(self => ({
        get areStoresFetching() {
            return (
                self.isFetching ||
                self.brokerStore.isFetching ||
                self.partnerStore.isFetching ||
                self.rentalTypesStore.isFetching ||
                self.personalCoveragesStore.isFetching
            );
        }
    }));

export default makeInspectable(RootStore.create({}, { Logger }));
