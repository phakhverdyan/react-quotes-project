import { types, getEnv } from 'mobx-state-tree';
import { flow } from 'mobx-state-tree';
import axios from 'axios';
import { profileItem } from './LoginStore';

const BrokersArray = types.optional(types.array(profileItem), []);

const BrokersItem = types
    .model('Brokers', {
        brokers: types.optional(BrokersArray, []),
        isFetching: types.optional(types.boolean, false)
    })
    .actions(self => {
        const logger = getEnv(self).Logger;

        function setBrokers(res) {
            logger.info(res, 'set brokers');
            self.isFetching = false;
            if (res && res.data) {
                self.brokers = BrokersArray.create(res.data);
            }
        }

        function getBrokersError(res) {
            logger.error(res, 'set brokers');
            self.isFetching = false;
        }

        function fetchBrokers() {
            logger.info('fetchBrokers brokers');
            self.isFetching = true;
            axios
                .get('/api/brokers')
                .then(self.setBrokers)
                .catch(self.getBrokersError);
        }

        function* invite(values, callback, errorCb) {
            self.isFetching = true;
            try {
                const r = yield axios.post('/api/broker/invite', {
                    firstname: values.firstName,
                    partner_id: values.partnerId,
                    middlename: values.middlename,
                    lastname: values.lastName,
                    email: values.email,
                    password: values.password
                });
                self.isFetching = false;
                logger.info(r);
                callback();
            } catch (err) {
                self.isFetching = false;
                logger.warn(err);
                errorCb();
            }
        }

        return {
            fetchBrokers,
            setBrokers,
            getBrokersError,
            invite: flow(invite)
        };
    })
    .views(self => ({
        get getBrokers() {
            return self.brokers;
        }
    }));

export default BrokersItem;
