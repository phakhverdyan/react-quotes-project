import { types, getEnv, flow } from 'mobx-state-tree';
import axios from 'axios';

export const PersonalCoverage = types.model('PersonalCoverage', {
    id: types.maybeNull(types.number),
    amount: types.maybeNull(types.string),
    price: types.maybeNull(types.string),
    status: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

export const PersonalCoveragesArray = types.optional(
    types.array(PersonalCoverage),
    []
);

const PersonalCoverageItem = types
    .model('PersonalCoverageItem', {
        personalCoverages: types.optional(PersonalCoveragesArray, []),
        isFetching: types.optional(types.boolean, false)
    })
    .actions(self => {
        const logger = getEnv(self).Logger;

        function* fetch() {
            logger.info('PersonalCoverageItem:fetch');
            try {
                self.isFetching = true;
                const res = yield axios.get('/api/personal-coverages/all');
                logger.info(res, 'PersonalCoverageItem:fetch');
                self.isFetching = false;
                self.personalCoverages = PersonalCoveragesArray.create(
                    res.data
                );
            } catch (err) {
                self.isFetching = false;
                logger.error(err, 'PersonalCoverageItem:fetch');
            }
        }

        return {
            fetch: flow(fetch)
        };
    })
    .views(self => ({
        get getPersonalCoverages() {
            return self.personalCoverages;
        }
    }));

export default PersonalCoverageItem;
