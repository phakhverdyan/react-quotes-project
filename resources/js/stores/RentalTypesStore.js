import { types, getEnv, flow } from 'mobx-state-tree';
import axios from 'axios';

const RentalTypeItem = types.model('RentalTypeItem', {
    id: types.maybeNull(types.number),
    type: types.maybeNull(types.string),
    status: types.maybeNull(types.number),
    created_at: types.maybeNull(types.string),
    updated_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

export const RentalTypesArray = types.optional(types.array(RentalTypeItem), []);

const PartnersItem = types
    .model({
        rentalTypes: types.optional(RentalTypesArray, []),
        isFetching: types.optional(types.boolean, false)
    })
    .actions(self => {
        const logger = getEnv(self).Logger;

        function* fetch() {
            logger.info('PartnersItem:fetch');
            try {
                self.isFetching = true;
                const res = yield axios.get('/api/rental-types');
                logger.info(res, 'PartnersItem:fetch');
                self.isFetching = false;
                self.rentalTypes = RentalTypesArray.create(res.data);
                logger.info(self.rentalTypes, 'PartnersItem:fetch:self.rentalTypes');
            } catch (err) {
                self.isFetching = false;
                logger.error(err, 'PartnersItem:fetch');
            }
        }

        return {
            fetch: flow(fetch)
        };
    });

export default PartnersItem;
