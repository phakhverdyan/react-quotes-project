import { types, getEnv, onSnapshot } from 'mobx-state-tree';
import makeInspectable from 'mobx-devtools-mst';
import Logger from '../utils/Logger';
import axios from 'axios';
import { LOGIN_KEY_NAME } from './../constants';
import { optionalAccess } from './../utils/common';
import { PartnerItem } from './PartnerStore';

//{"message":"The given data was invalid.","errors":{"email":["The email field is required."],"password":["The password field is required."]}}

const errorItem = types.model('ErroItem', {
    message: types.optional(types.string, ''),
    errors: types.model({
        email: types.optional(types.array(types.string), []),
        password: types.optional(types.array(types.string), [])
    })
});

export const profileItem = types.model('ProfileItem', {
    created_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string),
    email: types.maybeNull(types.string),
    email_verified_at: types.maybeNull(types.string),
    firstname: types.maybeNull(types.string),
    id: types.maybeNull(types.number),
    lastname: types.maybeNull(types.string),
    middlename: types.maybeNull(types.string),
    partner_id: types.maybeNull(types.number),
    partner: types.maybeNull(PartnerItem),
    updated_at: types.maybeNull(types.string)
});

export const LoginItem = types
    .model('LoginItem', {
        isFetching: types.optional(types.boolean, false),
        isAdmin: types.optional(types.boolean, false),
        error: types.maybeNull(errorItem),
        profile: types.maybeNull(profileItem),
        remember: types.optional(types.boolean, false),
        token: types.maybeNull(types.string),
        isLayoutExtended: types.optional(types.boolean, true),
        axiosUnAuthorizedInterceptor: types.maybeNull(types.integer)
    })
    .actions(self => {
        const logger = getEnv(self).Logger;

        function setProfile(profile) {
            self.profile = profileItem.create(profile);
        }

        function onLogin(res) {
            logger.info(res, 'LoginItem.onLogin:token');
            self.error = null;
            self.isFetching = false;
            if (res.data.token) {
                self.token = res.data.token;
                Logger.info(res.data.token, 'Setting token from /token');
                delete axios.defaults.headers.common['Authorization'];
                axios.defaults.headers.common['Authorization'] = `Bearer ${
                    res.data.token
                }`;
                self.addAxiosUnauthorizedInterceptor();
                self.setProfile(res.data.profile);
                self.isAdmin = res.data.isAdmin;
            } else {
                self.buildErrorFromString('Invalid Token Response');
            }
        }

        function buildErrorFromString(text) {
            logger.info(text, 'LoginItem.buildErrorFromString');
            self.error = errorItem.create({
                message: text,
                errors: {
                    email: [text]
                }
            });
        }

        function buildErrorFromResponse(res) {
            logger.info(res, 'LoginItem.buildErrorFromResponse');
            if (res && res.data) {
                self.error = errorItem.create({
                    message: optionalAccess(res, 'data.message', ''),
                    errors: {
                        email: [
                            optionalAccess(res, 'data.errors.email[0]', '')
                        ],
                        password: [
                            optionalAccess(res, 'data.errors.password[0]', '')
                        ]
                    }
                });
            } else {
                self.buildErrorFromString('Invalid Server Response');
            }
        }

        function onLoginError(res) {
            logger.error(res, 'LoginItem.onLoginError');
            self.remember = false;
            const response = res.response;
            self.buildErrorFromResponse(response);
            self.isFetching = false;
        }

        function doLogin(remember, username, password) {
            logger.info('doLogin', { username, password });
            self.error = null;
            self.isFetching = true;
            self.remember = remember;
            self.token = null;
            axios
                .post('/api/token', {
                    email: username,
                    password
                })
                .then(self.onLogin)
                .catch(self.onLoginError);
        }
        function doLogout() {
            logger.info('doLogout', 'doLogout');
            self.isFetching = false;
            self.token = null;
            axios.defaults.headers.common['Authorization'] = null;
            delete axios.defaults.headers.common['Authorization'];
            if (self.axiosUnAuthorizedInterceptor !== null) {
                logger.info(
                    self.axiosUnAuthorizedInterceptor,
                    'ejecting interceptor doLogout'
                );
                axios.interceptors.request.eject(
                    self.axiosUnAuthorizedInterceptor
                );
            }
        }

        return {
            doLogin,
            onLogin,
            setProfile,
            onLoginError,
            buildErrorFromResponse,
            buildErrorFromString,
            doLogout,
            clearError: function() {
                self.error = null;
            },
            toggleExtended: function() {
                self.isLayoutExtended = !self.isLayoutExtended;
            },
            addAxiosUnauthorizedInterceptor: function() {
                logger.info(
                    'addAxiosUnauthorizedInterceptor',
                    'addAxiosUnauthorizedInterceptor'
                );
                if (self.axiosUnAuthorizedInterceptor === null) {
                    self.axiosUnAuthorizedInterceptor = axios.interceptors.response.use(
                        function(response) {
                            return response;
                        },
                        function(error) {
                            if (
                                error &&
                                error.response &&
                                error.response.status === 401
                            ) {
                                self.doLogout();
                            }
                            return Promise.reject(error);
                        }
                    );
                }
            },
            afterCreate() {
                logger.info('afterCreate', 'afterCreate');
                self.axiosUnAuthorizedInterceptor = null;
                self.addAxiosUnauthorizedInterceptor();
                Logger.info(self.axiosUnAuthorizedInterceptor, 'afterCreate');
            }
        };
    })
    .views(self => ({
        get firstName() {
            return optionalAccess(self.profile, 'firstname', 'Guest');
        },
        get isLoggedIn() {
            return self.token !== null;
        },
        get hasErrors() {
            return self.error;
        },
        get hasErrorMessage() {
            return optionalAccess(self, 'error.message', false);
        },
        get getErrorMessage() {
            return optionalAccess(self, 'error.message', '');
        },
        get hasEmailError() {
            return optionalAccess(self, 'error.errors.email[0]', false);
        },
        get getEmailError() {
            return optionalAccess(self, 'error.errors.email[0]', '');
        },
        get hasPasswordError() {
            return optionalAccess(self, 'error.errors.password[0]', false);
        },
        get getPasswordError() {
            return optionalAccess(self, 'error.errors.password[0]', '');
        }
    }));

export const getLoginModel = () => {
    let initialState = {};

    if (localStorage.getItem(LOGIN_KEY_NAME)) {
        Logger.info(
            'Getting login store from local storage',
            'LoginItem:getLoginModel'
        );
        let json = JSON.parse(localStorage.getItem(LOGIN_KEY_NAME));
        if (LoginItem.is(json)) {
            Logger.info(
                'Setting login store from local storage',
                'LoginItem:getLoginModel'
            );
            if (json) {
                initialState = json;
                if (initialState.token) {
                    Logger.info(
                        initialState.token,
                        'Setting token from initial state'
                    );
                    delete axios.defaults.headers.common['Authorization'];
                    axios.defaults.headers.common['Authorization'] = `Bearer ${
                        initialState.token
                    }`;
                }
            }
        } else {
            Logger.warn(
                'Store in local storage does not match current model',
                'LoginItem:getLoginModel'
            );
        }
    }

    const loginItem = LoginItem.create(initialState, { Logger });

    // every time the store changes, save the new snapshot
    onSnapshot(loginItem, snapshot => {
        Logger.info(snapshot, 'LoginItem:onSnapshot');
        localStorage.setItem(LOGIN_KEY_NAME, JSON.stringify(snapshot));
    });

    return loginItem;
};

const loginStore = getLoginModel();
export default makeInspectable(loginStore);
