import { types, getEnv } from 'mobx-state-tree';
import axios from 'axios';

export const PartnerItem = types.model('PartnerItem', {
    id: types.maybeNull(types.number),
    name: types.maybeNull(types.string),
    created_at: types.maybeNull(types.string),
    deleted_at: types.maybeNull(types.string)
});

const PartnersArray = types.optional(types.array(PartnerItem), []);

const PartnersItem = types
    .model('PartnersItem', {
        partners: types.optional(PartnersArray, []),
        isFetching: types.optional(types.boolean, false)
    })
    .actions(self => {
        const logger = getEnv(self).Logger;

        function set(res) {
            logger.info(res, 'set partners');
            self.isFetching = false;
            if (res && res.data) {
                self.partners = PartnersArray.create(res.data);
            }
        }

        function getError(res) {
            logger.error(res, 'set partners');
            self.isFetching = false;
        }

        function fetch() {
            logger.info('fetch partners');
            self.isFetching = true;
            axios
                .get('/api/partners')
                .then(self.set)
                .catch(self.getError);
        }

        return {
            fetch,
            set,
            getError
        };
    })
    .views(self => ({
        get getPartners() {
            return self.partners;
        }
    }));

export default PartnersItem;
