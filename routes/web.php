<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
 ///       $this->post('login', 'Auth\LoginController@login');
 //       $this->post('logout', 'Auth\LoginController@logout')->name('logout');

Route::prefix('marcellus')->middleware('auth')->group(function () {
    

    Route::get("/", "MarcellusController@index");
    Route::get('email-invitation', "MarcellusController@emailInvitation" );
});

Route::get('auth/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('auth/login', 'Auth\LoginController@login')->name('auth.login.post');
Route::post('auth/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/user/accept-invitation/{token}', 'UserController@acceptInvitation');


Route::view('/{path?}', 'welcome')
    ->where('path', '.*')
    ->name('react');


//Route::get('/', function () {
//    return view('welcome');
//})->middleware('auth');

//Route::get('/home', 'HomeController@index')->name('home');//->middleware('verified');
