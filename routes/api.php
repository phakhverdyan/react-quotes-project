<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/token', 'UserController@token')->name('token');//->middleware('verified');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
Route::get($uri, $callback);
Route::post($uri, $callback);
Route::put($uri, $callback);
Route::patch($uri, $callback);
Route::delete($uri, $callback);
Route::options($uri, $callback);
*/

Route::middleware('auth:api')->post('/admin/check-email', 'UserController@email_exists')->name('admin-check-email');
Route::middleware('auth:api')->post('/admin/quote', 'AdminPropertyQuoteController@store')->name('admin-property-quote-store');
Route::middleware('auth:api')->post('/admin/quote/{property}/bound', 'AdminPropertyQuoteController@bound')->name('admin-property-quote-bound' );
Route::middleware('auth:api')->post('/admin/quote/{property}', 'AdminPropertyQuoteController@rejected' )->name('admin-property-quote' );

Route::middleware('auth:api')->get('/brokers', "UserController@brokers")->name( 'brokers' );
Route::middleware('auth:api')->post('/broker/invite', "UserController@create")->name( 'broker-create' );
Route::middleware('auth:api')->get('/partners', "PartnerController@partners")->name( 'partners' );


Route::middleware('auth:api')->get('/broker/property', "PropertyController@index")->name( 'broker-property-all' );
Route::middleware('auth:api')->post('/broker/property', "PropertyController@create")->name( 'broker-property-create' );
Route::middleware('auth:api')->get('/broker/property/{property}', "PropertyController@show")->name('broker-property-show' );
Route::middleware('auth:api')->put('/broker/property/{property}', "PropertyController@update")->name('broker-property-update' );
Route::middleware('auth:api')->put('/broker/property/{property}/summary', "PropertyController@updateSummary")->name('broker-property-update-summary');
Route::middleware('auth:api')->get('/broker/property/{property}/loss-runs', 'PropertyController@lossRuns')->name('broker-property-loss-runs' );
Route::middleware('auth:api')->get('/broker/quotes/{property}', 'PropertyQuoteController@byProperty')->name('admin-property-quote-store');
Route::middleware('auth:api')->post('/broker/quote/{property}', 'PropertyQuoteController@bind')->name('broker-bind-quote');


//Route::middleware('auth:api')->get('/rental-types', "RentalTypesController@all")->name('rental-types');
//Route::middleware('auth:api')->post('/rental-types', "RentalTypesController@create")->name('rental-types');
//Route::middleware('auth:api')->get('/rental-types/{id}', "RentalTypesController@show")->name('rental-types');
//Route::middleware('auth:api')->put('/rental-types/{id}', "RentalTypesController@update")->name('rental-types');

Route::middleware('auth:api')->get('/property/defaults', "PropertyController@defaults")->name('property-default-data');

Route::middleware('auth:api')->get('/property-quotes/defaults', "PropertyQuoteController@defaults")->name('property-quotes-default-data');
Route::middleware('auth:api')->get('/property-quotes/{property}', "PropertyQuoteController@show")->name( 'property-quotes' );


//Route::middleware('auth:api')->get('/rental-types/all', "RentalTypeController@all")->name( 'rental-types' );
Route::middleware('auth:api')->get('/personal-coverages/all', "PersonalCoveragesController@all")->name( 'rental-types' );

Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found.'], 404);
});