<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing PartnerSeeder" . PHP_EOL;

        $partners = json_decode(file_get_contents(__DIR__ . '/../models/partners.json'), true);

        foreach ($partners as $partnerKey => $partnerVal) {
            DB::table('partners')->insert([
                'name' => $partnerVal[ 'name' ],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}