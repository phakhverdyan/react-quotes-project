<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserInvitation;
use Carbon\Carbon;

class UserInvitationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::withRole('broker')->get();

        foreach ($users as $user) {
            $invitation = new UserInvitation([
                    'token' => Str::Random(40),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            $user->userInvitation()->save( $invitation );
        }
    }
}
