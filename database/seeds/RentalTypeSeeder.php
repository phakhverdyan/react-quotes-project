<?php

use Illuminate\Database\Seeder;
use App\Models\RentalType;
use Carbon\Carbon;

class RentalTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing RentalTypeSeeder" . PHP_EOL;

        $rentalTypes = json_decode(file_get_contents(__DIR__ . '/../models/rental_types.json'), true);

       

        foreach ($rentalTypes as $rentalTypeKey => $rentalTypeVal) {

            $rentalType = new RentalType();

            $rentalType->id = $rentalTypeVal[ 'id' ];
            $rentalType->type = $rentalTypeVal[ 'type' ];
            $rentalType->status = $rentalTypeVal[ 'status' ];
            $rentalType->created_at = Carbon::now();
            $rentalType->updated_at = Carbon::now();
            
            $rentalType->save();

        
        }
    }
}
