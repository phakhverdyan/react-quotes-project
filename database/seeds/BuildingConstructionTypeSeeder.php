<?php

use Illuminate\Database\Seeder;
use App\Models\BuildingConstructionType;
use Carbon\Carbon;

class BuildingConstructionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing BuildingConstructionTypeSeeder" . PHP_EOL;

        $aryBuildingConstructionTypes = json_decode(file_get_contents(__DIR__ . '/../models/building_construction_types.json'), true);
        
        foreach ($aryBuildingConstructionTypes as $buildingConstructionTypesKey => $buildingConstructionTypesVal) {
            $buildingConstructionType = new BuildingConstructionType();

            $buildingConstructionType->id = $buildingConstructionTypesVal[ 'id' ];
            $buildingConstructionType->name = $buildingConstructionTypesVal[ 'name' ];
            $buildingConstructionType->status = $buildingConstructionTypesVal[ 'status' ];
            $buildingConstructionType->created_at = Carbon::now();
            $buildingConstructionType->updated_at = Carbon::now();
            
            $buildingConstructionType->save();
        }
    }
}
