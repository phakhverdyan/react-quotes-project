<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
//use Illuminate\Support\Facades\Str;

class MyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing MyUserSeeder" . PHP_EOL;

        $users = json_decode(file_get_contents(__DIR__ . '/../models/users.json'), true);

        foreach ($users as $userKey => $userVal) {
            DB::table('users')->insert([
                'partner_id'=>$userVal[ 'partner_id' ],
                'firstname'=>$userVal[ 'firstname' ],
                'middlename'=>$userVal[ 'middlename' ],
                'lastname'=>$userVal[ 'lastname' ],
                'email' => $userVal[ 'email' ],
                'password'=>Hash::make(Crypt::decrypt(Config::get("app.password"))),
                'api_token' => Str::random(60),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
        DB::commit();
    }
}