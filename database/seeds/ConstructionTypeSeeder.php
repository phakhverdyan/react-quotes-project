<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Models\ConstructionType;

class ConstructionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing ConstructionTypeSeeder" . PHP_EOL;

        $aryConstructionTypes = json_decode(file_get_contents(__DIR__ . '/../models/construction_types.json'), true);

        foreach ($aryConstructionTypes as $constructionTypeKey => $constructionTypeVal) {

            $constructionType = new ConstructionType();

            $constructionType->id = $constructionTypeVal[ 'id' ];
            $constructionType->name = $constructionTypeVal[ 'name' ];
            $constructionType->status = $constructionTypeVal[ 'status' ];
            
            $constructionType->created_at = Carbon::now();
            $constructionType->updated_at = Carbon::now();
            
            $constructionType->save();
        }
    }
}
