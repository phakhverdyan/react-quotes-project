<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;


class BrokerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing BrokerSeeder" . PHP_EOL;

        $users = json_decode(file_get_contents(__DIR__ . '/../models/brokers.json'), true);

        $broker = Role::where([ 'name'=>'broker' ] )->first();

        foreach ($users as $userKey => $userVal) {

            $user = new User();

            $user->partner_id = $userVal[ 'partner_id' ];
            $user->firstname = $userVal[ 'firstname' ];
            $user->middlename = $userVal[ 'middlename' ];
            $user->lastname = $userVal[ 'lastname' ];
            $user->email = $userVal[ 'email' ];

            if( $userVal[ 'email_verified_at' ] != "" ) {
                $user->email_verified_at = Carbon::parse( $userVal[ 'email_verified_at' ] )->locale('us');
            }

            $user->password = Hash::make(Crypt::decrypt(Config::get("app.password")));
            $user->api_token = Str::random(60);
            $user->created_at = Carbon::now();
            $user->updated_at = Carbon::now();
            
            $user->save();

            $user->attachRole( $broker );
        }
        DB::commit();
    }
}
