<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;

class RBAC extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing RBAC Seeder" . PHP_EOL;

        //get all permissions
        $permissions = Permission::all();

        //Get super admin role
        $superAdminRole = Role::find(1);

        //loop through all permissions and add to the superAdmin Roll
        foreach( $permissions as $permission ){
            $superAdminRole->attachPermission( $permission );
        }

        //Select All Current Users
        $users = User::all();

        //loop through and add the superAdminRole to all current users.
        foreach ($users as $user) {
            $user->attachRole($superAdminRole);
        }
    }
}