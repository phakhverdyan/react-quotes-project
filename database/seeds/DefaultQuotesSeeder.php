<?php

use Illuminate\Database\Seeder;
use App\Models\DefaultQuote;
use Carbon\Carbon;

class DefaultQuotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing DefaultQuotesSeeder" . PHP_EOL;

        $aryDefaultQuotes = json_decode(file_get_contents(__DIR__ . '/../models/default_quotes.json'), true);
        
        foreach ($aryDefaultQuotes as $defaultQuotesKey => $defaultQuotesVal) {
            $defaultQuote = new DefaultQuote();

            $defaultQuote->pdlw_option_id = $defaultQuotesVal[ 'pdlw_option_id' ];
            $defaultQuote->base_price_id = $defaultQuotesVal[ 'base_price_id' ];
            $defaultQuote->personal_coverage_id = $defaultQuotesVal[ 'personal_coverage_id' ];
            $defaultQuote->enhancement_id = $defaultQuotesVal[ 'enhancement_id' ];
            $defaultQuote->status = $defaultQuotesVal[ 'status' ];
            $defaultQuote->created_at = Carbon::now();
            $defaultQuote->updated_at = Carbon::now();
            
            $defaultQuote->save();
        }
    }
}
