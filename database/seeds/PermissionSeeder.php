<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing RoleSeeder" . PHP_EOL;

        $permissions = json_decode(file_get_contents(__DIR__ . '/../models/permissions.json'), true);

        
        foreach ($permissions as $permissionKey => $permissionVal) {
            DB::table('permissions')->insert([
                'name' => $permissionVal[ 'name' ],
                'display_name' => $permissionVal[ 'display_name' ],
                'description' => $permissionVal[ 'description' ],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
