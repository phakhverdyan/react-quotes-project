<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing RoleSeeder" . PHP_EOL;

        $roles = json_decode(file_get_contents(__DIR__ . '/../models/roles.json'), true);

        foreach ($roles as $roleKey => $roleVal) {
            DB::table('roles')->insert([
                'name' => $roleVal[ 'name' ],
                'display_name' => $roleVal[ 'display_name' ],
                'description' => $roleVal[ 'description' ],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    
    }
}
