<?php

use Illuminate\Database\Seeder;
use App\Models\BasePrice;
use Carbon\Carbon;

class BasePricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing BasePricesSeeder" . PHP_EOL;

        $basePrices = json_decode(file_get_contents(__DIR__ . '/../models/base_prices.json'), true);
        
        foreach ($basePrices as $basePricesKey => $basePricesVal) {
            $basePrice = new BasePrice();

            $basePrice->id = $basePricesVal[ 'id' ];
            $basePrice->rental_type_id = $basePricesVal[ 'rental_type_id' ];
            $basePrice->price = $basePricesVal[ 'price' ];
            $basePrice->status = $basePricesVal[ 'status' ];
            $basePrice->created_at = Carbon::now();
            $basePrice->updated_at = Carbon::now();
            
            $basePrice->save();
        }
    }
}
