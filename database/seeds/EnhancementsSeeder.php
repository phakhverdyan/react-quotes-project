<?php

use Illuminate\Database\Seeder;
use App\Models\Enhancement;
use Carbon\Carbon;

class EnhancementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing EnhancementsSeeder" . PHP_EOL;

        $enhancements = json_decode(file_get_contents(__DIR__ . '/../models/enhancements.json'), true);
        
        foreach ($enhancements as $enhancementKey => $enhancementVal) {
            $enhancement = new Enhancement();

            $enhancement->id = $enhancementVal[ 'id' ];
            $enhancement->name = $enhancementVal[ 'name' ];
            $enhancement->price = $enhancementVal[ 'price' ];
            $enhancement->status = $enhancementVal[ 'status' ];
            $enhancement->created_at = Carbon::now();
            $enhancement->updated_at = Carbon::now();
            
            $enhancement->save();
        }
    }
}
