<?php

use Illuminate\Database\Seeder;
use App\Models\PdlwOption;
use Carbon\Carbon;

class PdlwOptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing PdlwOptionsSeeder" . PHP_EOL;

        $pdlwOptions = json_decode(file_get_contents(__DIR__ . '/../models/pdlw_options.json'), true);
        
        foreach ($pdlwOptions as $pdlwOptionKey => $pdlwOptionVal) {
            $pdlwOption = new PdlwOption();

            $pdlwOption->id = $pdlwOptionVal[ 'id' ];
            $pdlwOption->name = $pdlwOptionVal[ 'name' ];
            $pdlwOption->status = $pdlwOptionVal[ 'status' ];
            $pdlwOption->created_at = Carbon::now();
            $pdlwOption->updated_at = Carbon::now();
            
            $pdlwOption->save();
        }
    }
}
