<?php

use Illuminate\Database\Seeder;
use App\Models\PersonalCoverage;
use Carbon\Carbon;

class PersonalCoverageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing PersonalCoverageSeeder" . PHP_EOL;

        $personalCoverages = json_decode(file_get_contents(__DIR__ . '/../models/personal_coverages.json'), true);

       

        foreach ($personalCoverages as $personalCoverageKey => $personalCoverageVal) {

            $personalCoverage = new PersonalCoverage();

            $personalCoverage->id = $personalCoverageVal[ 'id' ];
            $personalCoverage->amount = $personalCoverageVal[ 'amount' ];
            $personalCoverage->price = $personalCoverageVal[ 'price' ];
            $personalCoverage->status = $personalCoverageVal[ 'status' ];
            $personalCoverage->created_at = Carbon::now();
            $personalCoverage->updated_at = Carbon::now();
            
            $personalCoverage->save();

        
        }
    }
}
