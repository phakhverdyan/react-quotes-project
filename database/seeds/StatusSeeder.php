<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Executing StatusSeeder" . PHP_EOL;

        $statuses = json_decode(file_get_contents(__DIR__ . '/../models/statuses.json'), true);

        foreach ($statuses as $statusKey => $statusVal) {
            DB::table('statuses')->insert([
                'id' => $statusVal[ 'id' ],
                'name' => $statusVal[ 'name' ],
                'description' => $statusVal[ 'description' ],
                'status' => $statusVal[ 'status' ],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
