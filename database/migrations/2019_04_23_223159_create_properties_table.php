<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('company_id')->comment("Foreign Key to 'company' table");
            $table->string( "property_name", 150)->comment("The Name of the Property that is admitted for a quote");
            $table->string( "phone_no", 25)->comment( "Property's Phone Number" );
            $table->string( "phone_ext", 15)->nullable()->comment( "The Extention for the Phone Number" );
            $table->string( "address", 150)->nullable()->comment( "The Property's Address");
            $table->string( "suite", 15 )->nullable()->comment( "The Property's suite for the Address" );
            $table->string( "city", 50)->nullable()->comment( "The Property's city for the Address" );
            $table->string( "state", 2)->nullable()->comment( "The Property's state for the Address" );
            $table->string( "zip", 15)->nullable()->comment( "The Property's zip for the Address" );
            $table->string( "country", 3)->nullable()->comment( "The Property's country for the Address" );
            $table->integer( "total_units" )->comment( "The total number of physical units in the property" );
            $table->boolean( "affordable_units" )->comment( "Whether the property has any affordable units or not (Section 8)" );
            $table->integer( "no_of_affordable_units" )->nullable()->comment( "The number of affordable units if any" );
            $table->string( "name_insured", 150 )->nullable()->comment( "The owner/Entity of the property" );
            $table->string( "loss_runs", 200)->comment( "the path where the file is uploaded" );
            $table->boolean( "onsite_management" )->comment( "Whether there is on-site management or not" );

            $table->timestamps();
            $table->softDeletes();

            $table->foreign( "company_id" )->references( 'id' )->on( 'companies' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
