<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyTableMakeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function ($table) {
            $table->string( "company_name", 200)->default('N/A')->comment("The company's name that manages/handles the property/ies")->change();
            $table->string( "contact_person", 100)->nullable()->comment( "The First and Lastname of the contact person for the company" )->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function ($table) {
            $table->string( "company_name", 200)->comment("The company's name that manages/handles the property/ies")->change();
            $table->string( "contact_person", 100)->comment( "The First and Lastname of the contact person for the company" )->change();
        });
    }
}
