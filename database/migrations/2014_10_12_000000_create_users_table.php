<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname', 75)->comment( "Broker's Firstname" );
            $table->string('middlename', 75)->comment( "Broker's Middle Name" );
            $table->string('lastname', 75)->comment( "Broker's Lastname" );
            $table->string('email', 75)->unique()->comment("Broker's Email Address. It is used by the broker in order to log in to the system.");
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 150)->comment("The password the broker uses in order to Log In to the system.");
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
