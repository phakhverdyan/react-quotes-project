<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class InsertProducerUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $broker = Role::where([ 'name'=>'super-admin' ] )->first();

        $user = new User();

        $user->partner_id = 1;
        $user->firstname = "Producers";
        $user->middlename = "";
        $user->lastname = "Account";
        $user->email = "producers@rllinsure.com";
        
        $user->email_verified_at = Carbon::now()->locale('us');

        $user->password = Hash::make(Crypt::decrypt(Config::get("app.password")));
        $user->api_token = Str::random(60);
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        
        $user->save();

        $user->attachRole( $broker );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = User::where( ['email'=>'producers@rllinsure.com'])->first();
        $user->forceDelete();
    }
}
