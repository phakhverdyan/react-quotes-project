<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertyTableSetDefaultNoOfAffordableUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function ($table) {
            $table->integer( "no_of_affordable_units" )->nullable()->default(0)->comment( "The number of affordable units if any" )->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function ($table) {
            $table->integer( "no_of_affordable_units" )->nullable()->comment( "The number of affordable units if any" )->change();
        });
    }
}
