<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('property_id')->comment("Foreign Key to 'Properties' table");
            $table->string( "building_number", 25 )->comment( "The number that is assigned to a specific building" );
            $table->integer( "year_built" )->comment( "The Year the building was build" );
            $table->integer( "number_of_stories" )->comment( "How many stories the building has." );

            $table->timestamps();
            $table->softDeletes();

            $table->foreign( "property_id" )->references( 'id' )->on( 'properties' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buildings');
    }
}
