<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertiesTableWithNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function ($table) {
            $table->unsignedBigInteger('construction_type_id')->after('rental_type_id')->comment("Foreign Key to 'construction_types' table");
            $table->unsignedBigInteger('building_construction_type_id')->after('construction_type_id')->comment("Foreign Key to 'bulding_construction_types' table");
            $table->string('roof_construction', 150)->after('onsite_management');
        });

        Schema::table('properties', function ($table) {
            //have to set data or else we are going to get a invalid contraint.
            DB::statement("UPDATE properties SET construction_type_id = 1, building_construction_type_id =1");

            $table->foreign( "construction_type_id" )->references( 'id' )->on( 'construction_types' );
            $table->foreign( "building_construction_type_id" )->references( 'id' )->on( 'building_construction_types' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('properties', 'construction_type_id' )) {
            Schema::table("properties", function ($table) {
                $table->dropForeign('properties_construction_type_id_foreign');
                $table->dropColumn('construction_type_id');
            });
        }

        if (Schema::hasColumn('properties', 'building_construction_type_id' )) {
            Schema::table("properties", function ($table) {
                $table->dropForeign('properties_building_construction_type_id_foreign');
                $table->dropColumn('building_construction_type_id');
            });
        }

        if (Schema::hasColumn('properties', 'roof_construction' )) {
            Schema::table("properties", function ($table) {
                $table->dropColumn('roof_construction');
            });
        }
    }
}
