<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Property;

class AddQuoteIdToProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->unsignedBigInteger('quote_id')->nullable()->after('building_construction_type_id');
        });
        $properties = Property::all();
        $id = 1001;
        foreach ($properties as $property)
        {
            $property->quote_id = $id;
            $property->save();
            $id++;

        }
        Schema::table('properties', function (Blueprint $table) {
            $table->unsignedBigInteger('quote_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('properties', 'quote_id' )) {

            Schema::table('properties', function (Blueprint $table) {
                $table->dropColumn('quote_id');
            });
        }
    }
}
