<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertyQuotesTableSelected extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_quotes', function ($table) {
            $table->boolean("selected")->after( "enhancement_id" )->default(0);
            $table->renameColumn( "include", "viewable" );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_quotes', function ($table) {
            $table->dropColumn('selected');
            $table->renameColumn( "viewable", "include" );
        });
    }
}
