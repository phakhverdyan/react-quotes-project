<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertyTableAddRentalTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'properties', function (Blueprint $table) {
            $table->unsignedBigInteger("rental_type_id")->after( "company_id" )->comment("Foreign Key to 'rental_types' table");

            $table->foreign( "rental_type_id" )->references( 'id' )->on( 'rental_types' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('properties', 'rental_type_id' )) {
            Schema::table("properties", function ($table) {
            
                $table->dropForeign('properties_rental_type_id_foreign');
                $table->dropColumn('rental_type_id');
            });
        }
    }
}
