<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersForeignKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'users', function (Blueprint $table) {
            $table->unsignedBigInteger("partner_id")->after( "id" )->comment("Foreign Key to 'Partners' table");

            $table->foreign( "partner_id" )->references( 'id' )->on( 'partners' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'partner_id' )) {
            Schema::table("users", function ($table) {
            
                $table->dropForeign('users_partner_id_foreign');
                $table->dropColumn('partner_id');
            });
        }
    }
}
