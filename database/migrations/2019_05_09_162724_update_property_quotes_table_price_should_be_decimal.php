<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertyQuotesTablePriceShouldBeDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_quotes', function ($table) {
            $table->dropForeign('property_quotes_base_price_id_foreign');
        });

        Schema::table('property_quotes', function ($table) {
            $table->dropColumn('base_price_id');
            $table->decimal("base_price", 10, 2)->after( "pdlw_option_id" );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_quotes', function ($table) {
            $table->dropColumn('base_price');
            $table->unsignedBigInteger('base_price_id')->default(1)->after( "pdlw_option_id");

            $table->foreign( "base_price_id" )->references( 'id' )->on( 'base_prices' );
        });
    }
}
