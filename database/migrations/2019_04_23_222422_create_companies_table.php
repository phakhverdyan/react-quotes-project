<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment("Foreign Key to 'Users' table");
            $table->string( "company_name", 200)->comment("The company's name that manages/handles the property/ies");
            $table->string( "contact_person", 100)->comment( "The First and Lastname of the contact person for the company" );
            $table->string( "phone_no", 25)->comment( "Company's Phone Number" );
            $table->string( "phone_ext", 15)->nullable()->comment( "The Extention for the Phone Number" );
            $table->string( "address", 150)->nullable()->comment( "The Company's Address");
            $table->string( "suite", 15 )->nullable()->comment( "The Company's suite for the Address" );
            $table->string( "city", 50)->nullable()->comment( "The Company's city for the Address" );
            $table->string( "state", 2)->nullable()->comment( "The Company's state for the Address" );
            $table->string( "zip", 15)->nullable()->comment( "The Company's zip for the Address" );
            $table->string( "country", 3)->nullable()->comment( "The Company's country for the Address" );
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( "user_id" )->references( 'id' )->on( 'users' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
