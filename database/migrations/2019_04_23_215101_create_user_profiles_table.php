<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment("Foreign Key to 'Users' table");
            $table->string( "phone_no", 25)->comment( "Broker's Phone Number" );
            $table->string( "phone_ext", 15)->nullable()->comment( "The Extention for the Phone Number" );
            $table->string( "address", 150)->nullable()->comment( "The Broker's Address");
            $table->string( "suite", 15 )->nullable()->comment( "The Broker's suite for the Address" );
            $table->string( "city", 50)->nullable()->comment( "The Broker's city for the Address" );
            $table->string( "state", 2)->nullable()->comment( "The Broker's state for the Address" );
            $table->string( "zip", 15)->nullable()->comment( "The Broker's zip for the Address" );
            $table->string( "country", 3)->nullable()->comment( "The Broker's country for the Address" );
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( "user_id" )->references( 'id' )->on( 'users' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
