<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pdlw_option_id');
            $table->unsignedBigInteger('base_price_id');
            $table->unsignedBigInteger('personal_coverage_id');
            $table->unsignedBigInteger('enhancement_id');
            $table->boolean("statue")->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( "pdlw_option_id" )->references( 'id' )->on( 'pdlw_options' );
            $table->foreign( "base_price_id" )->references( 'id' )->on( 'base_prices' );
            $table->foreign( "personal_coverage_id" )->references( 'id' )->on( 'personal_coverages' );
            $table->foreign( "enhancement_id" )->references( 'id' )->on( 'enhancements' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_quotes');
    }
}
