<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string( 'name', 75);
            $table->string( 'description' );
            $table->boolean( 'status' )->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        Artisan::call('db:seed', [
            '--class' => "StatusSeeder",
            '--force' => true,
        ]);

        Schema::table('properties', function ($table) {
            $table->unsignedBigInteger("status_id")->after( "construction_type_id" )->default(1);
            $table->foreign( "status_id" )->references( 'id' )->on( 'statuses' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('properties', function ($table) {
            $table->dropForeign('properties_status_id_foreign');
        });

        Schema::table('properties', function ($table) {
            $table->dropColumn("status_id");
        });

        Schema::dropIfExists('statuses');
    }
}
