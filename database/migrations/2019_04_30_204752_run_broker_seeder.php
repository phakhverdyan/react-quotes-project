<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;

class RunBrokerSeeder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo Config::get( 'app.env' ) . PHP_EOL;
        if ( strtolower( Config::get( 'app.env' ) ) != 'production' ) {
            echo "We are not in production environment." . PHP_EOL;
            Artisan::call('db:seed', [
                '--class' => "BrokerSeeder",
                '--force' => true,
            ]);
        } else {
            echo "We are in production environment. Not seeding test brokers" . PHP_EOL;
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
